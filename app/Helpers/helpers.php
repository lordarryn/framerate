<?php

use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use nadar\quill\Lexer;
use nadar\quill\InlineListener;
use nadar\quill\Line;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Activity;
use App\Models\ActivityType;

/**
 * Get an image from the given database $image and
 * return the absolute path or create it if it is not an URL
 */
function getImageUrl(string $image = null, string $notFound = 'placeholder.jpg')
{
    if(filter_var($image, FILTER_VALIDATE_URL)) {
        return $image;
    } elseif(empty($image) || is_null($image)) {
        return asset('assets') . '/' . $notFound;
    } else {
        return asset('images') . '/' . $image;
    }
}

/**
 * Return thumbnail from Youtube video
 */
function getVideoThumbnail(string $url)
{
    // Youtube
    if(preg_match("/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/", $url, $m)) {
        return 'https://img.youtube.com/vi/' . $m['7'] . '/sddefault.jpg';
    }
    // Vimeo
    elseif(preg_match('#(?:https?://)?(?:www.)?(?:player.)?vimeo.com/(?:[a-z]*/)*([0-9]{6,11})[?]?.*#', $url, $m)) {
        $data = unserialize(file_get_contents('https://vimeo.com/api/v2/video/'.$m['1'].'.php'));
        return $data['0']['thumbnail_medium'];
    }
    // Dailymotion
    /*elseif(preg_match('!^.+dailymotion\.com/(video|hub)/([^_]+)[^#]*(#video=([^_&]+))?|(dai\.ly/([^_]+))!', $url, $m)) {
        $id = $m['6'] ?? $m['4'] ?? $m['2'];
        $url = 'https://api.dailymotion.com/video/'.$id.'?fields=thumbnail_360_url';
        $data = json_decode(file_get_contents($url), TRUE);
        return $data['thumbnail_360_url'];
    }*/
    // Default
    else {
        return asset('assets') . '/' . 'video-placeholder.png';
    }
}

/**
  * Paginate an array or a collection
  *
  * @param array|Collection      $items
  * @param int  $perPage
  * @param int  $page
  *
  * @return LengthAwarePaginator
  */
function paginateCollection($items, $perPage = 10, $page = null)
{
    $pageName = 'page';
    $page     = $page ?: (Paginator::resolveCurrentPage($pageName) ?: 1);
    $items    = $items instanceof Collection ? $items : Collection::make($items);

    return new LengthAwarePaginator(
        $items->forPage($page, $perPage)->values(),
        $items->count(),
        $perPage,
        $page,
        [
            'path'     => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]
    );
}

/**
 * Round a note
 */
function note($note) {
    return str_replace('.', ',', round($note, 2));
}

/**
 * Forum page, return equivalent style
 */
function forumStyle($name) {
    switch ($name) {
        case 'Cinéma':
            return 'cinema';
        case 'Séries TV':
            return 'tv';
        case 'Littérature':
            return 'books';
        case 'Jeux vidéo':
            return 'vidya';
        case 'Musique':
            return 'music';
        case 'Bla-bla':
            return 'misc';
    }
}

/**
 * Return random hex color
 */
function randomColorPart() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function randomColor() {
    return randomColorPart() . randomColorPart() . randomColorPart();
}

/**
 * Render with lexer from json
 */
function lexer($json) {
    $lexer = new Lexer($json);

    $lexer->registerListener(new class extends InlineListener {
        public function process(Line $line) {
            if($line->getAttribute('spoiler')) {
                $this->updateInput($line, '<span class="spoiler-tag">' . $line->getInput() . '</span>');
            }
        }
    });

    return $lexer->render();
}

/**
 * Save an activity in the database
 */
function saveActivity(User $user, Model $model, String $activityType, Array $data = []) {
    $activityType = ActivityType::where('type', '=', $activityType)->first();

    if(is_null($activityType)) {
        return;
    }

    $activity = new Activity([
        'data' => json_encode($data)
    ]);
    $activity->user()->associate($user);
    $activity->activitiable()->associate($model);
    $activity->activityType()->associate($activityType);
    $activity->save();
}
