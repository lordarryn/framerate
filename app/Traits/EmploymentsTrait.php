<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use App\Models\Department;
use App\Models\Employment;

trait EmploymentsTrait {

    /**
     * Return jointure of works+films following the parameters
     * where the jobs and departments are in the parameters
     * except clause and everything clause
     */
    private function workingAs(array $parameters) {
        return $this->works()
                    ->join('films', 'works.film_id', '=', 'films.id')
                    ->whereHas('employment', function(Builder $query) use($parameters) {
                        $i = 0;

                        foreach($parameters as $department => $jobs) {
                            foreach($jobs as $job) {
                                // If job and not except object
                                if(is_string($job)) {
                                    // If first iteration, where clause
                                    if($i == 0) {
                                        $i++;
                                        // Select the $job in the $department
                                        $query->where('employment_id', '=', Employment::where('name', '=', $job)
                                                                                      ->where('department_id', '=', Department::where('name', '=', $department)->first()->id)
                                                                                      ->first()->id);
                                    }

                                    // Not first iteration, orWhere clause
                                    else {
                                        $query->orWhere('employment_id', '=', Employment::where('name', '=', $job)
                                                                                        ->where('department_id', '=', Department::where('name', '=', $department)->first()->id)
                                                                                        ->first()->id);
                                    }
                                }

                                // All + except case
                                else {
                                    // If first iteration, where clause
                                    if($i == 0) {
                                        $i++;
                                        // If only everything (*) clause
                                        if(!isset($job['except'])) {
                                            $query->whereIn('employment_id', Employment::where('department_id', '=', Department::where('name', '=', $department)->first()->id)
                                                                                       ->get()
                                                                                       ->pluck('id')
                                                                                       ->toArray());
                                        }
                                        // Otherwise create where all and where not
                                        else {
                                            $excludeIds = [];
                                            foreach($job['except'] as $except) {
                                                $excludeIds[] = Employment::where('name', '=', $except)
                                                                          ->where('department_id', '=', Department::where('name', '=', $department)->first()->id)
                                                                          ->first()->id;
                                            }
                                            $allIds = Employment::where('department_id', '=', Department::where('name', '=', $department)->first()->id)
                                                                ->get()
                                                                ->pluck('id')
                                                                ->toArray();
                                            $query->whereIn('employment_id', array_diff($allIds, $excludeIds));
                                        }
                                    }

                                    // Not in first iteration, orWhere clause
                                    else {
                                        // If only everything (*) clause
                                        if(!isset($job['except'])) {
                                            $query->orWhereIn('employment_id', Employment::where('department_id', '=', Department::where('name', '=', $department)->first()->id)
                                                                                         ->get()
                                                                                         ->pluck('id')
                                                                                         ->toArray());
                                        }
                                        // Otherwise create where all and where not
                                        else {
                                            $excludeIds = [];
                                            foreach($job['except'] as $except) {
                                                $excludeIds[] = Employment::where('name', '=', $except)
                                                                          ->where('department_id', '=', Department::where('name', '=', $department)->first()->id)
                                                                          ->first()->id;
                                            }
                                            $allIds = Employment::where('department_id', '=', Department::where('name', '=', $department)->first()->id)
                                                                ->get()
                                                                ->pluck('id')
                                                                ->toArray();
                                            $query->orWhereIn('employment_id', array_diff($allIds, $excludeIds));
                                        }
                                    }
                                }
                            }
                        }
                    })->orderBy('films.release', 'desc');
    }

    /**
     * Return crew (all except actors)
     */
    public function crew() {
        return $this->workingAs([
            'Lighting' => [['*']],
            'Crew' => [['*']],
            'Sound' => [['*']],
            'Directing' => [['*']],
            'Visual Effects' => [['*']],
            'Writing' => [['*']],
            'Camera' => [['*']],
            'Costume & Make-Up' => [['*']],
            'Editing' => [['*']],
            'Art' => [['*']],
            'Production' => [['*']],
        ]);
    }


    /**
     * Functions returning works
     */
    public $getters = [
        ['directed', 'Réalisateur', 'Réalisatrice'],
        ['assistantDirected', 'Assistant Réalisateur', 'Assistante Réalisatrice'],
        ['played', 'Acteur', 'Actrice'],
        ['produced', 'Producteur', 'Productrice'],
        ['didCasting', 'Casting'],
        ['writed', 'Ecriture'],
        ['edited', 'Montage'],
        ['didLightingOrCamera', 'Caméra et lumière'],
        ['didPhotography', 'Photographie'],
        ['didMusic', 'Musique'],
        ['didSound', 'Son'],
        ['didAnimation', 'Animation'],
        ['didVfx', 'Effets Spéciaux'],
        ['didCostumeMakeup', 'Costume et maquillage'],
        ['artDirected', 'Direction artistique'],
        ['didArt', 'Art et décoration'],
        ['miscellaneous', 'Divers'],
    ];

    /**
     * Return movies directed
     */
    public function directed() {
        return $this->workingAs([
            'Directing' => [
                'Director'
            ]
        ]);
    }

    /**
     * Return movies where assistant director
     */
    public function assistantDirected() {
        return $this->workingAs([
            'Directing' => [
                'Assistant Director'
            ]
        ]);
    }

    /**
     * Return movies where actings
     */
    public function played() {
        return $this->workingAs([
            'Actors' => [
                ['*']
            ]
        ])->orderBy('order');
    }

    /**
     * Return movies where producer
     */
    public function produced() {
        return $this->workingAs([
            'Production' => [
                'Producer',
                'Executive Producer'
            ]
        ]);
    }

    /**
     * Return movies where worked in casting
     */
    public function didCasting() {
        return $this->workingAs([
            'Production' => [
                "Casting",
                "Casting Associate",
                "ADR Voice Casting",
                "Casting Assistant",
                "Casting Consultant",
                "Local Casting",
                "Additional Casting",
                "Assistant Extras Casting",
                "Background Casting Director",
                "Casting Coordinator",
                "Casting Director",
                "Casting Producer",
                "Casting Researcher",
                "Extras Casting",
                "Extras Casting Assistant",
                "Extras Casting Coordinator",
                "Locale Casting Director",
                "Location Casting",
                "Musical Casting",
                "Original Casting",
                "Street Casting",
            ]
        ]);
    }


    /**
     * Return movies writed and worked in the writing
     */
    public function writed() {
        return $this->workingAs([
            'Writing' => [
                ['*']
            ]
        ]);
    }

    /**
     * Return movies edited
     */
    public function edited() {
        return $this->workingAs([
            'Editing' => [
                ['*']
            ]
        ]);
    }

    /**
     * Return movies where worked in lightning/camera
     */
    public function didLightingOrCamera() {
        return $this->workingAs([
            'Lighting' => [
                ['*']
            ],
            'Camera' => [
                "Underwater Camera",
                "Camera Operator",
                "Camera Department Manager",
                "Camera Supervisor",
                "Camera Technician",
                "Additional Camera",
                "Camera Intern",
                "Helicopter Camera",
                "First Assistant Camera",
                "Aerial Camera",
                "Aerial Camera Technician",
                "Camera Loader",
                "Epk Camera Operator",
                "\"A\" Camera Operator",
                "\"B\" Camera Operator",
                "\"C\" Camera Operator",
                "\"D\" Camera Operator",
                "Additional First Assistant Camera",
                "Additional Second Assistant Camera",
                "Assistant Camera",
                "Camera Car",
                "Camera Department Production Assistant",
                "Camera Production Assistant",
                "Camera Trainee",
                "Camera Truck",
                "First Assistant \"A\" Camera",
                "First Assistant \"B\" Camera",
                "First Assistant \"C\" Camera",
                "First Assistant \"D\" Camera",
                "Second Assistant \"A\" Camera",
                "Second Assistant \"B\" Camera",
                "Second Assistant \"C\" Camera",
                "Second Assistant \"D\" Camera",
                "Second Assistant Camera",
                "Third Assistant \"A\" Camera",
                "Third Assistant \"B\" Camera",
                "Third Assistant \"C\" Camera",
                "Third Assistant \"D\" Camera",
                "Third Assistant Camera",
            ]
        ]);
    }

    /**
     * Return movies where worked in photography
     */
    public function didPhotography() {
        return $this->workingAs([
            'Camera' => [
                "Director of Photography",
                "Additional Photography",
                "Aerial Director of Photography",
                "Second Unit Director of Photography",
                "Underwater Director of Photography",
                "Additional Director of Photography",
                "Additional Underwater Photography",
            ],
            'Crew' => [
                'Cinematography',
                'Second Unit Cinematographer'
            ]
        ]);
    }

    /**
     * Return movies where worked in music
     */
    public function didMusic() {
        return $this->workingAs([
            'Sound' => [
                "Original Music Composer",
                "Additional Soundtrack",
                "Songs",
                "Music",
                "Music Director",
                "Orchestrator",
                "Musician",
                "Theme Song Performance",
                "Loop Group Coordinator",
                "Main Title Theme Composer",
                "Music Arranger",
                "Music Co-Supervisor",
                "Music Consultant",
                "Music Coordinator",
                "Music Producer",
                "Music Sound Design and Processing",
                "Music Supervision Assistant",
            ]
        ]);
    }

    /**
     * Return movies where worked in sound
     */
    public function didSound() {
        return $this->workingAs([
            'Sound' => [
                [
                    '*',
                    'except' => [
                        "Original Music Composer",
                        "Additional Soundtrack",
                        "Songs",
                        "Music",
                        "Music Director",
                        "Orchestrator",
                        "Musician",
                        "Theme Song Performance",
                        "Loop Group Coordinator",
                        "Main Title Theme Composer",
                        "Music Arranger",
                        "Music Co-Supervisor",
                        "Music Consultant",
                        "Music Coordinator",
                        "Music Producer",
                        "Music Sound Design and Processing",
                        "Music Supervision Assistant",
                    ]
                ]
            ]
        ]);
    }

    /**
     * Return movies where worked in animation
     */
    public function didAnimation() {
        return $this->workingAs([
            'Visual Effects' => [
                "Animation",
                "Animation Manager",
                "Animation Director",
                "Animation Department Coordinator",
                "Animation Fix Coordinator",
                "Animation Production Assistant",
                "Animation Supervisor",
                "Key Animation",
                "Opening/Ending Animation",
                "Animation Coordinator",
                "Animation Technical Director",
                "Head of Animation",
                "Supervising Animation Director",
            ]
        ]);
    }

    /**
     * Return movies where worked in VFX
     */
    public function didVfx() {
        return $this->workingAs([
            'Visual Effects' => [
                [
                    '*',
                    'except' => [
                        "Animation",
                        "Animation Manager",
                        "Animation Director",
                        "Animation Department Coordinator",
                        "Animation Fix Coordinator",
                        "Animation Production Assistant",
                        "Animation Supervisor",
                        "Key Animation",
                        "Opening/Ending Animation",
                        "Animation Coordinator",
                        "Animation Technical Director",
                        "Head of Animation",
                        "Supervising Animation Director",
                    ]
                ]
            ]
        ]);
    }

    /**
     * Return movies where worked in makeup/costumes
     */
    public function didCostumeMakeup() {
        return $this->workingAs([
            'Costume & Make-Up' => [
                ['*']
            ]
        ]);
    }

    /**
     * Return movies where worked in art direction
     */
    public function artDirected() {
        return $this->workingAs([
            'Art' => [
                'Art Direction'
            ]
        ]);
    }

    /**
     * Return movies where worked in art
     */
    public function didArt() {
        return $this->workingAs([
            'Art' => [
                [
                    '*',
                    'except' => [
                        'Art Direction'
                    ]
                ]
            ]
        ]);
    }

    /**
     * Return movies where worked in miscellaneous = all except already returned
     */
    public function miscellaneous() {
        return $this->workingAs([
            'Crew' => [
                [
                    '*',
                    'except' => [
                        'Cinematography',
                        'Second Unit Cinematographer'
                    ]
                ]
            ],
            'Directing' => [
                [
                    '*',
                    'except' => [
                        'Director',
                        'Assistant Director'
                    ]
                ]
            ],
            'Camera' => [
                [
                    '*',
                    'except' => [
                        "Director of Photography",
                        "Additional Photography",
                        "Aerial Director of Photography",
                        "Second Unit Director of Photography",
                        "Underwater Director of Photography",
                        "Additional Director of Photography",
                        "Additional Underwater Photography",
                        "Underwater Camera",
                        "Camera Operator",
                        "Camera Department Manager",
                        "Camera Supervisor",
                        "Camera Technician",
                        "Additional Camera",
                        "Camera Intern",
                        "Helicopter Camera",
                        "First Assistant Camera",
                        "Aerial Camera",
                        "Aerial Camera Technician",
                        "Camera Loader",
                        "Epk Camera Operator",
                        "\"A\" Camera Operator",
                        "\"B\" Camera Operator",
                        "\"C\" Camera Operator",
                        "\"D\" Camera Operator",
                        "Additional First Assistant Camera",
                        "Additional Second Assistant Camera",
                        "Assistant Camera",
                        "Camera Car",
                        "Camera Department Production Assistant",
                        "Camera Production Assistant",
                        "Camera Trainee",
                        "Camera Truck",
                        "First Assistant \"A\" Camera",
                        "First Assistant \"B\" Camera",
                        "First Assistant \"C\" Camera",
                        "First Assistant \"D\" Camera",
                        "Second Assistant \"A\" Camera",
                        "Second Assistant \"B\" Camera",
                        "Second Assistant \"C\" Camera",
                        "Second Assistant \"D\" Camera",
                        "Second Assistant Camera",
                        "Third Assistant \"A\" Camera",
                        "Third Assistant \"B\" Camera",
                        "Third Assistant \"C\" Camera",
                        "Third Assistant \"D\" Camera",
                        "Third Assistant Camera"
                    ]
                ]
            ],
            'Production' => [
                [
                    '*',
                    'except' => [
                        'Producer',
                        'Executive Producer',
                        "Casting",
                        "Casting Associate",
                        "ADR Voice Casting",
                        "Casting Assistant",
                        "Casting Consultant",
                        "Local Casting",
                        "Additional Casting",
                        "Assistant Extras Casting",
                        "Background Casting Director",
                        "Casting Coordinator",
                        "Casting Director",
                        "Casting Producer",
                        "Casting Researcher",
                        "Extras Casting",
                        "Extras Casting Assistant",
                        "Extras Casting Coordinator",
                        "Locale Casting Director",
                        "Location Casting",
                        "Musical Casting",
                        "Original Casting",
                        "Street Casting",
                    ]
                ]
            ]
        ]);
    }

}
