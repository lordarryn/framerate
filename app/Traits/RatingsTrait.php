<?php

namespace App\Traits;

trait RatingsTrait {
    // deleted ratings
    public function deletedRatings() {
        return $this->hasMany('App\Models\Rating')
                    ->where('deleted', true);
    }

    // old ratings (updated ratings)
    public function oldRatings() {
        return $this->hasMany('App\Models\Rating')
                    ->where('deleted', false)
                    ->where('updated', true);
    }

    // current ratings (not deleted or updated) ordered by created_at by default
    public function ratings() {
        return $this->hasMany('App\Models\Rating')
                    ->where('deleted', false)
                    ->where('updated', false)
                    ->orderBy('created_at', 'desc');
    }
}
