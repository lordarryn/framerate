<?php

namespace App\Traits;
use DB;

trait RandomTrait {

    public static function random() {

        $model = static::class;
        $model = new $model();
        $table = $model->getTable();

        $random = DB::select(DB::raw("
            SELECT r1.id
            FROM $table AS r1 JOIN
                (SELECT CEIL(RAND() *
                    (SELECT MAX(id)
                    FROM $table)) AS id)
                AS r2
            WHERE r1.id >= r2.id
            ORDER BY r1.id ASC
            LIMIT 1"));
        $random = $random['0']->id;

        return $model::find($random);

    }

}
