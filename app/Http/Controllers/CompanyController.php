<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::join('likes', function($join) {
                               $join->on('likes.likeable_id', 'companies.id')
                                    ->where('likes.likeable_type', '=', Company::class)
                                    ->where('likes.type_id', '=', 'like');
                            })
                            ->selectRaw('companies.*, count(*) as likes')
                            ->groupBy('companies.id')
                            ->orderBy('likes', 'desc')
                            ->limit(20)
                            ->get();

        return view('company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $company = Company::findBySlugOrFail($slug);

        return view('company.show', compact('company'));
    }

    /**
     * Return a random page
     *
     * @return \Illuminate\Http\Response
     */
    public function random()
    {
        $company = Company::random();

        return redirect()->route('companies.show', $company->slug);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
