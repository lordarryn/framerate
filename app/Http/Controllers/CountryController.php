<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Film;
use App\Models\Rating;
use App\Models\Genre;
use DB;

class CountryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $default = Country::selectRaw('*, 0 as total')->get();

        // Countries with at least one movie
        $countries = Country::leftJoin('countriables', 'countriables.country_id', '=', 'countries.id')
                            ->whereNotNull('countriables.countriable_id')
                            ->where('countriables.countriable_type', '=', Film::class)
                            ->selectRaw('countries.*, count(*) as total')
                            ->groupBy('countries.id')
                            ->get();

        $countries = $default->merge($countries);

        return view('country.index', compact('countries'));
    }

    public function show($id, $name = null)
    {
        $country = Country::findOrFail($id);

        if(isset($name) && $name != __('countries.'.$country->name)) {
            abort(404);
        }

        $films = Film::topFilms(null, $country);

        return view('country.show', compact(['country', 'films']));
    }
}
