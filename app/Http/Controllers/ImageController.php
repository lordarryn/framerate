<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\User;
use App\Models\Personality;
use App\Models\Image;
use App\Models\Like;
use App\Models\Analytics;
use Auth;
use Str;
use ImageIntervention;
use Storage;

class ImageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'only.ajax'])->except('index');
    }

    /**
     * Public gallery
     */
    public function index()
    {
        $carousel = (new Analytics())->topFilms();

        $images = Image::join('likes', function($join) {
                            $join->on('likes.likeable_id', 'images.id')
                                 ->where('likes.likeable_type', '=', Image::class)
                                 ->where('likes.type_id', '=', 'like');
                         })
                         ->selectRaw('images.*, count(*) as totalLikes')
                         ->groupBy('images.id')
                         ->orderBy('totalLikes', 'desc')
                         ->orderBy('images.id', 'asc')
                         ->paginate(25);

        abort_if($images->isEmpty(), 204);

        return view('gallery.index', compact(['carousel', 'images']));
    }

    /**
     * Gallery related to a profile
     */
    public function profile($pseudo)
    {
        $user = User::where('pseudo', '=', $pseudo)->firstOrFail();

        $images = paginateCollection($user->images, 25);
        abort_if($images->isEmpty(), 204);

        return view('gallery.profile', compact('images'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $model
     * @param $slug
     * @param $media
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($model, $slug, $media, Request $request)
    {
        $this->validate($request, [
            'image' => 'required_without:video|image|mimes:jpeg,png,jpg,gif,svg|max:4096',
            'video' => 'required_without:image|url',
        ]);

        $user = Auth::user();

        if($model == 'film') {
            $model = Film::findBySlugOrFail($slug);
        } elseif($model == 'personality') {
            $model = Personality::findBySlugOrFail($slug);
        }

        if($media == 'image' && $request->has('image') && !empty($request->image)) {
            // Save image
            $imgName = Str::random(32) . '.' . $request->image->extension();
            $request->image->move(storage_path('app/images'), $imgName);

            // Save thumbnail
            $img = ImageIntervention::make(storage_path('app/images/') . $imgName);
            $img->resize(300, null, function($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path('app/images/') . 'thumbnail' . $imgName);

            $image = new Image;
            $image->url = $imgName;
            $image->user()->associate($user);
            $image->imageable()->associate($model);
            $image->save();
        }
        // save video
        elseif($media == 'video') {
            $image = new Image;
            $image->url = $request->video;
            $image->media_type = 'video';
            $image->user()->associate($user);
            $image->imageable()->associate($model);
            $image->save();
        }

        // activity
        saveActivity($user, $image, 'post_image');
    }

    /**
     * Delete image
     */
    public function destroy($id)
    {
        $image = Image::findOrFail($id);

        $imageName = $image->url;

        if(Storage::exists('images/' . $imageName)) {
            Storage::delete('images/' . $imageName);
        }

        if(Storage::exists('images/thumbnail' . $imageName)) {
            Storage::delete('images/thumbnail' . $imageName);
        }

        $image->user()->dissociate();
        $image->imageable()->detach();
        $image->likes()->delete();
        $image->delete();
    }
}
