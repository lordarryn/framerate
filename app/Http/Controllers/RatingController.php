<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Rating;
use App\Models\Activity;
use App\Models\ActivityType;
use Auth;

class RatingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'only.ajax']);
    }

    /**
     * Add a note to a film
     */
    public function rate(Request $request)
    {
        $notes = array(
            '1', '1.5', '2', '2.5',
            '3', '3.5', '4', '4.5',
            '5', '5.5', '6', '6.5',
            '7', '7.5', '8', '8.5',
            '9', '9.5', '10'
        );

        $this->validate($request, [
            'film' => 'required|string',
            'note' => 'required|in:' . implode(',', $notes)
        ]);

        $user = Auth::user();
        $film = Film::findBySlugOrFail($request->film);

        $rating = Rating::where('film_id', '=', $film->id)
                        ->where('user_id', '=', $user->id)
                        ->where('updated', false)
                        ->where('deleted', false)
                        ->first();

        // film already rated
        if(!is_null($rating)) {
            // if same
            if($rating->rating == $request->note) {
                return response()->json();
            }

            // set rating as updated
            $rating->updated = true;
            $rating->save();
        }

        $rating = new Rating([
            'rating' => floatval($request->note)
        ]);
        $rating->film()->associate($film);
        $rating->user()->associate($user);
        $rating->save();

        saveActivity($user, $rating, 'note');

        return response()->json();
    }

    /**
     * Delete user notes related to the film
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'film' => 'required|string'
        ]);

        $film = Film::findBySlugOrFail($request->film);

        $ratings = Rating::where('user_id', '=', Auth::user()->id)
                         ->where('film_id', '=', $film->id);

        $ratings->each(function($rating) {
            $rating->deleted = true;
            $rating->save();
        });
    }
}
