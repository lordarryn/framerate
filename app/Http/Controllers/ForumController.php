<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forum;
use App\Models\Thread;
use App\Models\Reply;
use Carbon\Carbon;
use Auth;

class ForumController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified'])->only(['store']);
    }

    public function index() {
        $forums = Forum::all();

        return view('forum.index', compact('forums'));
    }

    public function show($id) {
        $forum = Forum::findOrFail($id);

        $threads = $forum->threads()->get()->sortByDesc('latestReply.created_at');
        $threads = paginateCollection($threads, 20);

        $date = Carbon::now()->format('d/m/Y');

        return view('forum.show', compact(['forum', 'threads', 'date']));
    }

    public function showThread($id) {
        $thread = Thread::findOrFail($id);

        $replies = paginateCollection($thread->replies, 20);

        return view('forum.thread.show', compact(['thread', 'replies']));
    }

    public function showReply($id) {
        $reply = Reply::findOrFail($id);

        return view('forum.reply.show', compact('reply'));
    }

    public function storeThread($id, Request $request) {
        $this->validate($request, [
            'title' => 'string|max:100',
            'content' => 'json',
        ]);

        $forum = Forum::findOrFail($id);
        $json = json_decode($request->content, true);
        $content = lexer($json);

        // If empty
        if($content == '<p><br></p>') {
            $request->session()->flash('alert-content', 'Le contenu du sujet est vide.');
            return back();
        }

        // Too long
        if(strlen($content) > 65000) {
            $request->session()->flash('alert-content', 'Le contenu du sujet est trop long.');
            return back();
        }

        // Save new thread
        $thread = new Thread([
            'title'   => $request->title,
            'status'  => 0,
            'flag'    => 0
        ]);
        $thread->user()->associate(Auth::user());
        $thread->forum()->associate($forum);
        $thread->save();

        // Save message
        $reply = new Reply([
            'content' => $content
        ]);
        $reply->user()->associate(Auth::user());
        $reply->thread()->associate($thread);
        $reply->save();

        return redirect()->route('forums.threads.show', $thread->id);
    }

    public function storeReply($id, Request $request) {
        $this->validate($request, [
            'content' => 'json',
        ]);

        $thread = Thread::findOrFail($id);
        $json = json_decode($request->content, true);
        $content = lexer($json);

        // If empty
        if($content == '<p><br></p>') {
            $request->session()->flash('alert-content', 'Le contenu du sujet est vide.');
            return back();
        }

        // Too long
        if(strlen($content) > 65000) {
            $request->session()->flash('alert-content', 'Le contenu du sujet est trop long.');
            return back();
        }

        $reply = new Reply([
            'content' => $content
        ]);
        $reply->user()->associate(Auth::user());
        $reply->thread()->associate($thread);
        $reply->save();

        return redirect()->to(url()->previous() . '#reply_' . $reply->id);
    }
}
