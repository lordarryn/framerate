<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Auth;
use App\Models\Analytics;
use App\Models\Film;
use App\Models\DbHistory;
use App\Models\Rating;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('only.ajax')->only('live');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $analytics = new Analytics();
        $carousel = $analytics->topFilms();
        $personalities = $analytics->topPersonalities();

        // Logged, then feed
        $feed = (null !== Auth::user()) ? Auth::user()->feed() : null;

        return view('index.index', compact(['carousel', 'personalities', 'feed']));
    }

    public function about()
    {
        return view('index.about');
    }
}
