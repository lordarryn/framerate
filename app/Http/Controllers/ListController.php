<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use App\Models\FilmList;
use App\Models\Film;
use App\Models\Image;
use App\Models\Like;
use Auth;
use Str;
use ImageIntervention;
use Storage;

class ListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified'])->except(['show']);
    }

    public function show($slug, $id)
    {
        $list = FilmList::findOrFail($id);

        if($list->is_top_user) {
            return abort(404);
        }

        if($slug != $list->slug) {
            return redirect()->route('list.film.show', ['slug' => $list->slug, 'id' => $list->id]);
        }

        $films = paginateCollection($list->films, 20);

        return view('list.show', compact(['list', 'films']));
    }

    /**
     * Edit page list
     */
    public function edit($id)
    {
        $list = FilmList::where('user_id', '=', Auth::user()->id)
                        ->where('id', $id)
                        ->first();

        if(is_null($list)) {
            return abort(404);
        }

        if($list->is_top_user) {
            return abort(404);
        }

        $films = paginateCollection($list->films, 20);

        return view('list.edit', compact(['list', 'films']));
    }

    /**
     * Edit private top
     */
    public function editTop()
    {
        $user = Auth::user();
        $top = $user->top;

        return view('list.top', compact(['user', 'top']));
    }

    /**
     * Update a list (title, desc, image)
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'nullable|string|max:100',
            'content' => 'nullable|json',
            'backgroundList' => 'image|mimes:jpeg,png,jpg,svg|max:4096'
        ]);

        $user = Auth::user();
        $list = FilmList::where('user_id', '=', $user->id)
                        ->where('id', $id)
                        ->where('is_top_user', false)
                        ->first();

        if(is_null($list)) {
            return abort(404);
        }

        $json = json_decode($request->content, true);
        $content = lexer($json);

        // Too long
        if(strlen($content) > 65000) {
            $request->session()->flash('alert-content', 'Le contenu de la description est trop longue.');
            return back();
        }

        // If empty
        if($content == '<p><br></p>') {
            $content = '';
        }

        if($request->title == "") {
            $request->title = 'Liste';
        }

        // Save background
        if($request->has('backgroundList')) {
            $bgName = Str::random(32) . '.' . $request->backgroundList->extension();
            $request->backgroundList->move(storage_path('app/images'), $bgName);

            // Save thumbnail
            $thumb = ImageIntervention::make(storage_path('app/images/') . $bgName);
            $thumb->resize(400, null, function($constraint) {
                $constraint->aspectRatio();
            })->save(storage_path('app/images/') . 'thumbnail' . $bgName);
        }

        // remove old bg
        if(isset($bgName)) {
            if(isset($list->background)) {
                if(Storage::exists('images/' . $list->background)) {
                    Storage::delete('images/' . $list->background);
                }
                if(Storage::exists('images/thumbnail' . $list->background)) {
                    Storage::delete('images/thumbnail' . $list->background);
                }
            }
            $list->background = $bgName;
        }

        $list->text = $content;
        $list->slug = null;
        $list->title = $request->title;
        $list->save();

        $request->session()->flash('info', 'La liste a été mise à jour.');

        return back();
    }

    /**
     * Update text associated with a film in a list
     */
    public function updateText($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'nullable|string|max:100',
            'text' => 'nullable|string',
        ]);

        $film = Film::findOrFail($request->film_id);
        $list = FilmList::where('user_id', '=', Auth::user()->id)
                        ->where('id', $id)
                        ->where('is_top_user', false)
                        ->first();

        if(is_null($list)) {
            return abort(404);
        }

        $filmToUpdate = $list->films->where('id', '=', $film->id)->first();

        if(is_null($filmToUpdate)) {
            return abort(404);
        }

        $filmToUpdate->pivot->text = $request->text;
        $filmToUpdate->pivot->save();
        $list->touch();
        $request->session()->flash('info', 'Vous avez modifié la liste.');
        return redirect()->route('list.film.edit', $list->id);
    }

    /**
     * Create a list
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'film_id' => 'required|integer'
        ]);

        $film = Film::findOrFail($request->film_id);
        $user = Auth::user();

        $list = FilmList::create([
            'user_id' => $user->id,
            'is_top_user' => false,
            'text' => '',
            'title' => 'Liste'
        ]);

        $list->films()->attach($film, ['text' => '', 'order' => 1]);
        $request->session()->flash('info', 'Vous avez bien créé une liste et rajouté le film '. $film->title .'.');

        // activity
        saveActivity($user, $list, 'create_list');

        return redirect()->route('list.film.edit', $list->id);
    }

    /**
     * Add to a list
     */
    public function add($id, Request $request)
    {
        $this->validate($request, [
            'film_id' => 'required|integer'
        ]);

        $film = Film::findOrFail($request->film_id);
        $user = Auth::user();

        $list = FilmList::where('user_id', '=', $user->id)
                        ->where('id', '=', $id)
                        ->where('is_top_user', false)
                        ->first();

        if(is_null($list)) {
            return abort(404);
        }

        //film exists in list
        if($list->films->contains($film->id)) {
            $request->session()->flash('alert', 'Le film est déjà présent dans la liste.');
        } else {
            $list->touch();
            $list->films()->attach($film, ['text' => '', 'order' => $list->films->count()+1]);
            $request->session()->flash('info', 'Vous avez rajouté le film '. $film->title .' à votre liste.');

            // activity
            saveActivity($user, $list, 'add_film_list', ['add_film_id' => $film->id]);
        }

        return redirect()->route('list.film.edit', $id);
    }

    /**
     * Add to a top
     */
    public function storeTop(Request $request)
    {
        $this->validate($request, [
            'film_id' => 'required|integer',
        ]);

        $film = Film::findOrFail($request->film_id);
        $user = Auth::user();

        $top = FilmList::where('user_id', '=', $user->id)
                       ->where('is_top_user', true)
                       ->first();

        // if top doesnt exist yet
        if(is_null($top)) {
            $top = new FilmList;
            $top->is_top_user = true;
            $top->text = '';
            $top->user()->associate($user);
            $top->save();
            $top->films()->attach($film, ['text' => '', 'order' => 1]);

            // activity
            saveActivity($user, $top, 'add_film_top', ['add_film_id' => $film->id]);

            $request->session()->flash('info', 'Vous avez rajouté le film '. $film->title .' à votre top.');
        }

        // if top exists add in the top
        else {
            //film exists in top
            if($top->films->contains($film->id)) {
                $request->session()->flash('alert', 'Le film est déjà présent dans le top.');
            }
            //top > 50
            elseif($top->films->count() == 50) {
                $request->session()->flash('alert', 'Le top est complet, vous pouvez supprimer des films avant d\'en rajouter des nouveaux.');
            }
            // add to top
            else {
                // activity
                saveActivity($user, $top, 'add_film_top', ['add_film_id' => $film->id]);
                $top->films()->attach($film, ['text' => '', 'order' => $top->films->count()+1]);
                $request->session()->flash('info', 'Vous avez rajouté le film '. $film->title .' à votre top.');
            }
        }

        return redirect()->route('list.top.edit');
    }

    /**
     * order list
     */
    public function order($id, Request $request)
    {
        $this->validate($request, [
            'film_id' => 'required|integer',
            'order' => 'required|integer'
        ]);

        $film = Film::findOrFail($request->film_id);
        $user = Auth::user();

        $list = FilmList::where('user_id', '=', $user->id)
                        ->where('id', '=', $id)
                        ->first();

        if(is_null($list)) {
            return abort(404);
        }

        $order = $request->order;
        $films = $list->films;

        if($order > $films->count()) {
            return back();
        }

        $filmToReorder = $films->where('id', '=', $film->id)->first();

        if(!is_null($filmToReorder)) {
            if($filmToReorder->pivot->order == $order) {
                return back();
            }

            // old order
            $tmpOrder = $filmToReorder->pivot->order;
            // change order of the film
            $filmToReorder->pivot->order = $order;
            $filmToReorder->pivot->save();

            // loop through list to update
            foreach($films as $reorderFilm) {
                if($order > $tmpOrder) {
                    if($reorderFilm->pivot->order <= $order && $reorderFilm->pivot->order > $tmpOrder && $reorderFilm->id != $filmToReorder->id) {
                        $reorderFilm->pivot->order--;
                        $reorderFilm->pivot->save();
                    }
                } elseif($order < $tmpOrder) {
                    if($reorderFilm->pivot->order >= $order && $reorderFilm->pivot->order < $tmpOrder && $reorderFilm->id != $filmToReorder->id) {
                        $reorderFilm->pivot->order++;
                        $reorderFilm->pivot->save();
                    }
                }
             }

            $list->touch();
            $request->session()->flash('info', 'Vous avez changé l\'ordre de votre top.');
        }

        return back();
    }

    /**
     * remove to top/list
     */
    public function destroyFilm($id, Request $request)
    {
        $this->validate($request, [
            'film_id' => 'required|integer'
        ]);

        $film = Film::findOrFail($request->film_id);
        $user = Auth::user();

        $list = FilmList::where('user_id', '=', $user->id)
                        ->where('id', '=', $id)
                        ->first();

        if(is_null($list)) {
            return abort(404);
        }

        $filmToDestroy = $list->films->where('id', '=', $film->id)->first();

        if(is_null($filmToDestroy)) {
            return back();
        }

        $order = $filmToDestroy->pivot->order;

        // detach then reorder
        $list->films()->detach($film);

        $films = $list->films;

        foreach($films as $reorderFilm) {
            if($reorderFilm->pivot->order > $order) {
                $reorderFilm->pivot->order--;
                $reorderFilm->pivot->save();
            }
        }

        // if is top and empty delete
        if($list->is_top_user) {
            if($list->films->count()-1 == 0) {
                $list->delete();
            }
        } else {
            $list->touch();
        }

        $request->session()->flash('info', 'Vous avez retiré le film '. $film->title .' .');
        return back();
    }

    /**
     * Destroy list
     */
    public function destroy($id, Request $request)
    {
        $user = Auth::user();
        $list = FilmList::where('user_id', '=', $user->id)
                        ->where('id', '=', $id)
                        ->where('is_top_user', false)
                        ->first();

        if(is_null($list)) {
            return abort(404);
        }

        if(Storage::exists('images/' . $list->background)) {
            Storage::delete('images/' . $list->background);
        }
        if(Storage::exists('images/thumbnail' . $list->background)) {
            Storage::delete('images/thumbnail' . $list->background);
        }

        $list->films()->detach();
        $list->delete();
        $request->session()->flash('info', 'Vous avez supprimé votre liste.');
        return redirect()->route('list.top.edit');
    }
}
