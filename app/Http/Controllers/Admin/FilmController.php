<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobs\ProcessTmdbImport;
use App\Models\Film;
use App\Models\Employment;
use DB;
use Auth;

class FilmController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'verified']);
        $this->middleware(['only.ajax'])->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * Duplicated movies
         * Laravel and SQL don't return same values
         * need to make shit here with Laravel
         */

        /**
         * SELECT *
         * FROM `films`
         * INNER JOIN
         *     (SELECT *
         *     FROM `films`
         *     GROUP BY `films`.`original_title`, `films`.`release`
         *     HAVING COUNT(*) > 1) as `sub`
         * ON `sub`.`original_title` = `films`.`original_title` AND `sub`.`release` = `films`.`release`
         * ORDER BY `films`.`original_title` ASC
         */
        $grouped = Film::select('films.original_title', 'films.release', 'films.imdb_id')
                       ->groupBy('films.original_title', 'films.release', 'films.imdb_id')
                       ->havingRaw('COUNT(*) > 1')
                       ->get();

        $duplicated = collect();

        foreach($grouped as $film) {
            $films = Film::where('original_title', '=', $film->original_title)
                         ->where('release', '=', $film->release)
                         ->where('imdb_id', '=', $film->imdb_id)
                         ->get();

            $duplicated = $duplicated->merge($films);
        }

        // Movies without director
        $withoutDirectors = Film::whereNotExists(function($query) {
            $query->select('*')
                  ->from('films as with_directors')
                  ->leftJoin('works', 'works.film_id', '=', 'with_directors.id')
                  ->where('works.employment_id', '=', Employment::where('name', '=', 'Director')->first()->id)
                  ->whereColumn('films.id', 'with_directors.id');
        })->get();

        return view('admin.film.index', compact(['duplicated', 'withoutDirectors']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.film.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ids'   => 'required|json',
            '_type' => 'required|in:films,personalities',
        ]);

        $ids = json_decode($request->ids, true);
        $valid_ids = array();

        // Regex all IDs
        if($request->_type == 'films') {
            foreach($ids as $id) {
                if(preg_match('/tt\d{7}/', $id['value'])) {
                    $valid_ids[] = $id['value'];
                }
            }
        } elseif($request->_type == 'personalities') {
            foreach($ids as $id) {
                if(preg_match('/nm\d{7}/', $id['value'])) {
                    $valid_ids[] = $id['value'];
                }
            }
        }

        // If no valid movies
        if(empty($valid_ids)) {
            $request->session()->flash('alert-error', 'Vous n\'avez envoyé aucun ID valide.');
            return back();
        }

        if($request->_type == 'films') {
            $request->session()->flash('alert-info', 'Vous avez fait une requête pour ajouter ' . count($valid_ids) . ' film(s).');

            // Process async job
            foreach($valid_ids as $id) {
                ProcessTmdbImport::dispatch(auth()->user(), 'film', $id);
            }
        } elseif($request->_type == 'personalities') {
            $request->session()->flash('alert-info', 'Vous avez fait une requête pour ajouter des films liés à une personnalité.');

            // Process async job
            foreach($valid_ids as $id) {
                ProcessTmdbImport::dispatch(auth()->user(), 'personality', $id);
            }
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $film = Film::findOrFail($id);

        ProcessTmdbImport::dispatch(auth()->user(), 'film', $film->imdb_id, 'update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::findOrFail($id);

        $film->deletedRatings()->delete();
        $film->oldRatings()->delete();
        $film->ratings()->delete();
        $film->works()->delete();

        $film->companies()->detach();
        $film->genres()->detach();
        $film->languages()->detach();
        $film->countries()->detach();

        $film->delete();
    }
}
