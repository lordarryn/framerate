<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Image;
use DB;

class ImageController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Index
     */
    public function index() {
        $images = Image::all();

        return view('admin.image.index', compact('images'));
    }
}
