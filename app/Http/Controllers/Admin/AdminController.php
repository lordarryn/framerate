<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Personality;
use App\Models\Work;
use App\Models\User;
use App\Models\Session;
use DB;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Index
     */
    public function index() {
        $count = [
            'films' => Film::count(),
            'personnalities' => Personality::count(),
            'works' => Work::count(),
            'users' => User::count(),
            'activeUsers' => Session::usersWithinMinutes(10)->get()->count(),
            'activeGuests' => Session::guestsWithinMinutes(10)->get()->count(),
        ];

        $table = config('database.connections.mysql.database');

        $sizes = collect(DB::select(DB::raw("
            SELECT table_name AS 'table',
                    ROUND(((data_length + index_length) / 1024 / 1024), 2) AS 'size'
            FROM information_schema.TABLES
            WHERE table_schema = '$table'
            ORDER BY (data_length + index_length) DESC")));

        return view('admin.index', compact(['count', 'sizes']));
    }

    /**
     * Notifications page
     */
    public function notifications() {
        return view('admin.notifications');
    }
}
