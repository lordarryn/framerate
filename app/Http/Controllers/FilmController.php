<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Http;
use Storage;
use App\Models\Film;
use App\Models\Genre;
use App\Models\Country;
use App\Models\Personality;
use App\Models\User;
use DB;

class FilmController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware(['auth', 'verified'])->except(['index', 'show', 'random']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = Film::topFilms();
        $genres = Genre::all();

        return view('film.index', compact(['films', 'genres']));
    }

    /**
     * Display top movies by genre
     */
    public function genre($id, $name = null)
    {
        $genre = Genre::findOrFail($id);

        if(isset($name) && $name != __('genres.'.$genre->name)) {
            abort(404);
        }

        $films = Film::topFilms(null, null, $genre);

        return view('film.genre', compact(['genre', 'films']));
    }

    /**
     * Display top movies by year.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function year($slug)
    {
        $year = $slug;
        $film = Film::where('release_year', '=', $year)->first();

        if($film === null) {
            abort(404);
        }

        $films = Film::topFilms($year);

        return view('film.year', compact(['year', 'films']));
    }

    /**
     * Return a random page
     *
     * @return \Illuminate\Http\Response
     */
    public function random()
    {
        $film = Film::random();

        return redirect()->route('films.show', $film->slug);
    }

    /**
     * Films related to a profile (collection tab)
     */
    public function profile($pseudo, Request $request)
    {
        $genres = Genre::all();
        $genres_validation = '';
        $decades_validation = '';

        foreach($genres as $g) {
            $genres_validation .= $g->id . ',';
        }

        for($i=2020;$i>=1870;$i-=10) {
            $decades_validation .= $i . ',';
        }

        $genres_validation .= 'all';
        $decades_validation .= 'all';

        $this->validate($request, [
            'by' => 'required|in:note_date,release_date,user_note,film_avg,film_pop',
            'genre' => 'required|in:'.$genres_validation,
            'decade' => 'required|in:'.$decades_validation
        ]);

        $user = User::where('pseudo', '=', $pseudo)->firstOrFail();
        $sortBy = $request->by;
        $genre = $request->genre;
        $decade = $request->decade;

        // sort collection
        if($sortBy == 'note_date') {
            $films = $user->ratings;
        } elseif($sortBy == 'user_note') {
            $films = $user->ratingsByNote();
        } elseif($sortBy == 'release_date') {
            $films = $user->ratingsByReleaseDate();
        } elseif($sortBy == 'film_avg') {
            $films = $user->ratingsByFilmAvg();
        } elseif($sortBy == 'film_pop') {
            $films = $user->ratingsByFilmPopularity();
        }

        // filter with genre
        if($genre != 'all') {
            $films = $films->filter(function($item, $key) use($genre) {
                foreach($item->film->genres as $elem) {
                    if($elem->id == $genre) {
                        return true;
                    }
                }
            });
        }

        // filter with valid decade
        if($decade != 'all') {
            $films = $films->filter(function($item, $key) use($decade) {
                if(isset($item->film->release_year)) {
                    if($item->film->release_year >= $decade && $item->film->release_year < $decade+10) {
                        return true;
                    }
                }
            });
        }

        // pagination
        $films = paginateCollection($films, 25);

        return view('film.profile', compact('films'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*$genres = Genre::all();
        $countries = Country::all();
        $languages = Language::all();
        $companies = Company::all();

        return view('film.create', compact(['genres', 'countries', 'languages', 'companies']));*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $film = Film::findBySlugOrFail($slug);

        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
