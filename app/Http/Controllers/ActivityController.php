<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\User;

class ActivityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified'])->only('feed');
    }

    /**
     * Show the activity feed.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function feed(Request $request)
    {
        $user = $request->user();
        $following_validation = '';

        foreach($user->following as $f) {
            $following_validation .= $f->id . ',';
        }

        $this->validate($request, [
            'follow' => 'required|in:0,'.$following_validation,
            'type' => 'required|in:all,list',
        ]);

        if($request->follow == 0) {
            $follow = null;
        } else {
            $follow = User::findOrFail($request->follow);
        }

        $feed = $user->feed($follow, $request->type);
        abort_if($feed->isEmpty(), 204);

        return view('activity.feed', ['activities' => $feed]);
    }

    /**
     * Display timeline for a profile.
     *
     * @param  string  $pseudo
     * @return \Illuminate\Http\Response
     */
    public function profile($pseudo)
    {
        $user = User::where('pseudo', '=', $pseudo)->firstOrFail();

        $activities = paginateCollection($user->activities, 25);
        abort_if($activities->isEmpty(), 204);

        return view('activity.profile', compact('activities'));
    }
}
