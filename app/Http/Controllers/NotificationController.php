<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DbHistory;

class NotificationController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'verified', 'only.ajax']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = auth()->user()->notifications()->paginate(10);
        return view('notification.index', compact('notifications'));
    }

    /**
     * Get the number of notifications
     */
    public function notified() {
        $count = auth()->user()->unreadNotifications()->count();
        return json_encode(['unread' => $count]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notification = auth()->user()->notifications->where('id', $id)->first();

        if(!isset($notification)) {
            return abort(404);
        }

        $history = null;

        // If notification is TMDb import
        // If DbHistory model exists fetch data
        if(class_basename($notification->type) == 'TmdbImport' && isset($notification->data['db_history'])) {
            // Fetch the db history related entry
            if(isset(json_decode(DbHistory::findOrFail($notification->data['db_history'])->data, true)['created'])) {
                $history = json_decode(DbHistory::findOrFail($notification->data['db_history'])->data, true)['created'];
            } elseif(isset(json_decode(DbHistory::findOrFail($notification->data['db_history'])->data, true)['updated'])) {
                $history = json_decode(DbHistory::findOrFail($notification->data['db_history'])->data, true)['updated'];
            }
            // Get all created models
            $history = collect($history)->map(function($entry) {
                return $entry['model']::find($entry['id']);
            });
        }

        return view('notification.show', compact(['notification', 'history']));
    }

    /**
     * Read the specified notification.
     *
     * @param  int|string(*)  $id
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        if($id == '*') {
            auth()->user()->unreadNotifications->markAsRead();
        } else {
            $notification = auth()->user()->notifications->where('id', $id)->first();

            if(!isset($notification)) {
                return abort(404);
            }

            $notification->markAsRead();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notification = auth()->user()->notifications->where('id', $id)->first();

        if(!isset($notification)) {
            return abort(404);
        }

        $notification->delete();
    }
}
