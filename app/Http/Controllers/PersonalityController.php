<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Str;
use Auth;
use App\Models\Country;
use App\Models\Personality;
use App\Models\Like;

class PersonalityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'only.ajax'])->only(['like', 'destroyLike']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personalities = Personality::join('likes', function($join) {
                            $join->on('likes.likeable_id', 'personalities.id')
                                 ->where('likes.likeable_type', '=', Personality::class)
                                 ->where('likes.type_id', '=', 'like');
                         })
                         ->selectRaw('personalities.*, count(*) as likes')
                         ->groupBy('personalities.id')
                         ->orderBy('likes', 'desc')
                         ->limit(20)
                         ->get();

        return view('personality.index', compact('personalities'));
    }

    /**
     * Return a random page
     *
     * @return \Illuminate\Http\Response
     */
    public function random()
    {
        $personality = Personality::random();

        return redirect()->route('personalities.show', $personality->slug);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();

        return view('personality.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'imdb_id' => 'required|string',
            'name' => 'required|string',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'birthday' => 'required|date',
            'deathday' => 'sometimes|nullable|date',
        ]);

        if($request->has('image')) {
            $imageName = Str::random(32) . '.' . $request->image->extension();
            $request->image->move(storage_path('app/images'), $imageName);
        }

        // if imdb not null verify if exists

        $personality = Personality::create($request->all());

        foreach($request->akas as $aka) {
            $personality->akas()->create(['name' => $aka]);
        }

        foreach($request->countries as $country) {
            $country = Country::find($country);
            $personality->countries()->save($country);
        }

        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $personality = Personality::findBySlugOrFail($slug);

        return view('personality.show', compact('personality'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
