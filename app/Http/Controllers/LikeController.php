<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Like;
use App\Models\Image;
use App\Models\Personality;
use App\Models\Company;
use App\Models\FilmList;
use App\Models\Activity;
use Auth;

class LikeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Return model instance
     * @param String $model
     * @param int $id
     */
    private function getModel(String $model, $id)
    {
        if($model == 'image') {
            return Image::findOrFail($id);

        } elseif($model == 'personality') {
            return Personality::findOrFail($id);

        } elseif($model == 'company') {
            return Company::findOrFail($id);

        } else {
            // verify if model is not created by user (cannot like) and abort
            if($model == 'list') {
                $return = FilmList::findOrFail($id);
            } elseif($model == 'activity') {
                $return = Activity::findOrFail($id);
            }

            if(Auth::user()->id == $return->user->id) {
                abort(403);
            } else {
                return $return;
            }
        }
    }

    /**
     * Like
     * @param $model type of model
     * @param $id
     */
    public function like(String $model, $id)
    {
        $user = Auth::user();
        $activityType = $model;
        $model = $this->getModel($model, $id);

        $exists = $model->likes->where('user_id', '=', $user->id)->first();

        if($exists !== null) {
            // If like entry exists and is like exit
            if($exists->type_id == 'like') {
                return;
            // If like entry exists and is dislike type delete it
            } elseif($exists->type_id == 'dislike') {
                $exists->delete();
            }
        }

        $like = new Like;
        $like->type_id = 'like';
        $like->user()->associate($user);
        $like->likeable()->associate($model);
        $like->save();

        // Activities for feed
        if($activityType == 'personality') {
            saveActivity($user, $model, 'like_personality');
        } elseif($activityType == 'list') {
            saveActivity($user, $model, 'like_list');
        } elseif($activityType == 'company') {
            saveActivity($user, $model, 'like_company');
        }
    }

    /**
     * Dislike
     * @param $model type of model
     * @param $id
     */
    public function dislike(String $model, $id)
    {
        $user = Auth::user();
        $model = $this->getModel($model, $id);

        $exists = $model->likes->where('user_id', '=', $user->id)->first();

        if($exists !== null) {
            // If dislike entry exists and is like exit
            if($exists->type_id == 'dislike') {
                return;
            // If like entry exists and is dislike type delete it
            } elseif($exists->type_id == 'like') {
                $exists->delete();
            }
        }

        $like = new Like;
        $like->type_id = 'dislike';
        $like->user()->associate($user);
        $like->likeable()->associate($model);
        $like->save();
    }

    /**
     * Destroy like/dislike
     * @param $model type of model
     * @param $id
     */
    public function destroy(String $model, $id)
    {
        $user = Auth::user();
        $model = $this->getModel($model, $id);

        $exists = $model->likes->where('user_id', '=', $user->id)->first();

        if($exists !== null) {
            $exists->delete();
        }
    }
}
