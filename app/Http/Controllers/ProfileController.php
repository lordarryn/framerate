<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Genre;
use App\Models\FilmList;
use App\Models\Film;
use Str;
use Auth;
use Storage;
use Hash;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth'])->except(['show']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return view('profile.index');
    }

    /**
     * Update user.
     */
    public function update(Request $request) {
        $this->validate($request, [
            'profile_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'background' => 'image|mimes:jpeg,png,jpg,svg|max:4096',
            'birthday' => 'nullable|date',
            'show_birthday' => '',
            'location' => 'nullable|string|max:191',
            'website' => 'nullable|url|max:191',
            'content' => 'nullable|json',
        ]);

        // handle bio request
        if(isset($request->content)) {
            $bio = lexer(json_decode($request->content, true));
            if($bio == '<p><br></p>') {
                $bio = '';
            }
        }

        // Save profile_image
        if($request->has('profile_image')) {
            $profileImage = Str::random(32) . '.' . $request->profile_image->extension();
            $request->profile_image->move(storage_path('app/images'), $profileImage);
        }
        // Save background
        if($request->has('background')) {
            $bgName = Str::random(32) . '.' . $request->background->extension();
            $request->background->move(storage_path('app/images'), $bgName);
        }

        $user = Auth::user();

        // if isset profile image delete old
        if(isset($profileImage)) {
            if(isset($user->profile_image)) {
                Storage::delete('images/' . $user->profile_image);
            }
            $user->profile_image = $profileImage;
        }
        // if isset background delete old
        if(isset($bgName)) {
            if(isset($user->background)) {
                Storage::delete('images/' . $user->background);
            }
            $user->background = $bgName;
        }

        $user->birthday      = $request->input('birthday');
        $user->show_birthday = $request->input('show_birthday') == 'on' ? true : false;
        $user->location      = $request->input('location');
        $user->website       = $request->input('website');
        $user->bio           = $bio;
        $user->save();

        $request->session()->flash('alert-info', 'Le profil a bien été mis à jour.');

        return back();
    }

    public function updateAccount(Request $request) {
        $this->validate($request, [
            'email' => 'nullable|email',
            'password' => 'nullable|string|min:8|confirmed',
        ]);

        $user = Auth::user();

        if(isset($request->email)) {
            $user->email = $request->email;
            $user->save();
        }

        if(isset($request->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
        }

        $request->session()->flash('alert-account', 'Le compte a bien été mis à jour.');

        return back();
    }

    /**
     * Show a profile.
     */
    public function show($pseudo)
    {
        $user = User::where('pseudo', '=', $pseudo)->firstOrFail();
        $genres = Genre::all();

        if(Auth::user() !== null) {
            $authVisit = ($user->id == Auth::user()->id);
        } else {
            $authVisit = null;
        }

        $top = $user->top;
        $activities = paginateCollection($user->activities, 25);
        $images = paginateCollection($user->images, 25);

        return view('profile.show', compact(['user', 'genres', 'authVisit', 'top', 'activities', 'images']));
    }

    /**
     * Follow an user
     */
    public function follow($pseudo, Request $request) {
        $this->validate($request, [
            'follow' => 'required|in:0,1'
        ]);

        $auth = Auth::user();
        $user = User::where('pseudo', '=', $pseudo)->firstOrFail();

        if($auth->id == $user->id) {
            return back();
        }

        if($request->follow == '1') {
            saveActivity($auth, $user, 'follow');
            $auth->following()->attach($user);
        } elseif($request->follow == '0') {
            $auth->following()->detach($user);
        }

        return back();
    }
}
