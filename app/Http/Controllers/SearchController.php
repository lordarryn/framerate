<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Personality;
use App\Models\Company;
use App\Models\Employment;
use App\Models\Department;

use DB;
class SearchController extends Controller
{
    /**
     * Searching function
     */
    public function search(Request $request)
    {DB::enableQueryLog();
        $input = $request->input('q');
        $type = ($request->input('type') !== null && in_array($request->input('type'), ['tout', 'film', 'realisateur', 'acteur', 'personnalite', 'societe'])) ?
                $request->input('type') : 'tout';

        $results = collect();

        if(empty($input)) {
            $results = paginateCollection($results);
            return view('search.search', compact(['input', 'type', 'results']));
        }

        if(in_array($type, ['tout', 'film'])) {
            $films = Film::where('title', 'like', '%'.$input)
                         ->orWhere('title', 'like', $input.'%')
                         ->orWhere('original_title', 'like', '%'.$input)
                         ->orWhere('original_title', 'like', $input.'%')
                         ->orWhere('imdb_id', 'like', $input)
                         ->get();

            $results = $results->merge($films);
        }

        if($type == 'realisateur') {
            $directors = Personality::join('works', 'works.personality_id', '=', 'personalities.id')
                                    ->where('works.employment_id', '=', Employment::where('name', '=', 'Director')
                                        ->where('department_id', '=', Department::where('name', '=', 'Directing')->first()->id)
                                        ->first()->id)
                                    ->where(function($q) use($input) {
                                        $q->where('personalities.name', 'like', '%'.$input)
                                          ->orWhere('personalities.name', 'like', $input.'%')
                                          ->orWhere('imdb_id', 'like', $input);
                                    })
                                    ->get()
                                    ->groupBy('personality_id')
                                    ->map(function ($item, $key) {
                                        return $item->first();
                                    });

            $results = $results->merge($directors);
        }

        if($type == 'acteur') {
            $actors = Personality::join('works', 'works.personality_id', '=', 'personalities.id')
                                 ->where('works.employment_id', '=', Employment::where('name', '=', 'Actor')
                                     ->where('department_id', '=', Department::where('name', '=', 'Actors')->first()->id)
                                     ->first()->id)
                                 ->where(function($q) use($input) {
                                     $q->where('personalities.name', 'like', '%'.$input)
                                       ->orWhere('personalities.name', 'like', $input.'%')
                                       ->orWhere('imdb_id', 'like', $input);
                                 })
                                 ->get()
                                 ->groupBy('personality_id')
                                 ->map(function ($item, $key) {
                                     return $item->first();
                                 });

            $results = $results->merge($actors);
        }

        if(in_array($type, ['tout', 'personnalite'])) {
            $personalities = Personality::where('name', 'like', '%'.$input)
                                        ->orWhere('name', 'like', $input.'%')
                                        ->orWhere('imdb_id', 'like', $input)
                                        ->get();

            $results = $results->merge($personalities);
        }

        if(in_array($type, ['tout', 'societe'])) {
            $companies = Company::where('name', 'like', '%'.$input)
                                ->orWhere('name', 'like', $input.'%')
                                ->get();

            $results = $results->merge($companies);
        }

        $results = $results->sortBy('created_at');
        $results = paginateCollection($results);

        return view('search.search', compact(['input', 'type', 'results']));
    }
}
