<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name'];

    public function employments() {
        return $this->hasMany('App\Models\Employment');
    }

    public function works() {
        return $this->hasManyThrought('App\Models\Work', 'App\Models\Employment');
    }
}
