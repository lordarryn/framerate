<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function companies() {
        return $this->morphedByMany('App\Models\Company');
    }

    public function films() {
        return $this->morphedByMany('App\Models\Film', 'countriable');
    }

    public function personalities() {
        return $this->morphedByMany('App\Models\Personality', 'countriable');
    }
}
