<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $fillable = [
        'role',
        'order'
    ];

    public function personality() {
        return $this->belongsTo('App\Models\Personality');
    }

    public function film() {
        return $this->belongsTo('App\Models\Film');
    }

    public function employment() {
        return $this->belongsTo('App\Models\Employment');
    }
}
