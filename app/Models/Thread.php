<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $fillable = [
        'title',
        'content',
        'status',
        'flag'
    ];

    public function replies() {
        return $this->hasMany('App\Models\Reply');
    }

    public function latestReply() {
        return $this->hasOne('App\Models\Reply')->latest();
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function forum() {
        return $this->belongsTo('App\Models\Forum');
    }
}
