<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'url', 'media_type'
    ];

    public function imageable() {
        return $this->morphTo();
    }

    public function likes() {
        return $this->morphMany('App\Models\Like', 'likeable');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}
