<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public function films() {
        return $this->belongsToMany('App\Models\Film');
    }
}
