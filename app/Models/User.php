<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\RatingsTrait;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, RatingsTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pseudo', 'email', 'password', 'last_active_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_active_at' => 'datetime',
    ];

    // Followers
    public function followers() {
        return $this->belongsToMany(self::class, 'follows', 'following_id', 'follower_id');
    }

    // Followings
    public function following() {
        return $this->belongsToMany(self::class, 'follows', 'follower_id', 'following_id');
    }

    // likes (lists, personalities...)
    public function likes() {
        return $this->hasMany('App\Models\Like');
    }

    // Activities (rate, like, follow, etc.)
    public function activities() {
        return $this->hasMany('App\Models\Activity')
                    ->orderBy('created_at', 'desc');
    }

    /**
     * Activities of people followed
     * @param User $user default null, fetch a particular feed
     * @param String $type type of feed fetched
     * @param int $pagination
     */
    public function feed(User $user = null, String $type = 'all', $pagination = 20) {
        $feed = Activity::join('follows', 'activities.user_id', '=', 'follows.following_id')
                        ->select('activities.*')
                        ->where('follows.follower_id', '=', $this->id)
                        ->orderBy('activities.created_at', 'desc');

        if(!is_null($user)) {
            $feed->where('follows.following_id', '=', $user->id);
        }

        if($type == 'list') {
            $ids[] = ActivityType::where('type', '=', 'create_list')->first()->id;
            $ids[] = ActivityType::where('type', '=', 'add_film_list')->first()->id;
            $ids[] = ActivityType::where('type', '=', 'add_film_top')->first()->id;
            $feed->whereIn('activities.activity_type_id', $ids);
        }

        return $feed->paginate($pagination);
    }

    // Lists
    public function lists() {
        return $this->hasMany('App\Models\FilmList')
                    ->where('is_top_user', false);
    }

    // Top 50
    public function top() {
        return $this->hasOne('App\Models\FilmList')
                    ->where('is_top_user', true);
    }

    // Images posted by user
    public function images() {
        // leftjoin sub then order by likes number
        $likes = Like::where('likes.likeable_type', '=', Image::class)
                     ->where('likes.type_id', '=', 'like')
                     ->selectRaw('likes.*, count(*) as likes_total')
                     ->groupBy('likes.likeable_id');

        return $this->hasMany('App\Models\Image')
                    ->leftJoinSub($likes, 'likes', function($join) {
                        $join->on('images.id', '=', 'likes.likeable_id');
                    })->selectRaw('images.*, likes.likes_total')
                    ->orderBy('likes_total', 'desc')
                    ->orderBy('created_at', 'desc');
    }

    /**
     * Film ratings
     */

    // ratings sorted by note
    public function ratingsByNote() {
        return $this->ratings->sortByDesc('rating');
    }

    // ratings by film release date
    public function ratingsByReleaseDate() {
        $ids = $this->ratings->pluck('id')->toArray();

        return Rating::join('films', 'ratings.film_id', '=', 'films.id')
                     ->whereIn('ratings.id', $ids)
                     ->select('ratings.*', 'films.release')
                     ->orderBy('films.release', 'desc')
                     ->get();
    }

    // ratings by film average
    public function ratingsByFilmAvg() {
        $ratings = $this->ratings->map(function($item, $key) {
            $film = Film::find($item->film_id);
            $item->average = $film->getAverage();
            return $item;
        });
        return $ratings->sortByDesc('average');
    }

    // ratings by film popularity
    public function ratingsByFilmPopularity() {
        $ratings = $this->ratings->map(function($item, $key) {
            $film = Film::find($item->film_id);
            $item->popularity = $film->ratings->count();
            return $item;
        });
        return $ratings->sortByDesc('popularity');
    }

    /***/


    public function dbHistories() {
        return $this->hasMany('App\Models\DbHistory');
    }

    /**
     * Get number countries watched
     */
    public function countries() {
        $countries = collect();
        $ratings = $this->ratings;

        $ratings->each(function($rating) use($countries) {
            foreach($rating->film->countries as $country) {
                if($countries->has($country->name)) {
                    $countries[$country->name] = $countries[$country->name]+1;
                }
                else {
                    $countries[$country->name] = 1;
                }
            }
        });

        return $countries->sortDesc();
    }
}
