<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scene extends Model
{
    protected $fillable = [
        'url'
    ];

    public function film() {
        return $this->belongsTo('App\Models\Film');
    }
}
