<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DbHistory extends Model
{
    protected $fillable = [
        'data',
        'isTmdbImport'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}
