<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Activity extends Model
{
    protected $fillable = ['data'];

    public function activitiable() {
        return $this->morphTo();
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function activityType() {
        return $this->belongsTo('App\Models\ActivityType');
    }

    /**
     * get film added to a list from activity
     */
    public function addedFilm() {
        $data = json_decode($this->data, true);

        if(isset($data['add_film_id'])) {
            $film = Film::find($data['add_film_id']);

            if(!is_null($film)) {
                return $film;
            }
        }
    }

    /**
     * For timeline purpose
     */
    public function isLastDailyActivity() {
        $groupedActivities = Activity::where('user_id', '=', $this->user->id)
                                     ->orderBy('created_at', 'desc')
                                     ->get()
                                     ->groupBy(function($item) {
                                        return $item->created_at->format('d-M-y');
                                     });
        $results = [];

        foreach($groupedActivities as $group) {
            $results[] = $group->first()->id;
        }

        return in_array($this->id, $results);
    }
}
