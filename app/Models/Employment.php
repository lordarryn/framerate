<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
    protected $fillable = ['name'];

    public function department() {
        return $this->belongsTo('App\Models\Department');
    }
}
