<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class FilmList extends Model
{
    use Sluggable, SluggableScopeHelpers;

    protected $fillable = [
        'user_id',
        'is_top_user',
        'title',
        'text'
    ];

    public function sluggable() {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $table = 'lists';

    public function likes() {
        return $this->morphMany('App\Models\Like', 'likeable');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function films() {
        return $this->belongsToMany('App\Models\Film', 'film_list', 'list_id', 'film_id')
                    ->withPivot('text', 'order')
                    ->orderBy('film_list.order');
    }
}
