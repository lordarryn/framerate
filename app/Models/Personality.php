<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use App\Traits\EmploymentsTrait;
use App\Traits\RandomTrait;
use DB;

use App\Models\Employment;
use App\Models\Department;
use App\Models\Film;
use App\Models\Rating;

class Personality extends Model
{
    use Sluggable, SluggableScopeHelpers, EmploymentsTrait, RandomTrait;

    protected $fillable = [
        'imdb_id',
        'name',
        'birthday',
        'deathday',
        'bio',
        'image',
        'place_of_birth',
        'slug',
        'sex'
    ];

    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function works() {
        return $this->hasMany('App\Models\Work');
    }

    public function akas() {
        return $this->hasMany('App\Models\Aka');
    }

    public function countries() {
        return $this->morphToMany('App\Models\Country', 'countriable');
    }

    public function images() {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    public function likes() {
        return $this->morphMany('App\Models\Like', 'likeable');
    }

    public function topRatedFilms(int $limit = 5)
    {
        $latestRating = Rating::select('id', 'user_id', 'film_id', DB::raw('MAX(created_at) AS latest'))
                              ->groupBy('user_id', 'film_id');

        $ratings = Rating::joinSub($latestRating, 'latest_rating', function($join) {
                            $join->on('ratings.id', '=', 'latest_rating.id');
                         })
                         ->select('ratings.*')
                         ->where('deleted', 'false');

        $films = Film::joinSub($ratings, 'ratings', function($join) {
                        $join->on('ratings.film_id', '=', 'films.id');
                     })
                     ->select('*', DB::raw('AVG(rating) as average'), DB::raw('COUNT(films.id) as number'))
                     ->join('works', function($join) {
                        $join->on('works.film_id', '=', 'films.id')
                             ->where('works.personality_id', '=', $this->id);
                     })
                     ->groupBy('films.id')
                     ->orderBy('average', 'desc')
                     ->limit($limit)
                     ->get();

        return $films;
    }
}
