<?php

namespace App\Models;

use Analytics as GA;
use Spatie\Analytics\Period;
use App\Models\Film;
use App\Models\Personality;

class Analytics
{
    private $analytics;

    public function __construct(int $days = 7, int $pages = 100)
    {
        $this->analytics = GA::fetchMostVisitedPages(Period::days($days), $pages);
    }

    /**
     * Return most popular movies visited
     */
    public function topFilms(int $number = 5)
    {
        $data = collect();

        foreach($this->analytics as $elem) {
            if(preg_match('/\/film\/(.*)/', $elem['url']) && $data->count() < 5) {
                preg_match('/\/film\/(.*)/', $elem['url'], $matches);

                $film = Film::findBySlug($matches['1']);

                if($film !== null) {
                    $data->push($film);
                }
            }
        }

        return $data;
    }

    /**
     * Return most popular persons visited
     */
    public function topPersonalities(int $number = 5)
    {
        $data = collect();

        foreach($this->analytics as $elem) {
            if(preg_match('/\/personnalite\/(.*)/', $elem['url']) && $data->count() < 5) {
                preg_match('/\/personnalite\/(.*)/', $elem['url'], $matches);

                $perso = Personality::findBySlug($matches['1']);

                if($perso !== null) {
                    $data->push($perso);
                }
            }
        }

        return $data;
    }
}
