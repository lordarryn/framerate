<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aka extends Model
{
    protected $fillable = ['name'];

    public function personality() {
        return $this->belongsTo('App\Models\Personality');
    }
}
