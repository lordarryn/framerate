<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use App\Traits\RandomTrait;

class Company extends Model
{
    use Sluggable, SluggableScopeHelpers, RandomTrait;

    protected $fillable = [
        'name',
        'logo',
        'slug'
    ];

    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function likes() {
        return $this->morphMany('App\Models\Like', 'likeable');
    }

    public function country() {
        return $this->belongsTo('App\Models\Country');
    }

    public function films() {
        return $this->belongsToMany('App\Models\Film')->orderBy('release', 'desc');
    }
}
