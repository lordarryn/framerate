<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = ['rating'];

    public function film() {
        return $this->belongsTo('App\Models\Film');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}
