<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    protected $fillable = ['name'];

    public function threads() {
        return $this->hasMany('App\Models\Thread');
    }

    public function replies() {
        return $this->hasManyThrough('App\Models\Reply');
    }
}
