<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use App\Models\Employment;
use App\Models\Image;
use App\Models\Like;
use App\Traits\EmploymentsTrait;
use App\Traits\RandomTrait;
use App\Traits\RatingsTrait;
use Auth;

class Film extends Model
{
    use Sluggable, SluggableScopeHelpers, EmploymentsTrait, RandomTrait, RatingsTrait;

    protected $fillable = [
        'imdb_id',
        'title',
        'original_title',
        'duration',
        'release',
        'aspect',
        'budget',
        'overview',
        'poster',
        'backdrop',
        'poster_thumbnail',
        'slug',
        'release_year'
    ];

    public function sluggable() {
        return [
            'slug' => [
                'source' => ['title', 'release_year']
            ]
        ];
    }

    // Average of the film based on the ratings
    public function getAverage() {
        return $this->ratings->avg('rating');
    }

    // Note of the logged user
    public function authRating() {
        if(Auth::user() === null) {
            return;
        }

        $rating = $this->ratings
                       ->where('user_id', '=', Auth::user()->id)
                       ->first();

        return ($rating === null) ? null : $rating->rating;
    }

    // Note of an user
    public function userRating(User $user) {
        $rating = $this->ratings
                       ->where('user_id', '=', $user->id)
                       ->first();

        return ($rating === null) ? null : $rating->rating;
    }

    // top films ordered by average, filtered by release year, country, genre
    public static function topFilms($year = null,
                                    Country $country = null,
                                    Genre $genre = null,
                                    $limit = 50,
                                    $minimumRatings = 2) {
        $top = Film::join('ratings', 'ratings.film_id', '=', 'films.id')
                   ->withCount('ratings')
                   ->having('ratings_count', '>=', $minimumRatings)
                   ->selectRaw('films.*, AVG(rating) as average')
                   ->where('ratings.deleted', false)
                   ->where('ratings.updated', false)
                   ->groupBy('ratings.film_id')
                   ->orderBy('average', 'desc')
                   ->limit($limit);

        if(!is_null($year)) {
            $top->where('films.release_year', '=', $year);
        }

        if(!is_null($country)) {
            $top->join('countriables', 'films.id', '=', 'countriables.countriable_id')
                ->where('countriables.country_id', '=', $country->id);
        }

        if(!is_null($genre)) {
            $top->join('film_genre', 'films.id', '=', 'film_genre.film_id')
                ->where('film_genre.genre_id', '=', $genre->id);
        }

        return $top->get();
    }

    // lists where the film is
    public function lists() {
        return $this->belongsToMany('App\Models\FilmList', 'film_list', 'film_id', 'list_id')
                    ->where('is_top_user', false)
                    ->withPivot('text', 'order')
                    ->orderBy('film_list.order');
    }

    // tops 50 where the film is
    public function tops() {
        return $this->belongsToMany('App\Models\FilmList', 'film_list', 'film_id', 'list_id')
                    ->where('is_top_user', true)
                    ->withPivot('text', 'order')
                    ->orderBy('film_list.order');
    }

    public function images() {
        // leftjoin sub then order by likes number
        $likes = Like::where('likes.likeable_type', '=', Image::class)
                     ->where('likes.type_id', '=', 'like')
                     ->selectRaw('likes.*, count(*) as likes_total')
                     ->groupBy('likes.likeable_id');

        return $this->morphMany('App\Models\Image', 'imageable')
                    ->leftJoinSub($likes, 'likes', function($join) {
                        $join->on('images.id', '=', 'likes.likeable_id');
                    })->selectRaw('images.*, likes.likes_total')
                    ->orderBy('likes_total', 'desc')
                    ->orderBy('created_at', 'desc');
    }

    public function works() {
        return $this->hasMany('App\Models\Work');
    }

    public function scenes() {
        return $this->hasMany('App\Models\Scene');
    }

    public function companies() {
        return $this->belongsToMany('App\Models\Company');
    }

    public function genres() {
        return $this->belongsToMany('App\Models\Genre');
    }

    public function languages() {
        return $this->belongsToMany('App\Models\Language');
    }

    public function countries() {
        return $this->morphToMany('App\Models\Country', 'countriable');
    }
}
