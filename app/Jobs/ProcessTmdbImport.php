<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Notifications\TmdbImport;

use Illuminate\Http\Client\ConnectionException;
use Carbon\Carbon;
use Http;
use Str;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Film;
use App\Models\Genre;
use App\Models\Country;
use App\Models\Personality;
use App\Models\Language;
use App\Models\Company;
use App\Models\Aka;
use App\Models\Work;
use App\Models\Employment;
use App\Models\Department;
use App\Models\DbHistory;

class ProcessTmdbImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $type;
    protected $id;
    protected $id_type;

    private $errors = array();
    private $infos  = array();
    private $notify = true;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, String $type, String $id, String $id_type = 'imdb')
    {
        $this->user = $user;
        $this->type = $type;
        $this->id = $id;
        $this->id_type = $id_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->type == 'personality') {
            $this->importPersonalityFilms($this->id);
        } elseif($this->type == 'film') {
            $history = null;

            $this->getFilm($this->id);

            // If entries created create db history entry of created models
            if(!empty($this->infos)) {
                $history = new DbHistory();
                $history->data = ($this->id_type == 'update') ? json_encode(['updated' => $this->infos]) : json_encode(['created' => $this->infos]);
                $history->isTmdbImport = 1;
                $history->user()->associate($this->user);
                $history->save();
            }

            // Notification user with errors or entry created
            if($this->notify) {
                $this->user->notify(new TmdbImport($this->id, $this->errors, $history));
            } else {
                $this->notify = true;
            }
        }
    }

    /**
     * Import movies from personnality
     * recursive process
     */
    private function importPersonalityFilms($id) {
        $data = $this->request(config('tmdb.find_base_url') . $id . '?api_key=' . config('tmdb.api_key') . config('tmdb.find_end_url'));

        // Person not found
        if(isset($data['status_code']) && $data['status_code'] == 34) {
            return $this->error('Personnalité non trouvée.');
        }

        if(!isset($data['person_results']['0']['id'])) {
            return $this->error('Personnalité non trouvée.');
        }

        $data = $this->request(config('tmdb.people_base_url') . $data['person_results']['0']['id'] . config('tmdb.people_movie_credits') . '?api_key=' . config('tmdb.api_key') . config('tmdb.find_end_url'));
        $imports = array();

        foreach($data['cast'] as $array) {
            $imports[] = $array['id'];
        }
        foreach($data['crew'] as $array) {
            $imports[] = $array['id'];
        }

        // Process async job
        foreach($imports as $import) {
            ProcessTmdbImport::dispatch($this->user, 'film', $import, $id);
        }
    }

    /**
     * Add an error
     */
    private function error(String $msg) {
        $this->errors[] = $msg;
    }

    /**
     * Add model to notify and as historical
     */
    private function info($model) {
        $this->infos[] = [
            'model' => get_class($model),
            'id'    => $model->id
        ];
    }

    /**
     * Http request
     */
    private function request($url, $type = 'json')
    {
        try {
            if($type == 'json') {
                $data = Http::get($url)->json();
            } elseif($type == 'image') {
                $data = Http::get($url);
            }

            if(isset($data['status_code']) && $data['status_code'] == 7) {
                return $this->error($data['status_message']);
            } else {
                return $data;
            }
        } catch(ConnectionException $e) {
            return $this->error($e->getMessage());
        }
    }

    /**
     * Get the personality
     */
    private function getPersonality($id)
    {
        $data = $this->request(config('tmdb.people_base_url') . $id . '?api_key=' . config('tmdb.api_key') . config('tmdb.people_end_url'));

        if(isset($data['status_code']) && $data['status_code'] == 34) {
            return $this->error('Personalité non trouvée.');
        }

        $personality = Personality::where('imdb_id', '=', $data['imdb_id'])
                                  ->where('name', '=', $data['name'])
                                  ->get()->first();

        if($personality !== null) {
            return $personality;
        }

        if(isset($data['profile_path'])) {
            $profile = config('tmdb.image_base_url') . config('tmdb.profile_size') . $data['profile_path'];
        } else {
            $profile = null;
        }

        $personality = Personality::create([
            'imdb_id' => $data['imdb_id'],
            'name' => $data['name'],
            'birthday' => $data['birthday'],
            'deathday' => $data['deathday'],
            'bio' => $data['biography'],
            'image' => $profile,
            'place_of_birth' => $data['place_of_birth'],
            'sex' => $data['gender']
        ]);

        $this->info($personality);

        foreach($data['also_known_as'] as $peopleAka) {
            $personality->akas()->create(['name' => $peopleAka]);
        }

        return $personality;
    }

    /**
     * Get the movie
     */
    private function getFilm($id)
    {
        if($this->id_type == 'imdb') {
            if(Film::where('imdb_id', '=', $id)->exists()) {
                return $this->error('L\'ID du film est déjà dans la base de données.');
            }
        }

        if($this->id_type == 'update') {
            if(!Film::where('imdb_id', '=', $id)->exists()) {
                return $this->error('L\'ID du film n\'est pas dans la base de données.');
            }
        }

        $data = $this->request(config('tmdb.movie_base_url') . $id . '?api_key=' . config('tmdb.api_key') . config('tmdb.movie_end_url'));

        // If import personnality movies data
        if($this->id_type != 'imdb' && $this->id_type != 'update') {
            // If movie already id DB (isset IMDb ID)
            if(isset($data['imdb_id']) && !empty($data['imdb_id']) && Film::where('imdb_id', '=', $data['imdb_id'])->exists()) {
                $this->notify = false;
                return $this->error('L\'ID du film est déjà dans la base de données.');
            }
            // Else if no IMDb data, check if title, release date from related personnality movie exists
            elseif(isset($data['title']) && isset($data['release_date'])) {
                $film = Film::join('works', 'works.film_id', '=', 'films.id')
                            ->join('personalities', 'personalities.id', 'works.personality_id')
                            ->where('films.title', '=', $data['title'])
                            ->where('films.release', '=', $data['release_date'])
                            ->where('personalities.imdb_id', '=', $this->id_type)
                            ->exists();

                if($film) {
                    $this->notify = false;
                    return $this->error('Le film est déjà présent dans la base de données (même titre, même date de sortie et même personne y travaillant).');
                }
            }
        }

        // Movie not found
        if(isset($data['status_code']) && $data['status_code'] == 34) {
            return $this->error('Film non trouvé.');
        }

        if(isset($data['poster_path'])) {
            $poster = config('tmdb.image_base_url') . config('tmdb.poster_size') . $data['poster_path'];
            $posterThumbnail = config('tmdb.image_base_url') . config('tmdb.poster_thumbnail') . $data['poster_path'];
        } else {
            $poster = null;
            $posterThumbnail = null;
        }

        if(isset($data['backdrop_path'])) {
            $backdrop = config('tmdb.image_base_url') . config('tmdb.backdrop_size') . $data['backdrop_path'];
        } else {
            $backdrop = null;
        }

        // If title in other language, scrap the english version
        if(empty($data['title'])) {
            $data['title'] = $this->request(config('tmdb.movie_base_url') . $id . '?api_key=' . config('tmdb.api_key') . config('tmdb.movie_en_end_url'))['title'];
        }

        if(!empty($data['release_date'])) {
            $year = (new Carbon($data['release_date']))->year;
        } else {
            $year = null;
        }

        if($this->id_type == 'imdb' || $this->id_type == 'update') {
            $imdb_id = $id;
        } else {
            $imdb_id = isset($data['imdb_id']) ? $data['imdb_id'] : null;
        }

        if($this->id_type != 'update') {
            $film = Film::create([
                'imdb_id' => $imdb_id,
                'title' => $data['title'],
                'original_title' => isset($data['original_title']) ? $data['original_title'] : null,
                'duration' => $data['runtime'],
                'release' => !empty($data['release_date']) ? $data['release_date'] : null,
                // 'aspect'
                'budget' => $data['budget'] == 0 ? null : $data['budget'],
                'overview' => isset($data['overview']) ? $data['overview'] : null,
                'poster' => $poster,
                'poster_thumbnail' => $posterThumbnail,
                'backdrop' => $backdrop,
                'release_year' => $year
            ]);
        } elseif($this->id_type == 'update') {
            $film = Film::where('imdb_id', '=', $imdb_id)->first();

            $film->works()->delete();
            $film->companies()->detach();
            $film->genres()->detach();
            $film->languages()->detach();
            $film->countries()->detach();

            $film->title = $data['title'];
            $film->original_title = isset($data['original_title']) ? $data['original_title'] : null;
            $film->duration = $data['runtime'];
            $film->release = !empty($data['release_date']) ? $data['release_date'] : null;
            $film->budget = $data['budget'] == 0 ? null : $data['budget'];
            $film->overview = isset($data['overview']) ? $data['overview'] : null;
            $film->poster = $poster;
            $film->poster_thumbnail = $posterThumbnail;
            $film->backdrop = $backdrop;
            $film->release_year = $year;
            $film->save();
        }

        $this->info($film);

        // Genres
        if($data['adult']) {
            $genre = Genre::where('name', '=', 'Adult')->first();
            $film->genres()->save($genre);
        }

        if($data['runtime'] != 0 && $data['runtime'] < 30) {
            $genre = Genre::where('name', '=', 'Short')->first();
            $film->genres()->save($genre);
        }

        foreach($data['genres'] as $dataGenre) {
            $genre = Genre::where('name', '=', config('tmdb.'.$dataGenre['name']))->first();
            if(isset($genre)) {
                $film->genres()->save($genre);
            }
        }

        // Countries
        foreach($data['production_countries'] as $dataCountry) {
            $country = Country::where('code', '=', $dataCountry['iso_3166_1'])->first();
            if(isset($country)) {
                $film->countries()->save($country);
            }
        }

        // Languages
        foreach($data['spoken_languages'] as $dataLang) {
            $lang = Language::where('iso', '=', $dataLang['iso_639_1'])->first();
            if(isset($lang)) {
                $film->languages()->save($lang);
            }
        }

        // Production companies
        foreach($data['production_companies'] as $dataCompany) {
            $company = Company::where('name', '=', $dataCompany['name'])->first();

            // If company already exists
            if($company !== null) {
                $film->companies()->save($company);
            }
            // If company doesn't exit, create it
            else {
                if(isset($dataCompany['logo_path'])) {
                    $logo = config('tmdb.image_base_url') . config('tmdb.logo_size') . $dataCompany['logo_path'];
                } else {
                    $logo = null;
                }

                $company = Company::create([
                    'name' => $dataCompany['name'],
                    'logo' => $logo
                ]);

                $this->info($company);

                if(isset($dataCompany['origin_country'])) {
                    $country = Country::where('code', '=', $dataCompany['origin_country'])->first();
                    $company->country()->associate($country);
                    $company->save();
                }

                $film->companies()->save($company);
            }
        }

        // Personalities
        foreach($data['credits'] as $credit => $dataCasts) {

            // Cast or crew
            // Cast max $i = 50, crew max $j = 200
            $i = 0;
            $j = 0;
            foreach($dataCasts as $test_nb => $dataCast) {

                if($credit == 'cast') {
                    $i++;
                    if($i > 50) {
                        break;
                    }
                } elseif($credit == 'crew') {
                    $j++;
                    if($j > 200) {
                        break;
                    }
                }

                $personality = $this->getPersonality($dataCast['id']);

                if($personality === null) {
                    break;
                }

                // Connect the personality to the movie
                if($credit == 'cast') {

                    $work = new Work([
                        'role' => $dataCast['character'],
                        'order' => $dataCast['order'],
                    ]);

                    $work->employment()->associate(Employment::where('name', '=', 'Actor')->first());
                    $work->personality()->associate($personality);
                    $work->film()->associate($film);
                    $work->save();

                    $this->info($work);

                } elseif($credit == 'crew') {

                    $job = Employment::where('name', '=', $dataCast['job'])
                                     ->where('department_id', '=', Department::where('name', '=', $dataCast['department'])->first()->id)
                                     ->get()->first();

                    // If the job is in the database, add it to the people
                    if(!is_null($job)) {
                        $work = new Work();
                        $work->employment()->associate($job);
                        $work->personality()->associate($personality);
                        $work->film()->associate($film);
                        $work->save();
                    }

                    $this->info($work);
                }
            }
        }
    }
}
