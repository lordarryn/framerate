<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\DbHistory;

class TmdbImport extends Notification
{
    use Queueable;

    private $movie_id;
    private $errors;
    private $history;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(String $id, Array $errors, DbHistory $history = null)
    {
        $this->movie_id = $id;
        $this->errors  = $errors;
        $this->history = $history;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id'         => $this->movie_id,
            'errors'     => $this->errors,
            'db_history' => isset($this->history) ?  $this->history->id : null
        ];
    }
}
