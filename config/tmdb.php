<?php

return [
    // api key
    'api_key' => '446d43758561aeb240285bb261288cb4',

    // movie api
    'movie_base_url' => 'https://api.themoviedb.org/3/movie/',
    'movie_external_ids' => '/external_ids',
    'movie_end_url' => '&language=fr-FR&append_to_response=credits',
    'movie_en_end_url' => '&language=en-US',

    // people api
    'people_base_url' => 'https://api.themoviedb.org/3/person/',
    'people_movie_credits' => '/movie_credits',
    'people_end_url' => '&language=fr-FR',

    // find api
    'find_base_url' => 'https://api.themoviedb.org/3/find/',
    'find_end_url' => '&language=fr-FR&external_source=imdb_id',

    // image api
    'image_base_url' => 'https://image.tmdb.org/t/p/',

    // image sizes
    'backdrop_size' => 'w1280',
    'poster_size' => 'w500',
    'poster_thumbnail' => 'w185',
    'profile_size' => 'w185',
    'logo_size' => 'w500',

    // genres fr-FR to us-US
    "Action" => "Action",
    "Aventure" => "Adventure",
    "Animation" => "Animation",
    "Comédie" => "Comedy",
    "Crime" => "Crime",
    "Documentaire" => "Documentary",
    "Drame" => "Drama",
    "Familial" => "Family",
    "Fantastique" => "Fantasy",
    "Histoire" => "History",
    "Horreur" => "Horror",
    "Musique" => "Musical",
    "Mystère" => "Mystery",
    "Romance" => "Romance",
    "Science-Fiction" => "Sci-Fi",
    "Téléfilm" => "TV Movie",
    "Thriller" => "Thriller",
    "Guerre" => "War",
    "Western" => "Western",
];
