require('./bootstrap');

$(function() {

    /**
     * Notifications
     */
    if(!guest) {
        (function notify() {
            $.ajax({
                url: notificationsRoute,
                type: "GET",
                success: function(data) {
                    n = JSON.parse(data).unread;

                    if(n > 0) {
                        $("a[data-id=notifications] i").addClass("text-danger");
                        $(".notification-bell").show(200);

                        if(n < 10) {
                            $("a[data-id=notifications] p").text("Notifications ("+n+")");
                        } else {
                            $("a[data-id=notifications] p").text("Notifications (+9)");
                        }
                    } else {
                        $("a[data-id=notifications] i").removeClass("text-danger");
                        $("a[data-id=notifications] p").text("Notifications");
                        $(".notification-bell").hide();
                    }
                }
            })
            setTimeout(notify, 2000);
        })();
    }

    /**
     * Responsive nav
     */
    var opened = false;

    $('#menu-responsive .fa-times').hide();
    $('#navigation-mobile').hide();

    $('#menu-responsive').click(function(e) {
        e.preventDefault();

        if(!opened) {
            opened = true;

            $('#menu-responsive').find('.fa-bars').fadeOut(100, 'linear', function() {
                $('#menu-responsive').find('.fa-times').fadeIn(100, 'linear');
            });

            $('#navigation-mobile').show("slide");
        } else {
            opened = false;

            $('#menu-responsive').find('.fa-times').fadeOut(100, 'linear', function() {
                $('#menu-responsive').find('.fa-bars').fadeIn(100, 'linear');
            });

            $('#navigation-mobile').hide("slide");
        }
    });

    $('#navigation-mobile ul.menu > li').hover(function() {
        $(this).find('> a').toggleClass('dropdown');
    });

    /**
     * Logging out
     */
    $('.logout-btn').click(function(e) {
        e.preventDefault();
        $('#logout-form').submit();
    });

    /**
     * Smooth scroll to top
     */
     $('#scroll-main').on('click', function(e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $('#main').offset().top
        }, 800, function(){
            window.location.hash = hash;
        });
     });

     var faded = false;

     $(window).scroll(function() {
        var y = $(window).scrollTop();

        if(y > 500 && !faded) {
            faded = true;
            $('#scroll-main').show(100);
        } else if(y <= 500 && faded) {
            faded = false;
            $('#scroll-main').hide(100);
        }
     });

});
