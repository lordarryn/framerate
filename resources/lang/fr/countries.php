<?php

return [
    'United States' => 'États-Unis',
    'Canada' => 'Canada',
    'Afghanistan' => 'Afghanistan',
    'Albania' => 'Albanie',
    'Algeria' => 'Algérie',
    'American Samoa' => 'Samoa',
    'Andorra' => 'Andorre',
    'Angola' => 'Angola',
    'Anguilla' => 'Anguilla',
    'Antarctica' => 'Antarctique',
    'Antigua and/or Barbuda' => 'Antigua-et-Barbuda',
    'Argentina' => 'Argentine',
    'Armenia' => 'Arménie',
    'Aruba' => 'Aruba',
    'Australia' => 'Australie',
    'Austria' => 'Autriche',
    'Azerbaijan' => 'Azerbaïdjan',
    'Bahamas' => 'Bahamas',
    'Bahrain' => 'Bahreïn',
    'Bangladesh' => 'Bangladesh',
    'Barbados' => 'Barbade',
    'Belarus' => 'Biélorussie',
    'Belgium' => 'Belgique',
    'Belize' => 'Belize',
    'Benin' => 'Bénin',
    'Bermuda' => 'Bermudes',
    'Bhutan' => 'Bhoutan',
    'Bolivia' => 'Bolivie',
    'Bosnia and Herzegovina' => 'Bosnie-Herzégovine',
    'Botswana' => 'Botswana',
    'Bouvet Island' => 'Île Bouvet',
    'Brazil' => 'Brésil',
    'British lndian Ocean Territory' => 'Territoire britannique de l\'océan Indien',
    'Brunei Darussalam' => 'Brunéi',
    'Bulgaria' => 'Bulgarie',
    'Burkina Faso' => 'Burkina',
    'Burundi' => 'Burundi',
    'Cambodia' => 'Cambodge',
    'Cameroon' => 'Cameroun',
    'Cape Verde' => 'Cap-Vert',
    'Cayman Islands' => 'Îles Caïmans',
    'Central African Republic' => 'République centrafricaine',
    'Chad' => 'Tchad',
    'Chile' => 'Chili',
    'China' => 'Chine',
    'Christmas Island' => 'Île Christmas',
    'Cocos (Keeling) Islands' => 'Îles Cocos',
    'Colombia' => 'Colombie',
    'Comoros' => 'Comores',
    'Congo' => 'Congo',
    'Cook Islands' => 'Îles Cook',
    'Costa Rica' => 'Costa Rica',
    'Croatia (Hrvatska)' => 'Croatie',
    'Cuba' => 'Cuba',
    'Cyprus' => 'Chypre',
    'Czech Republic' => 'République tchèque',
    'Democratic Republic of Congo' => 'République démocratique du Congo',
    'Denmark' => 'Danemark',
    'Djibouti' => 'Djibouti',
    'Dominica' => 'Dominique',
    'Dominican Republic' => 'République dominicaine',
    'East Timor' => 'Timor oriental',
    'Ecudaor' => 'Équateur',
    'Egypt' => 'Égypte',
    'El Salvador' => 'Salvador',
    'Equatorial Guinea' => 'Guinéee équatorienne',
    'Eritrea' => 'Érythrée',
    'Estonia' => 'Estonie',
    'Ethiopia' => 'Éthiopie',
    'Falkland Islands (Malvinas)' => 'Îles Malouines',
    'Faroe Islands' => 'Îles Féroé',
    'Fiji' => 'Fidji',
    'Finland' => 'Finlande',
    'France' => 'France',
    'France, Metropolitan' => 'France',
    'French Guiana' => 'Guyane française',
    'French Polynesia' => 'Polynésie française',
    'French Southern Territories' => 'TAAF',
    'Gabon' => 'Gabon',
    'Gambia' => 'Gambie',
    'Georgia' => 'Géorgie',
    'Germany' => 'Allemagne',
    'Ghana' => 'Ghana',
    'Gibraltar' => 'Gibraltar',
    'Greece' => 'Grèce',
    'Greenland' => 'Groenland',
    'Grenada' => 'Grenade',
    'Guadeloupe' => 'Guadeloupe',
    'Guam' => 'Guam',
    'Guatemala' => 'Guatemala',
    'Guinea' => 'Guinée',
    'Guinea-Bissau' => 'Guinée-Bissao',
    'Guyana' => 'Guyana',
    'Haiti' => 'Haïti',
    'Heard and Mc Donald Islands' => 'Îles Heard-et-MacDonald',
    'Honduras' => 'Honduras',
    'Hong Kong' => 'Hong-Kong',
    'Hungary' => 'Hongrie',
    'Iceland' => 'Islande',
    'India' => 'Inde',
    'Indonesia' => 'Indonésie',
    'Iran (Islamic Republic of)' => 'Iran',
    'Iraq' => 'Iraq',
    'Ireland' => 'Irlande',
    'Israel' => 'Israël',
    'Italy' => 'Italie',
    'Ivory Coast' => 'Côte d\'Ivoire',
    'Jamaica' => 'Jamaïque',
    'Japan' => 'Japon',
    'Jordan' => 'Jordanie',
    'Kazakhstan' => 'Kazakhstan',
    'Kenya' => 'Kenya',
    'Kiribati' => 'Kiribati',
    'Korea, Democratic People\'s Republic of' => 'Corée du Nord',
    'Korea, Republic of' => 'Corée du Sud',
    'Kuwait' => 'Koweït',
    'Kyrgyzstan' => 'Kirghizstan',
    'Lao People\'s Democratic Republic' => 'Laos',
    'Latvia' => 'Lettonie',
    'Lebanon' => 'Liban',
    'Lesotho' => 'Lesotho',
    'Liberia' => 'Libéria',
    'Libyan Arab Jamahiriya' => 'Lybie',
    'Liechtenstein' => 'Liechtenstein',
    'Lithuania' => 'Lituanie',
    'Luxembourg' => 'Luxembourg',
    'Macau' => 'Macau',
    'Macedonia' => 'Macédoine',
    'Madagascar' => 'Madagascar',
    'Malawi' => 'Malawi',
    'Malaysia' => 'Malaisie',
    'Maldives' => 'Maldives',
    'Mali' => 'Mali',
    'Malta' => 'Malte',
    'Marshall Islands' => 'Îles Marshall',
    'Martinique' => 'Martinique',
    'Mauritania' => 'Mauritanie',
    'Mauritius' => 'Île Maurice',
    'Mayotte' => 'Mayotte',
    'Mexico' => 'Mexique',
    'Micronesia, Federated States of' => 'Micronésie',
    'Moldova, Republic of' => 'Moldavie',
    'Monaco' => 'Monaco',
    'Mongolia' => 'Mongolie',
    'Montserrat' => 'Montserrat',
    'Morocco' => 'Maroc',
    'Mozambique' => 'Mozambique',
    'Myanmar' => 'Birmanie',
    'Namibia' => 'Namibie',
    'Nauru' => 'Nauru',
    'Nepal' => 'Népal',
    'Netherlands' => 'Pays-Bas',
    'Netherlands Antilles' => 'Antilles néerlandaises',
    'New Caledonia' => 'Nouvelle-Calédonie',
    'New Zealand' => 'Nouvelle-Zélande',
    'Nicaragua' => 'Nicaragua',
    'Niger' => 'Niger',
    'Nigeria' => 'Nigéria',
    'Niue' => 'Niué',
    'Norfork Island' => 'Île Norfolk',
    'Northern Mariana Islands' => 'Îles Mariannes du Nord',
    'Norway' => 'Norvège',
    'Oman' => 'Oman',
    'Pakistan' => 'Pakistan',
    'Palau' => 'Palau',
    'Panama' => 'Panama',
    'Papua New Guinea' => 'Papouasie-Nouvelle-Guinée',
    'Paraguay' => 'Paraguay',
    'Peru' => 'Pérou',
    'Philippines' => 'Philippines',
    'Pitcairn' => 'Îles Pitcairn',
    'Poland' => 'Pologne',
    'Portugal' => 'Portugal',
    'Puerto Rico' => 'Porto Rico',
    'Qatar' => 'Qatar',
    'Republic of South Sudan' => 'Soudan du Sud',
    'Reunion' => 'La Réunion',
    'Romania' => 'Roumanie',
    'Russian Federation' => 'Russie',
    'Rwanda' => 'Rwanda',
    'Saint Kitts and Nevis' => 'Saint-Christophe-et-Niévès',
    'Saint Lucia' => 'Sainte-Lucie',
    'Saint Vincent and the Grenadines' => 'Saint-Vincent-et-les-Grenadines',
    'Samoa' => 'Samoa',
    'San Marino' => 'Saint-Marin',
    'Sao Tome and Principe' => 'Sao Tomé et Principe',
    'Saudi Arabia' => 'Arabie saoudite',
    'Senegal' => 'Sénégal',
    'Serbia' => 'Serbie',
    'Seychelles' => 'Seychelles',
    'Sierra Leone' => 'Sierra Leone',
    'Singapore' => 'Singapour',
    'Slovakia' => 'Slovaquie',
    'Slovenia' => 'Slovénie',
    'Solomon Islands' => 'Îles Salomon',
    'Somalia' => 'Somalie',
    'South Africa' => 'Afrique du Sud',
    'South Georgia South Sandwich Islands' => 'Géorgie du Sud-et-les îles Sandwich du Sud',
    'Spain' => 'Espagne',
    'Sri Lanka' => 'Sri Lanka',
    'St. Helena' => 'St. Hélène',
    'St. Pierre and Miquelon' => 'St. Pierre et Miquelon',
    'Sudan' => 'Soudan',
    'Suriname' => 'Surinam',
    'Svalbarn and Jan Mayen Islands' => 'Îles Svalbard et Jan Mayen',
    'Swaziland' => 'Swaziland',
    'Sweden' => 'Suède',
    'Switzerland' => 'Suisse',
    'Syrian Arab Republic' => 'Syrie',
    'Taiwan' => 'Taïwan',
    'Tajikistan' => 'Tadjikistan',
    'Tanzania, United Republic of' => 'Tanzanie',
    'Thailand' => 'Thaïlande',
    'Togo' => 'Togo',
    'Tokelau' => 'Tokelau',
    'Tonga' => 'Tonga',
    'Trinidad and Tobago' => 'Trinité-et-Tobago',
    'Tunisia' => 'Tunisie',
    'Turkey' => 'Turquie',
    'Turkmenistan' => 'Turkménistan',
    'Turks and Caicos Islands' => 'Îles Turques-et-Caïques',
    'Tuvalu' => 'Tuvalu',
    'Uganda' => 'Ouganda',
    'Ukraine' => 'Ukraine',
    'United Arab Emirates' => 'Émirats arabes unis',
    'United Kingdom' => 'Royaume-Uni',
    'United States minor outlying islands' => 'Îles mineures éloignées des États-Unis',
    'Uruguay' => 'Uruguay',
    'Uzbekistan' => 'Ouzbékistan',
    'Vanuatu' => 'Vanuatu',
    'Vatican City State' => 'Vatican',
    'Venezuela' => 'Venezuela',
    'Vietnam' => 'Viêt-Nam',
    'Virgin Islands (British)' => 'Îles Vierges britanniques',
    'Virgin Islands (U.S.)' => 'Îles Vierges américaines',
    'Wallis and Futuna Islands' => 'Wallis et Futuna',
    'Western Sahara' => 'Sahara occidental',
    'Yemen' => 'Yémen',
    'Yugoslavia' => 'Yougoslavie',
    'Zaire' => 'Zaïre',
    'Zambia' => 'Zambie',
    'Zimbabwe' => 'Zimbabwe',
    'Palestine' => 'Paléstine',
    'Kosovo' => 'Kosovo',
    'Montenegro' => 'Monténégro',
];
