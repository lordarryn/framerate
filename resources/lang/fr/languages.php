<?php

return [

    'No Language' => 'pas de langage',
    'Afar' => 'afar',
    'Afrikaans' => 'afrikaans',
    'Akan' => 'akan',
    'Aragonese' => 'aragonais',
    'Assamese' => 'assamais',
    'Avaric' => 'avar',
    'Avestan' => 'avestique',
    'Aymara' => 'aymara',
    'Azerbaijani' => 'azéri',
    'Bashkir' => 'bachkir',
    'Bambara' => 'bambara',
    'Bislama' => 'bichelamar',
    'Tibetan' => 'tibétain',
    'Breton' => 'breton',
    'Catalan' => 'catalan',
    'Czech' => 'tchèque',
    'Chechen' => 'tchétchène',
    'Slavic' => 'slave',
    'Chuvash' => 'tchouvache',
    'Cornish' => 'cornique',
    'Corsican' => 'corse',
    'Cree' => 'cri',
    'Welsh' => 'gallois',
    'Danish' => 'danois',
    'German' => 'allemand',
    'Divehi' => 'maldivien',
    'Dzongkha' => 'dzongkha',
    'Esperanto' => 'esperanto',
    'Estonian' => 'estonien',
    'Basque' => 'basque',
    'Faroese' => 'féroïen',
    'Fijian' => 'fidjien',
    'Finnish' => 'finnois',
    'French' => 'français',
    'Frisian' => 'frison',
    'Fulah' => 'peul',
    'Gaelic' => 'gaélique',
    'Irish' => 'irlandais',
    'Galician' => 'galicien',
    'Manx' => 'mannois',
    'Guarani' => 'guarani',
    'Gujarati' => 'gujarati',
    'Haitian; Haitian Creole' => 'créole haitien',
    'Hausa' => 'haoussa',
    'Serbo-Croatian' => 'serbo-croate',
    'Herero' => 'héréro',
    'Hiri Motu' => 'motu',
    'Croatian' => 'croate',
    'Hungarian' => 'hongrois',
    'Igbo' => 'igbo',
    'Ido' => 'ido',
    'Yi' => 'yi',
    'Inuktitut' => 'inuktitut',
    'Interlingue' => 'interlingue',
    'Interlingua' => 'interlingua',
    'Indonesian' => 'indonésien',
    'Inupiaq' => 'inupiaq',
    'Icelandic' => 'islandais',
    'Italian' => 'italien',
    'Javanese' => 'javanais',
    'Japanese' => 'japonais',
    'Kalaallisut' => 'groenlandais',
    'Kannada' => 'kannada',
    'Kashmiri' => 'cachemiri',
    'Kanuri' => 'kanouri',
    'Kazakh' => 'kazakh',
    'Khmer' => 'khmer',
    'Kikuyu' => 'kikuyu',
    'Kinyarwanda' => 'kinyarwanda',
    'Kirghiz' => 'kirghize',
    'Komi' => 'komi',
    'Kongo' => 'kikongo',
    'Korean' => 'coréen',
    'Kuanyama' => 'kuanyama',
    'Kurdish' => 'kurde',
    'Lao' => 'laotien',
    'Latin' => 'latin',
    'Latvian' => 'letton',
    'Limburgish' => 'limbourgeois',
    'Lingala' => 'lingala',
    'Lithuanian' => 'lituanien',
    'Letzeburgesch' => 'luxembourgeois',
    'Luba-Katanga' => 'luba-katanga',
    'Ganda' => 'luganda',
    'Marshall' => 'marshallais',
    'Malayalam' => 'malayalam',
    'Marathi' => 'marathi',
    'Malagasy' => 'malgache',
    'Maltese' => 'maltais',
    'Moldavian' => 'moldavien',
    'Mongolian' => 'mongol',
    'Maori' => 'maori',
    'Malay' => 'malais',
    'Burmese' => 'birman',
    'Nauru' => 'nauru',
    'Navajo' => 'navajo',
    'Ndebele' => 'ndébélé',
    'Ndonga' => 'ndonga',
    'Nepali' => 'népalais',
    'Dutch' => 'néerlandais',
    'Norwegian Nynorsk' => 'nynorsk',
    'Norwegian Bokmål' => 'bokmål',
    'Norwegian' => 'norvégien',
    'Chichewa; Nyanja' => 'chewa',
    'Occitan' => 'occitan',
    'Ojibwa' => 'ojibwé',
    'Oriya' => 'odia',
    'Oromo' => 'oromo',
    'Ossetian; Ossetic' => 'ossète',
    'Pali' => 'pali',
    'Polish' => 'polonais',
    'Portuguese' => 'portuguais',
    'Quechua' => 'quechua',
    'Raeto-Romance' => 'rhéto-roman',
    'Romanian' => 'roumain',
    'Rundi' => 'kirundi',
    'Russian' => 'russe',
    'Sango' => 'sango',
    'Sanskrit' => 'sanskrit',
    'Sinhalese' => 'singhalais',
    'Slovak' => 'slovaque',
    'Slovenian' => 'slovénien',
    'Northern Sami' => 'same du Nord',
    'Samoan' => 'samoan',
    'Shona' => 'schona',
    'Sindhi' => 'sindhi',
    'Somali' => 'somali',
    'Sotho' => 'sotho',
    'Spanish' => 'espagnol',
    'Albanian' => 'albanais',
    'Sardinian' => 'sarde',
    'Serbian' => 'serbe',
    'Swati' => 'swati',
    'Sundanese' => 'soundanais',
    'Swahili' => 'swahili',
    'Swedish' => 'suédois',
    'Tahitian' => 'tahitien',
    'Tamil' => 'tamoul',
    'Tatar' => 'tatar',
    'Telugu' => 'télougou',
    'Tajik' => 'tadjik',
    'Tagalog' => 'tagalog',
    'Thai' => 'thailandais',
    'Tigrinya' => 'tigrigna',
    'Tonga' => 'tonga',
    'Tswana' => 'tswana',
    'Tsonga' => 'tsonga',
    'Turkmen' => 'turkmène',
    'Turkish' => 'turc',
    'Twi' => 'twi',
    'Uighur' => 'cuïghour',
    'Ukrainian' => 'ukrainien',
    'Urdu' => 'ourdou',
    'Uzbek' => 'ouzbek',
    'Venda' => 'venda',
    'Vietnamese' => 'vietnamien',
    'Volapük' => 'volapük',
    'Walloon' => 'wallon',
    'Wolof' => 'wolof',
    'Xhosa' => 'xhosa',
    'Yiddish' => 'yiddish',
    'Zhuang' => 'zhuang',
    'Zulu' => 'zoulou',
    'Abkhazian' => 'abkhaze',
    'Mandarin' => 'mandarin',
    'Pushto' => 'pachto',
    'Amharic' => 'amharique',
    'Arabic' => 'arabe',
    'Bulgarian' => 'bulgare',
    'Cantonese' => 'cantonais',
    'Macedonian' => 'macédonien',
    'Greek' => 'grec',
    'Persian' => 'persan',
    'Hebrew' => 'hébreu',
    'Hindi' => 'hindi',
    'Armenian' => 'arménien',
    'English' => 'anglais',
    'Ewe' => 'éwé',
    'Georgian' => 'géorgien',
    'Punjabi' => 'pendjabi',
    'Bengali' => 'bengalais',
    'Bosnian' => 'bosnien',
    'Chamorro' => 'chamorro',
    'Belarusian' => 'bélarus',
    'Yoruba' => 'yoruba',

];
