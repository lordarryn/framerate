<?php

return [

      'Action' => 'Action',
      'Adult' => 'Adulte',
      'Adventure' => 'Aventure',
      'Animation' => 'Animation',
      'Biography' => 'Biopic',
      'Comedy' => 'Comédie',
      'Crime' => 'Gangster',
      'Documentary' => 'Documentaire',
      'Drama' => 'Drame',
      'Family' => 'Famille',
      'Fantasy' => 'Fantastique',
      'Film Noir' => 'Film noir',
      'History' => 'Histoire',
      'Horror' => 'Épouvante-horreur',
      'Musical' => 'Comédie musicale',
      'Mystery' => 'Mystère',
      'Romance' => 'Romance',
      'Sci-Fi' => 'Science-fiction',
      'Short' => 'Court',
      'Sport' => 'Sport',
      'TV Movie' => 'Téléfilm',
      'Thriller' => 'Thriller',
      'War' => 'Guerre',
      'Western' => 'Western',

];
