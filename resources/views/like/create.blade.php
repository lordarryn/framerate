@verified
  @php($react = $model->likes->where('user_id', '=', Auth::user()->id)->first())
  <div class="btn-toolbar mb-3" role="toolbar">
    <div class="btn-group mr-3" role="group">
      <button type="button" {!! isset($react) && $react->type_id == 'like' ? 'disabled class="btn btn' : 'class="btn btn-outline' !!}-primary shadow-none like-btn" data-value="like"><i class="fas fa-thumbs-up"></i></button>
      <button type="button" {!! isset($react) && $react->type_id == 'dislike' ? 'disabled class="btn btn' : 'class="btn btn-outline' !!}-danger shadow-none like-btn" data-value="dislike"><i class="fas fa-thumbs-down"></i></button>
    </div>

    @if(isset($react))
      <div class="btn-group" role="group">
        <button class="btn btn-danger btn-sm" type="button" id="react-delete" title="Supprimer les actions"><i class="fa fa-trash"></i></button>
      </div>
    @endif
  </div>
@endverified

@php($likes = count($model->likes->where('type_id', '=', 'like')))
@php($dislikes = count($model->likes->where('type_id', '=', 'dislike')))

@if($type == 'list')
<p class="annotation-div infos-list mb-5">{{ $likes }} aime{{ $likes > 1 ? 'nt' : '' }}. {{ $dislikes }} n'aime{{ $dislikes > 1 ? 'nt' : '' }} pas.</p>
@else
<p>{{ $likes }} aime{{ $likes > 1 ? 'nt' : '' }}. {{ $dislikes }} n'aime{{ $dislikes > 1 ? 'nt' : '' }} pas.</p>
@endif

{{-- script --}}
@section('script')
<script>
  $(function() {
    $(".like-btn").click(function() {
      var type = $(this).data("value");
      var route;
      if(type == 'like') {
        route = "{{ route('like', ['model' => $type, 'id' => $model->id]) }}";
      } else if(type == 'dislike') {
        route = "{{ route('dislike', ['model' => $type, 'id' => $model->id]) }}";
      }
      $.ajax({
        url: route,
        type: 'POST',
        success: function() {
          location.reload();
        }
      });
    });

    $("#react-delete").click(function() {
      $.ajax({
        url: "{{ route('like.destroy', ['model' => $type, 'id' => $model->id]) }}",
        type: 'DELETE',
        success: function() {
          location.reload();
        }
      });
    });
  });
</script>
@append
