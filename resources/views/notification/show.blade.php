{{-- body --}}
<div class="modal-body">
    {{-- Modal subtitle --}}
    @if(class_basename($notification->type) == 'TmdbImport')
        <h3>Import film {{ $notification->data['id'] }}</h3>
    @endif

    {{-- content --}}
    @if(!empty($notification->data['errors']))
        <div class="callout callout-danger">
            <h5>Erreur</h5>
            @foreach($notification->data['errors'] as $error)
                {{ $error }}<br>
            @endforeach
        </div>
    @endif

    {{-- content if TmdbImport --}}
    @if(class_basename($notification->type) == 'TmdbImport' && isset($history))
        <div class="callout callout-info">
            @foreach($history as $model)
                @if(class_basename($model) == 'Film')
                    Entrée pour le film <a href="{{ route('films.show', $model->slug) }}">{{ $model->title }}</a> créée.<br>
                @elseif(class_basename($model) == 'Company')
                    Entrée pour la société <a href="{{ route('companies.show', $model->slug) }}">{{ $model->name }}</a> créée.<br>
                @elseif(class_basename($model) == 'Personality')
                    Entrée pour la personnalité <a href="{{ route('personalities.show', $model->slug) }}">{{ $model->name }}</a> créée.<br>
                @elseif(class_basename($model) == 'Work')
                    {{ $model->personality->name }} enregistré travaillant en tant que {{ $model->employment->name }} ({{ $model->employment->department->name }}).<br>
                @endif
            @endforeach
        </div>
    @endif
</div>
