{{-- pagination --}}
<ul id="pagination" class="pagination float-right">
    <li class="page-item{{ ($notifications->currentPage() == 1) ? ' disabled' : '' }}" data-value="1">
        <a class="page-link" href="#">«</a>
    </li>
    @php($prev = false)
    @php($next = false)
    @for($i = 1; $i <= $notifications->lastPage(); $i++)
        @if($i == $notifications->currentPage())
            <li class="page-item active" data-value="{{ $i }}">
                <a class="page-link" href="#">{{ $i }}</a>
            </li>
        @elseif($i == $notifications->currentPage()-1 || $i == $notifications->currentPage()+1)
            <li class="page-item" data-value="{{ $i }}">
                <a class="page-link" href="#">{{ $i }}</a>
            </li>
        @else
            @if($i < $notifications->currentPage() && !$prev)
                <li class="page-item disabled">
                    <a class="page-link" href="#">...</a>
                </li>
                @php($prev = true)
            @elseif($i > $notifications->currentPage() && !$next)
                <li class="page-item disabled">
                    <a class="page-link" href="#">...</a>
                </li>
                @php($next = true)
            @endif
        @endif
    @endfor
    <li class="page-item{{ ($notifications->currentPage() == $notifications->lastPage()) ? ' disabled' : '' }}"  data-value="{{ $notifications->currentPage()+1 }}">
        <a class="page-link" href="#" >»</a>
    </li>
</ul>

{{-- table --}}
<table class="table" id="table">
    <tbody>
    @foreach($notifications as $notification)
        <tr class="notification-row {{ $notification->read_at ? 'notification-read' : 'notification-unread' }}"
            data-id="{{ $notification->id }}">
            <td>
            {{-- type import imdb --}}
            @if(class_basename($notification->type) == 'TmdbImport')
                Import film {{ $notification->data['id'] }}
            @endif
            </td>
            <td class="text-right">
                le {{ date('d/m/Y à H:i', $notification->created_at->timestamp )}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
