      <div class="mv-search">
        <form role="search" method="get" action="{{ route('search') }}">
          <input type="text" name="q" placeholder="Rechercher" value="{{ isset($input) ? $input : '' }}">
          <input type="text" name="type" value="{{ isset($type) ? $type : 'tout' }}" hidden="">
          <input type="submit" alt="Search" value="Go">
        </form>
      </div>
