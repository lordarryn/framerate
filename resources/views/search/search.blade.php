@extends('layouts.app')

@section('og:title', 'Recherche' . ' - ' . config('app.name'))
@section('title', 'Recherche' . ' - ' . config('app.name'))

@section('content')
<div class="container">
    <div class="row mt-3">
        <div class="col-md-9">
            @if($results->total() == 0)
                <h5>Aucun résultat pour "{{ $input }}"</h5>
            @elseif($results->total() == 1)
                <h5>1 résultat pour "{{ $input }}"</h5>
            @elseif($results->total() <= 100)
                <h5>{{ $results->total() }} résultats pour "{{ $input }}"</h5>
            @else
                <h5>Plus de 100 résultats pour "{{ $input }}"</h5>
            @endif

            <hr>

            {{-- search results --}}
            <div class="row">
                @foreach($results as $result)
                    {{-- Film --}}
                    @if(class_basename($result) == 'Film')
                        <div class="col-12 my-2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="film-poster"
                                         title="{{ $result->title }} @if(isset($result->release_year)) ({{ $result->release_year }})@endif"
                                         style="background-image: url({{ getImageUrl($result->poster_thumbnail) }})"></div>
                                </div>
                                <div class="col-8">
                                    <p>
                                        <a class="movie-title" href="{{ route('films.show', $result->slug) }}">
                                            {{ $result->title }}
                                            @if($result->original_title != $result->title)
                                                ({{ $result->original_title }})
                                            @endif
                                        </a>
                                        <span> - {{ $result->release_year }}</span>
                                    </p>
                                    @if($result->ratings->count() > 0)
                                        <p>{{ note($result->getAverage()) }}</p>
                                    @endif
                                    <p id="directors">
                                        Réalisé par
                                        @foreach($result->directed as $director)
                                            <a clas="btn btn-link" href="{{ route('personalities.show', $director->personality->slug) }}">{{ $director->personality->name }}</a>@if(!$loop->last), @endif
                                            @if($result->directed->count() > 5 && $loop->iteration == 5)
                                                <a class="btn btn-light-pink shadow-none my-2" href="#" data-toggle="collapse" data-target="#direction-more" aria-expanded="false" aria-controls="direction-more">Voir plus <i class="fas fa-caret-down"></i></a>
                                                <div id="direction-more" class="collapse" data-parent="#directors">
                                            @endif
                                            @if($result->directed->count() > 5 && $loop->last)</div>@endif
                                        @endforeach
                                    </p>
                                </div>
                            </div>
                        </div>

                    {{-- Personality --}}
                    @elseif(class_basename($result) == 'Personality')
                        <div class="col-12 my-2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="personality-image" title="{{ $result->name }}" style="background-image: url({{ getImageUrl($result->image) }})"></div>
                                </div>
                                <div class="col-8">
                                    <p>
                                        <a clas="btn btn-link" href="{{ route('personalities.show', $result->slug) }}">{{ $result->name }}</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                    {{-- Company --}}
                    @elseif(class_basename($result) == 'Company')
                        <div class="col-12 my-2">
                            <div class="row">
                                <div class="col-4">
                                    <div class="company-logo" title="{{ $result->name }}" style="background-image: url({{ getImageUrl($result->logo) }})"></div>
                                </div>
                                <div class="col-8">
                                    <p>
                                        <a clas="btn btn-link" href="{{ route('companies.show', $result->slug) }}">{{ $result->name }}</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

            {{-- pagination --}}
            @if($results->count() != 0 && $results->lastPage() > 1)
                <hr>

                <div class="row">
                    <div class="col-md-2 py-1 text-center">
                        @if($results->currentPage() != 1)
                            <a class="btn btn-pink shadow-none" href="{{ route('search', ['q' => $input, 'type' => $type, 'page' => $results->currentPage()-1]) }}">Précédent</a>
                        @endif
                    </div>

                    <div class="col-md-8 py-1 text-center d-flex">
                        {{-- pagination main --}}
                        <ul class="pagination mx-auto">
                            {{-- always display first page --}}
                            <li class="page-item{{ ($results->currentPage() == 1) ? ' active' : '' }}">
                                <a class="page-link" href="{{ route('search', ['q' => $input, 'type' => $type]) }}">1</a>
                            </li>

                            @php($prev = false)
                            @php($next = false)
                            @for($i = 1; $i <= $results->lastPage(); $i++)
                                {{-- if iteration isn't first or last page --}}
                                @if($i != 1 && $i != $results->lastPage())
                                    {{-- if iteration is current page --}}
                                    @if($i == $results->currentPage())
                                        <li class="page-item active">
                                            <a class="page-link" href="{{ route('search', ['q' => $input, 'type' => $type, 'page' => $i]) }}">{{ $i }}</a>
                                        </li>
                                    {{-- display on each side --}}
                                    @elseif(in_array($i, [$results->currentPage()-2, $results->currentPage()-1, $results->currentPage()+1, $results->currentPage()+2]))
                                        <li class="page-item">
                                            <a class="page-link" href="{{ route('search', ['q' => $input, 'type' => $type, 'page' => $i]) }}">{{ $i }}</a>
                                        </li>
                                    {{-- or display "..." --}}
                                    @else
                                        @if($i < $results->currentPage() && !$prev)
                                            <li class="page-item disabled">
                                                <a class="page-link" href="{{ route('search', ['q' => $input, 'type' => $type, 'page' => $i]) }}">...</a>
                                            </li>
                                            @php($prev = true)
                                        @elseif($i > $results->currentPage() && !$next)
                                            <li class="page-item disabled">
                                                <a class="page-link" href="{{ route('search', ['q' => $input, 'type' => $type, 'page' => $i]) }}">...</a>
                                            </li>
                                            @php($next = true)
                                        @endif
                                    @endif
                                @endif
                            @endfor

                            {{-- always display last page --}}
                            <li class="page-item{{ ($results->currentPage() == $results->lastPage()) ? ' active' : '' }}">
                                <a class="page-link" href="{{ route('search', ['q' => $input, 'type' => $type, 'page' => $results->lastPage()]) }}">{{ $results->lastPage() }}</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-2 py-1 text-center">
                        @if($results->currentPage() != $results->lastPage())
                            <a class="btn btn-pink shadow-none" href="{{ route('search', ['q' => $input, 'type' => $type, 'page' => $results->currentPage()+1]) }}">Suivant</a>
                        @endif
                    </div>
                </div>
            @endif
        </div>

        {{-- search result type --}}
        <div class="col-md-3 order-first order-md-2 mb-3">
            <h5>Afficher les résultats pour</h5>
            <hr>
            <div class="list-group">
                <a class="list-group-item list-group-item-action {{ $type == 'tout' ? 'active' : '' }}"
                   href="{{ route('search', ['q' => $input, 'type' => 'tout']) }}">
                    Tout
                </a>
                <a class="list-group-item list-group-item-action {{ $type == 'film' ? 'active' : '' }}"
                   href="{{ route('search', ['q' => $input, 'type' => 'film']) }}">
                    Films
                </a>
                <a class="list-group-item list-group-item-action {{ $type == 'realisateur' ? 'active' : '' }}"
                   href="{{ route('search', ['q' => $input, 'type' => 'realisateur']) }}">
                    Réalisateurs
                </a>
                <a class="list-group-item list-group-item-action {{ $type == 'acteur' ? 'active' : '' }}"
                   href="{{ route('search', ['q' => $input, 'type' => 'acteur']) }}">
                    Acteurs
                </a>
                <a class="list-group-item list-group-item-action {{ $type == 'personnalite' ? 'active' : '' }}"
                   href="{{ route('search', ['q' => $input, 'type' => 'personnalite']) }}">
                    Personnalités
                </a>
                <a class="list-group-item list-group-item-action {{ $type == 'societe' ? 'active' : '' }}"
                   href="{{ route('search', ['q' => $input, 'type' => 'societe']) }}">
                    Sociétés de production
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
