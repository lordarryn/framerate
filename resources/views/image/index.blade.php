<div class="grid">
  @foreach($images as $image)
    @php($datePost = \Carbon\Carbon::parse($image->created_at->timestamp)->translatedFormat('j F Y'))
    <div class="grid-item">
      {{-- image type --}}
      @if($image->media_type == 'image')
        <a data-fancybox="galerie" data-caption-id="{{ $image->id }}" href="{{ getImageUrl($image->url) }}">
          <img class="gallery-thumbnail" src="{{ getImageUrl('thumbnail'.$image->url) }}" alt=""/>
        </a>
      {{-- video type --}}
      @elseif($image->media_type == 'video')
        <a data-fancybox="galerie" data-caption-id="{{ $image->id }}" href="{{ $image->url }}">
          <img class="gallery-thumbnail" src="{{ getVideoThumbnail($image->url) }}" alt=""/>
          <i class="fas fa-play video-i"></i>
        </a>
      @endif
      <div class="d-none" id="caption-{{ $image->id }}">
        {{-- caption when gallery from film/personality --}}
        @if($caption == 'page')
          Posté par <a href="{{ route('profile.show', $image->user->pseudo) }}">{{ $image->user->pseudo }}</a> le {{ $datePost }}
        {{-- caption when gallery from /galerie --}}
        @elseif($caption == 'index')
          @if($image->imageable_type == "App\Models\Film")
            <a href="{{ route('films.show', $image->imageable->slug) }}">{{ $image->imageable->title }}</a>, posté par <a href="{{ route('profile.show', $image->user->pseudo) }}">{{ $image->user->pseudo }}</a> le {{ $datePost }}
          @elseif($image->imageable_type == "App\Models\Personality")
            <a href="{{ route('personalities.show', $image->imageable->slug) }}">{{ $image->imageable->name }}</a>, posté par <a href="{{ route('profile.show', $image->user->pseudo) }}">{{ $image->user->pseudo }}</a> le {{ $datePost }}
          @endif
        {{-- caption when gallery from profile page --}}
        @elseif($caption == 'profile')
          @if($image->imageable_type == "App\Models\Film")
            <a href="{{ route('films.show', $image->imageable->slug) }}">{{ $image->imageable->title }}</a>, posté le {{ $datePost }}
          @elseif($image->imageable_type == "App\Models\Personality")
            <a href="{{ route('personalities.show', $image->imageable->slug) }}">{{ $image->imageable->name }}</a>, posté le {{ $datePost }}
          @endif
        @endif
        @verified
          @php($isLiked = $image->likes->where('user_id', '=', Auth::user()->id)->first())
          <span class="image-like {{ $isLiked ? 'active' : '' }}" data-id="{{ $image->id }}" title="{{ $isLiked ? 'Je n\'aime plus' : 'J\'aime' }}"><i class="fas fa-thumbs-up"></i></span>
          <span class="span-like ml-2" data-id="{{ $image->id }}" title="Mentions j'aime">{{ $image->likes->count() > 0 ? $image->likes->count() : '' }}</span>
        @else
          <span class="span-like ml-2" data-id="{{ $image->id }}" title="Mentions j'aime">{!! $image->likes->count() > 0 ? $image->likes->count() . ' <i class="fas fa-thumbs-up"></i>' : '' !!}</span>
        @endverified
      </div>
    </div>
  @endforeach
</div>
