<div class="modal fade" id="gallery-modal" tabindex="-1" role="dialog" aria-labelledby="gallery-modal-label" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Poster une image ou une vidéo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- image --}}
        <form class="upload" method="POST" action="{{ route('images.store', ['model' => $model, 'slug' => $slug, 'media' => 'image']) }}" enctype="multipart/form-data">
          @csrf
          <div class="upload-trigger">
            <i class="fas fa-upload upload-icon"></i>
            <input type="file" name="image" id="image" class="input-image">
            <div class="row justify-content-center">
              <div class="col-10 text-center">
                <label for="image" class="input-label">
                  <strong class="btn btn-link shadow-none upload-browse">Choisir une image</strong>
                  <span class="d-inline">ou glisser-déposer</span>
                </label>
              </div>
            </div>
            <button type="submit" class="btn btn-pink shadow-none upload-submit">Envoyer</button>
          </div>
          <div class="row justify-content-center">
            <div class="col-10 text-center">
              <div class="trigger-uploading text-center alert alert-info" role="alert">Upload en cours.</div>
              <div class="trigger-success text-center alert alert-success" role="alert">L'image a été uploadée.</div>
              <div class="trigger-error text-center alert alert-danger" role="alert">Erreur.</div>
            </div>
          </div>
        </form>

        {{-- video --}}
        <form id="video-submit" method="POST" action="{{ route('images.store', ['model' => $model, 'slug' => $slug, 'media' => 'video']) }}">
          @csrf
          <div class="form-group">
              <div class="alert alert-info mt-3" role="alert">
                Ou bien poster une vidéo
              </div>

              <input type="url" id="video" class="form-control" name="video" placeholder="Adresse de la vidéo">

              @error('video')
                <div class="alert alert-danger m-1" role="alert">
                  {{ $message }}
                </div>
              @enderror

              <div class="form-group mt-2">
                <button type="submit" class="btn btn-pink shadow-none">Envoyer</button>
              </div>

              <div id="video-uploaded" class="alert d-none m-1" role="alert">
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<script>
  $(function() {

    var form = $(".upload");
    var droppedFiles = false;
    var uploaded = false;

    // events
    ['drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop'].forEach(function(event) {
      form.on(event, function(e) {
        // preventing the unwanted behaviours
        e.preventDefault();
        e.stopPropagation();
      });
    });

    ['dragover', 'dragenter'].forEach(function(event) {
      form.on( event, function() {
        form.addClass('is-dragover');
      });
    });

    ['dragleave', 'dragend', 'drop'].forEach(function(event) {
      form.on( event, function() {
        form.removeClass('is-dragover');
      });
    });

    form.on('drop', function(e) {
      if(e.originalEvent.dataTransfer) {
        if(e.originalEvent.dataTransfer.files.length) {
          e.preventDefault();
          droppedFiles = e.originalEvent.dataTransfer.files;
          $(".input-label").text(droppedFiles[0].name);
        }
      }
      return false;
    });

    $(".input-image").on('change', function(e) {
      e.preventDefault();
      droppedFiles = e.target.files;
      $(".input-label").text(droppedFiles[0].name);
    });

    // submit
    form.on('submit', function(e) {
      e.preventDefault();
      var formData = new FormData();

      $(".upload-submit").addClass('d-none');
      form.addClass('is-uploading');
      form.removeClass('is-error');

      if(droppedFiles) {
        formData.append('image', droppedFiles[0]);
      }

      $.ajax({
        url: '{{ route('images.store', ['model' => $model, 'slug' => $slug, 'media' => 'image']) }}',
        type: 'POST',
        enctype: 'multipart/form',
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        success: function(data) {
          form.addClass('is-success');
          uploaded = true;
        },
        error: function(data) {
          form.addClass('is-error');

          if(typeof data.responseJSON.errors !== 'undefined') {
            for(errorType in data.responseJSON.errors) {
              data.responseJSON.errors[errorType].forEach(elem => $('.trigger-error').append('<p>'+elem+'</p>'));
            }
          }
         }
      });
    });

    // video
    $("#video-submit").on('submit', function(e) {
      e.preventDefault();

      $.ajax({
        url: '{{ route('images.store', ['model' => $model, 'slug' => $slug, 'media' => 'video']) }}',
        type: 'POST',
        data: {video: $("#video").val()},
        success: function(data) {
          $("#video-uploaded").removeClass("d-none").addClass("alert-success").text("La vidéo a été uploadée.");
          $("#video").val('')
          uploaded = true;;
        },
        error: function(data) {
          $("#video-uploaded").removeClass("d-none").addClass("alert-danger");

          if(typeof data.responseJSON.errors !== 'undefined') {
            for(errorType in data.responseJSON.errors) {
              data.responseJSON.errors[errorType].forEach(elem => $("#video-uploaded").append('<p>'+elem+'</p>'));
            }
          }
        }
      });
    });

    $('#gallery-modal').on('hidden.bs.modal', function() {
      form.removeClass('is-uploading');
      form.removeClass('is-success');
      form.removeClass('is-error');
      $('.trigger-error').empty();

      $("#video-uploaded").removeClass("alert-danger")
                          .removeClass("alert-success")
                          .addClass("d-none")
                          .empty();
      if(uploaded) {
        location.reload();
      } else {
        $(".upload-submit").removeClass('d-none');
      }
    })

  });
</script>
@include('image.js')
