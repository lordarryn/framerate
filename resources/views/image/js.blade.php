<script>
$(function() {
  /**
   * gallery
   */
  $().fancybox({
    selector: '[data-fancybox="galerie"]',
    padding: 0,
    transitionIn: 'none',
    transitionOut: 'none',
    //type: 'media',
    changeFade: 0,
    caption: function(instance) {
      var id = $(this).data('caption-id');
      return $("#caption-"+id).html();
    }
  });

  $(".grid").masonry({
    gutter: 10,
    fitWidth: true
  });

  // like
  $(document).on("click", ".image-like:not(.active)", function() {
    var id = $(this).data("id");
    $.ajax({
        url: '{{ route('like', ['model' => 'image', 'id' => 1]) }}'.slice(0, -1) + id,
        type: 'POST',
        success: function() {
          $('.image-like[data-id="'+id+'"]').toggleClass('active');
          var total = parseInt($('.span-like[data-id="'+id+'"]').first().text());
          $('.span-like[data-id="'+id+'"]').html(total+1);
        }
    });
  });

  // destroy like
  $(document).on("click", ".image-like.active", function() {
    var id = $(this).data("id");
    $.ajax({
        url: '{{ route('like.destroy', ['model' => 'image', 'id' => 1]) }}'.slice(0, -1) + id,
        type: 'DELETE',
        success: function() {
          $('.image-like[data-id="'+id+'"]').toggleClass('active');
          var total = parseInt($('.span-like[data-id="'+id+'"]').first().text());
          $('.span-like[data-id="'+id+'"]').html(total-1);
        }
    });
  });
});
</script>
