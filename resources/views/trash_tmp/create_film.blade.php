@extends('layouts.app')

@section('title', 'rajouter film')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-5">
                <div class="card-header">Ajout film</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('films.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="imdb_id" class="col-md-4 col-form-label text-md-right">ID IMDb</label>

                            <div class="col-md-6">
                                <input id="imdb_id" type="text" class="form-control @error('imdb_id') is-invalid @enderror" name="imdb_id" value="{{ old('imdb_id') }}" required>

                                @error('imdb_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div id="names">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Nom</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-2">
                                    <button type="button" id="add-akas" class="btn btn-success"><i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bio" class="col-md-4 col-form-label text-md-right">Biographie</label>

                            <div class="col-md-6">
                                <textarea id="bio" class="form-control @error('bio') is-invalid @enderror" name="bio"></textarea>

                                @error('bio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birthday" class="col-md-4 col-form-label text-md-right">Naissance</label>

                            <div class="col-md-6">
                                <input id="birthday" type="date" class="form-control @error('birthday') is-invalid @enderror" name="birthday" required>

                                @error('birthday')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="deathday" class="col-md-4 col-form-label text-md-right">Décès</label>

                            <div class="col-md-6">
                                <input id="deathday" type="date" class="form-control @error('deathday') is-invalid @enderror" name="deathday">

                                @error('deathday')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label text-md-right">Photo</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image">

                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="countries" class="col-md-4 col-form-label text-md-right">Pays</label>

                            <div class="col-md-6">
                                <select name="countries[]" id="countries" class="form-control @error('country') is-invalid @enderror" multiple="multiple">
                                    @foreach($countries as $country)
                                      <option value="{{ $country->id }}" >{{ $country->name }}</option>
                                    @endforeach
                                </select>

                                @error('countries')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mt-1">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-pink shadow-none">Ajouter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
