@extends('layouts.app')

@section('og:title')Galerie - {{ config('app.name') }}@endsection
@section('title')Galerie - {{ config('app.name') }}@endsection

@section('og:description')Explorez la galerie - {{ config('app.name') }}@endsection
@section('description')Explorez la galerie - {{ config('app.name') }}@endsection

@section('header')
<style>
  .slick-prev:before {
    color: #fd0e3e !important;
    background-color: #eee;
  }
  .slick-next:before {
    color: #fd0e3e !important;
    background-color: #eee;
  }
</style>
@endsection

@section('content')
{{-- Carousel --}}
@if(!empty($carousel))
  <div id="carousel" class="row m-1">
    @foreach($carousel as $film)
    <div style="background-image: url({{ getImageUrl($film->backdrop) }})" class="carousel-background">
      <div class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-6 col-sm-7 col-8 carousel-content text-center">
          <h1 class="movie-title">{{ $film->title }}</h1>
          <h3>{{ $film->ratings->count() == 0 ? 'Ø' : note($film->getAverage()) }}</h3>
          <a class="btn btn-link shadow-none" href="{{ route('films.show', $film->slug) }}">Voir le film</a>
        </div>
      </div>
    </div>
    @endforeach
  </div>
@endif
{{-- </Carousel --}}

<div class="container">
  <h1>Explorez la galerie</h1>
  <hr>

  @include('image.index', ['images' => $images, 'caption' => 'index'])

  <div class="page-load-status">
    <div class="loader-ellips infinite-scroll-request">
      <span class="loader-ellips__dot"></span>
      <span class="loader-ellips__dot"></span>
      <span class="loader-ellips__dot"></span>
      <span class="loader-ellips__dot"></span>
    </div>
    <p class="infinite-scroll-last">Fin de la page</p>
    <p class="infinite-scroll-error">Fin de la page</p>
  </div>
</div>
@endsection

@section('script')
@include('image.js')
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<script>
  $(function() {
    $("#carousel").slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false,
      dots: true,
      autoplaySpeed: 10000,
    });

    $('.grid').infiniteScroll({
      path: "?page=@{{#}}",
      append: ".grid-item",
      outlayer: $(".grid").data("masonry"),
      history: false,
      status: ".page-load-status"
    });
  });
</script>
@endsection
