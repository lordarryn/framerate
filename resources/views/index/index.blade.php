@extends('layouts.app')

@section('og:title'){{ config('app.name') }}@endsection
@section('title'){{ config('app.name') }}@endsection

@section('og:description')FrameRate, site communautaire sur le cinéma, faites de belles découvertes - {{ config('app.name') }}@endsection
@section('description')FrameRate, site communautaire sur le cinéma, faites de belles découvertes - {{ config('app.name') }}@endsection

@section('content')
{{-- Carousel --}}
@if(!empty($carousel))
  <div id="carousel" class="row m-1">
    <div style="background-image: url({{ asset('assets/footer-bg.png') }})" class="carousel-background">
      <div class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-6 col-sm-7 col-8 carousel-content text-center">
          <h1 class="movie-title">Bienvenue sur FrameRate</h1>
          <p><a class="btn btn-link shadow-none" href="{{ route('films.index') }}">Accéder au top des films de tous les temps</a></p>
          <p>En construction</p>
        </div>
      </div>
    </div>
    @foreach($carousel as $film)
    <div style="background-image: url({{ getImageUrl($film->backdrop) }})" class="carousel-background">
      <div class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-6 col-sm-7 col-8 carousel-content text-center">
          <h1 class="movie-title">{{ $film->title }}</h1>
          <h3>{{ $film->ratings->count() == 0 ? 'Ø' : note($film->getAverage()) }}</h3>
          <a class="btn btn-link shadow-none" href="{{ route('films.show', $film->slug) }}">Voir le film</a>
        </div>
      </div>
    </div>
    @endforeach
  </div>
@endif
{{-- </Carousel --}}

@verified
{{-- feed --}}
<div class="container">
  <hr>
  <div class="row">
    {{-- following col--}}
    <div class="col-md-4 align-self-start">
      <h3>Eclaireurs</h3>
      @php($following = Auth::user()->following)
      @if($following->count() == 0)
        <p>Vous n'avez aucun éclaireur.</p>
      @else
        <div id="following-list" class="list-group">
          <a href="#" id="all-follows" data-id="0" class="user-feed disabled list-group-item list-group-item-action active">Tous</a>
          @foreach($following as $user)
            <a href="#" data-id="{{ $user->id }}" class="user-feed list-group-item list-group-item-action">
              <div style="background-image: url({{ getImageUrl($user->profile_image, 'avatar.jpg') }})" class="rounded-circle profile-avatar"></div>
              <span>{{ $user->pseudo }}</span>
            </a>
          @endforeach
        </div>
      @endif
    </div>
    {{-- feed --}}
    <div class="col-md-8">
      <div class="row justify-content-end">
        <header id="feed-header" class="col text-right">
          <a href="#" id="all-feed" class="btn btn-link active shadow-none">Toutes les activités</a href="#">
          <a href="#" id="list-feed" class="btn btn-link shadow-none">Listes</a href="#">
        </header>
      </div>
      <div id="feed">
        @include('activity.feed', ['activities' => $feed])
      </div>
      <div id="feed-loader" class="page-load-status">
        <div class="loader-ellips infinite-scroll-request">
          <span class="loader-ellips__dot"></span>
          <span class="loader-ellips__dot"></span>
          <span class="loader-ellips__dot"></span>
          <span class="loader-ellips__dot"></span>
        </div>
        <p class="infinite-scroll-last">Fin de la page</p>
        <p class="infinite-scroll-error">Fin de la page</p>
      </div>
    </div>
  </div>
</div>
{{-- </feed> --}}
@else
{{-- Top personalities --}}
<div class="bg-light text-dark">
  <div class="container">
    <header class="text-center mt-2">
      <h2>Découvertes</h2>
      <hr>
    </header>
    @foreach($personalities as $personality)
      <div class="row">
        <div class="col-4">
          <div class="personality-image" title="{{ $personality->name }}" style="background-image: url({{ getImageUrl($personality->image) }})"></div>
          <p class="text-center"><a clas="btn btn-link" href="{{ route('personalities.show', $personality->slug) }}">{{ $personality->name }}</a></p>
        </div>
        <div class="col-8">
          @php($films = $personality->topRatedFilms())
          @if($films->count())
            <div class="personality-carousel">
              @foreach($films as $film)
                <div>
                  <div class="film-poster"
                       title="{{ $film->title }} @if(isset($film->release_year)) ({{ $film->release_year }})@endif"
                       style="background-image: url({{ getImageUrl($film->poster_thumbnail) }})"></div>
                  <p class="text-center">
                    <a clas="btn btn-link" href="{{ route('films.show', $film->slug) }}">{{ $film->title }}</a>
                    - {{ note($film->average) }}
                  </p>
                </div>
              @endforeach
            </div>
          @endif
        </div>
      </div>
    @endforeach
  </div>
</div>
{{-- </Top personalities> --}}
@endverified
@endsection

@section('script')
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<script>
  $(function() {
    $("#carousel").slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      arrows: false,
      dots: true,
      autoplaySpeed: 10000,
    });

    $(".personality-carousel").slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 10000,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });

    @verified
    /**
     * Feed
     */
    var loader =
    '<div class="text-center">'+
      '<div class="spinner-border text-center m-5" role="status">'+
        '<span class="sr-only">Loading...</span>'+
      '</div>'+
    '</div>';
    var follow = 0;
    var type = "all";
    var $feed = $("#feed");

    // reset infinite scroll
    function setScroll() {
      $feed.infiniteScroll({
        path: "{{ route('activities.feed') }}?page=@{{#}}&follow="+follow+"&type="+type,
        append: ".activity-row",
        history: false,
        status: "#feed-loader"
      });
    }

    setScroll();

    // reset feed div
    function resetFeed() {
      $feed.infiniteScroll('destroy');
      $feed.html(loader);

      $.ajax({
        type: 'GET',
        url: "{{ route('activities.feed') }}?follow="+follow+"&type="+type,
        success: function(data) {
          if(data === undefined) {
            $feed.html("");
          } else {
            $feed.html(data);
          }
          setScroll();
        },
        error: function(request) {
          $feed.html('<p class="text-center text-muted col-12">Erreur</p>');
        }
      });
    }

    // by following
    $(".user-feed").click(function(e) {
      e.preventDefault();

      $(".user-feed").each(function() {
        $(this).removeClass("active").removeClass("disabled");
      });
      $(this).addClass("disabled").addClass("active");

      follow = $(this).data("id");
      resetFeed();
    });

    // filters
    $("#all-feed").click(function(e) {
      e.preventDefault();
      if(type == "all") return;
      $("#all-feed").addClass("active");
      $("#list-feed").removeClass("active");
      type = "all";
      resetFeed();
    });

    $("#list-feed").click(function(e) {
      e.preventDefault();
      if(type == "list") return;
      $("#list-feed").addClass("active");
      $("#all-feed").removeClass("active");
      type = "list";
      resetFeed();
    });
    @endverified
  });
</script>
@append
