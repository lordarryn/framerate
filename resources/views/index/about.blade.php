@extends('layouts.app')

@section('og:title')A propos - {{ config('app.name') }}@endsection
@section('title')A propos - {{ config('app.name') }}@endsection

@section('og:description')A propos du site - {{ config('app.name') }}@endsection
@section('description')A propos du site - {{ config('app.name') }}@endsection


@section('content')
<div class="container">
  <h1>A propos</h1>

  <h3 id="cookie-policy">Cookies</h3>

  <h3>Hébergement du site</h3>

  <p>
  OVH SAS<br>
  2 rue Kellerman<br>
  59100 Rouba<br>
  1007
  </p>

  <div class="text-center">
    <img src="{{ asset('assets/logo-900px.png') }}" class="img-fluid" alt="Logo Framerate">
  </div>
</div>
@endsection
