@extends('adminlte::page')

@section('title')Ajouter un film - Administration - {{ config('app.name') }}@endsection

@section('content_header')
    <h1>Ajouter un film</h1>
@stop

@section('content')
    <div class="card card-secondary">

        <div class="alert alert-danger alert-dismissible my-2">
            Temporairement désactivé
        </div>

        <div class="card-header">
            <h3 class="card-title">Ajouter des films via l'ID IMDb</h3>
        </div>
        <div class="card-body">
            @error('ids')
                <div class="alert alert-danger alert-dismissible my-2">
                    <h5><i class="icon fas fa-ban"></i> Erreur</h5>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ $message }}
                </div>
            @enderror

            @if(Session::has('alert-error'))
                  <div class="alert alert-danger alert-dismissible my-2">
                    <h5><i class="icon fas fa-ban"></i> Erreur</h5>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('alert-error') }}
                </div>
            @endif

            @if(Session::has('alert-info'))
                <div class="alert alert-success alert-dismissible my-2">
                    <h5><i class="icon fas fa-check"></i> Ajout de film</h5>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ Session::get('alert-info') }}
                </div>
            @endif
@if(in_array(Auth::user()->pseudo, ['Aerosse', 'LordArryn', 'Vilain', 'Siegut', 'Lornithorynque', 'GoyEternel']))
            <blockquote class="quote-info m-0">
                Vous pouvez ajouter plusieurs ID à la suite. Il faut entrer un ID puis valider en appuyant sur entrée. Vous retrouvez l'ID sur un lien de cette manière : <em>https://www.imdb.com/title/<b class="text-lg">tt0056327</b>/</em>
            </blockquote>

            <form method="POST" action="{{ route('admin.films.store') }}">
                @csrf

                <input type="hidden" name="_type" value="films">
                <input id="tags-film" class="my-2" name="ids" placeholder="Ajouter un ID IMDb">
                <button id="tags-film-delete" class="btn btn-danger" type="button">Supprimer</button>
                <input class="btn btn-info" type="submit" value="Envoyer">
            </form>

            <blockquote class="quote-info m-0 mt-2">
                Importer tous les films liés à une personnalité : il suffit de rentrer l'ID d'une personne, exemple : <em>https://www.imdb.com/name/<b class="text-lg">nm0793983</b>/</em>
            </blockquote>

            <form method="POST" action="{{ route('admin.films.store') }}">
                @csrf

                <input type="hidden" name="_type" value="personalities">
                <input id="tags-p" class=" my-2" name="ids" placeholder="Ajouter un ID IMDb">
                <button id="tags-p-delete" class="btn btn-danger" type="button">Supprimer</button>
                <input class="btn btn-info" type="submit" value="Envoyer">
            </form>
@endif
        </div>
    </div>
    <!-- end api bloc -->

    <!-- form movie creation -->
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Ajouter un film manuellement</h3>
        </div>
        <div class="card-body">
            <blockquote class="quote-info m-0">
                Avant d'ajouter un film, penser à créer en premier lieu les fiches de personnalités
            </blockquote>
        </div>
    </div>
@stop

@section('js')
@include('js.app')
<script>
    $(function() {
        var $input = $("#tags-film").tagify()
        var jqTagify = $input.data('tagify');
        $("#tags-film-delete").on('click', jqTagify.removeAllTags.bind(jqTagify))

        var $input = $("#tags-p").tagify()
        var jqTagify = $input.data('tagify');
        $("#tags-p-delete").on('click', jqTagify.removeAllTags.bind(jqTagify))
    })
</script>
@stop
