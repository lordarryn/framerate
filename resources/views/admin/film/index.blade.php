@extends('adminlte::page')

@section('title')Gestion des films - Administration - {{ config('app.name') }}@endsection

@section('content_header')
    <h1>Gérer les films</h1>
@stop
@if(Auth::user()->pseudo == 'LordArryn')
@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Duplications de films</h3>
        </div>
        <div class="card-body">
            <table id="duplicated">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>ID IMDb</th>
                        <th>Date création</th>
                        <th>Titre</th>
                        <th>URL</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Films sans réalisateur</h3>
        </div>
        <div class="card-body">
            <table id="without-director">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>ID IMDb</th>
                        <th>Date création</th>
                        <th>Titre</th>
                        <th>URL</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@stop
@endif
@section('js')
@include('js.app')
<script>
$(function() {
    $("#duplicated").DataTable({
        data: [
        @foreach($duplicated as $film)
            [
                {{ $film->id }},
                "{{ $film->imdb_id }}",
                "{{ date('d/m H:i:s', $film->created_at->timestamp) }}",
                "{{ $film->title }}",
                "<a href=\"{{ route('films.show', $film->slug) }}\">{{ $film->slug }}</a>"
                + "<a class=\"float-right\" id=\"delete-film\" data-id=\"{{ $film->id }}\" href=\"#\"><span class=\"badge badge-danger\"><i class=\"fas fa-trash-alt\"></i></span></a>",
            ],
        @endforeach
        ],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json"
        }
    });

    $("#without-director").DataTable({
        data: [
        @foreach($withoutDirectors as $film)
            [
                {{ $film->id }},
                "{{ $film->imdb_id }}",
                "{{ date('d/m H:i:s', $film->created_at->timestamp) }}",
                "{{ $film->title }}",
                "<a href=\"{{ route('films.show', $film->slug) }}\">{{ $film->slug }}</a>"
                + "<a class=\"float-right pl-1\" id=\"update-film\" data-id=\"{{ $film->id }}\" href=\"#\"><span class=\"badge badge-info\"><i class=\"fas fa-edit\"></i></span></a>"
                + "<a class=\"float-right pl-1\" id=\"delete-film\" data-id=\"{{ $film->id }}\" href=\"#\"><span class=\"badge badge-danger\"><i class=\"fas fa-trash-alt\"></i></span></a>",
            ],
        @endforeach
        ],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json"
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // delete film
    $(document).on("click", "#delete-film", function(e) {
        e.preventDefault();
        id = $(this).data("id");

        $.ajax({
            url: "{{ route('admin.films.destroy', '') }}/" + id,
            type: 'DELETE',
            success: function(data) {
                var row = $('a[data-id="'+id+'"]').parents('tr');
                var table = $(row).parents('table').DataTable();
                table.row(row).remove().draw();
            }
        });
    });

    // update import film
    $(document).on("click", "#update-film", function(e) {
        e.preventDefault();
        id = $(this).data("id");

        $.ajax({
            url: "{{ route('admin.films.update', '') }}/" + id,
            type: 'PUT',
            success: function(data) {
                var row = $('a[data-id="'+id+'"]').parents('tr');
                var table = $(row).parents('table').DataTable();
                table.row(row).remove().draw();
            }
        });
    });
});
</script>
@stop
