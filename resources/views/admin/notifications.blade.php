@extends('adminlte::page')

@section('title')Notifications - Administration - {{ config('app.name') }}@endsection

@section('content_header')
    <h1>Notifications</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <button id="notifications-read-all" class="btn btn-info" type="button">Tout marquer comme lu</button>
            <div id="notifications-pagination" class="card-tools"></div>
        </div>
        <div class="card-body p-0" id="notifications-table"></div>
    </div>
@stop

@section('js')
@include('js.app')
@include('js.notifications')
@stop
