@extends('adminlte::page')

@section('title')Administration - {{ config('app.name') }}@endsection

@section('content_header')
  <h1>Gestion des galeries</h1>
@stop
@if(Auth::user()->pseudo == 'Kafka')
@section('content')
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Liste des images</h3>
    </div>
    <div class="card-body">
      <table id="gallery">
        <thead>
          <tr>
            <th>ID</th>
            <th>Date création</th>
            <th>Posté par</th>
            <th>Aperçu</th>
            <th>Page</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
@stop
<img src="" alt="">
@section('js')
@include('js.app')
<script>
$(function() {
  $("#gallery").DataTable({
    data: [
    @foreach($images as $image)
      [
        {{ $image->id }},
        "{{ date('d/m H:i:s', $image->created_at->timestamp) }}",
        '<a href="{{ route('profile.show', $image->user->pseudo) }}">{{ $image->user->pseudo }}</a>',
        '<a href="{{ getImageUrl($image->url) }}"><img src="{{ $image->media_type == 'image' ? getImageUrl('thumbnail'.$image->url) : getVideoThumbnail($image->url) }}" alt="Aperçu" width="150"></a>',
        '<a href="{{ route('films.show', $image->imageable->slug) }}">{{ route('films.show', $image->imageable->slug) }}</a>'
        + "<a class=\"float-right pl-1\" id=\"delete-image\" data-id=\"{{ $image->id }}\" href=\"#\"><span class=\"badge badge-danger\"><i class=\"fas fa-trash-alt\"></i></span></a>",
      ],
    @endforeach
    ],
    "language": {
      "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/French.json"
    }
  });

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  // delete film
  $(document).on("click", "#delete-image", function(e) {
    e.preventDefault();
    id = $(this).data("id");

    $.ajax({
      url: "{{ route('images.destroy', '') }}/" + id,
      type: 'DELETE',
      success: function(data) {
        var row = $('a[data-id="'+id+'"]').parents('tr');
        var table = $(row).parents('table').DataTable();
        table.row(row).remove().draw();
      }
    });
  });
});
</script>
@stop
@endif
