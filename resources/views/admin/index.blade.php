@extends('adminlte::page')

@section('title')Administration - {{ config('app.name') }}@endsection

@section('content_header')
    <h1>Bienvenue</h1>
@stop

@section('content')
    <p>Vous êtes sur la page d'administration de la base de données de FrameRate.</p>

    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Base de données</h3>
        </div>
        <div class="card-body">
            <canvas id="sizeChart" style="min-height: 300px; height: 300px; max-height: 300px; max-width: 100%; display: block;" height="300" class="chartjs-render-monitor"></canvas>
        </div>
    </div>

    <p>{{ $count['users'] }} utilisateurs inscrits.</p>

    <p>Dernière activité (10 min) : {{ $count['activeUsers'] }} utilisateur(s) actif(s), {{ $count['activeGuests'] }} invité(s) actif(s).</p>

    <p>Il y a actuellement {{ $count['films'] }} films et {{ $count['personnalities'] }} personnalités dans la base de données.</p>
    <p>Il y a actuellement {{ $count['works'] }} enregistrements de personnes sur des films, soit une moyenne de {{ round($count['works']/$count['films'], 2) }} personnes par film.</p>
@stop

@section('js')
@include('js.app')
<script>
    var sizeData = {
        labels: [
            @foreach($sizes as $size) '{{ $size->table }}', @endforeach
        ],
        datasets: [{
            data: [ @foreach($sizes as $size) '{{ $size->size }}', @endforeach ],
            backgroundColor: [ @foreach($sizes as $size) '#{{ randomColor() }}', @endforeach ]
        }]
    };

    var sizeChart = new Chart(
        $('#sizeChart').get(0).getContext('2d'),
        {
            type: 'pie',
            data: sizeData,
            options: {
                maintainAspectRatio : false,
                responsive : true,
                title: {
                    display: true,
                    text: 'Taille totale de la BDD : {{ $sizes->sum('size') }} Mo',
                    fontSize: '16'
                },
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                        title: function(tooltipItem, data) {
                            return 'Taille de la table ' + data['labels'][tooltipItem[0]['index']];
                        },
                        label: function(tooltipItem, data) {
                            return data['datasets'][0]['data'][tooltipItem['index']] + ' Mo';
                        },
                        afterLabel: function(tooltipItem, data) {
                            var dataset = data['datasets'][0];
                            var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
                            return '(' + percent + '%)';
                        }
                    }
                }
            }
    });
</script>
@stop
