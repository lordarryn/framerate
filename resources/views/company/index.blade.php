@extends('layouts.app')

@section('og:title', 'Top sociétés de production - ' . config('app.name'))

@section('title', 'Top sociétés de production- ' . config('app.name'))

@section('content')
<div class="container">
  <h1>Top sociétés de production</h1>

  <ul class="list-group list-group-flush">
    @foreach($companies as $company)
      <li class="list-group-item text-center">
        <div class="company-logo" title="{{ $company->name }}" style="background-image: url({{ getImageUrl($company->logo) }})"></div>
        <a href="{{ route('companies.show', $company->slug) }}">{{ $company->name }}</a> : {{ $company->likes }} fan{{ $company->likes > 1 ? 's' : '' }}.
      </li>
    @endforeach
  </ul>
</div>
@endsection
