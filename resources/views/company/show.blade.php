@extends('layouts.app')

@section('description', 'Société de production ' . $company->name)
@section('og:title', $company->name . ' - ' . config('app.name'))
@section('og:description', 'Société de production ' . $company->name)
@if($company->image)
  @section('og:image', getImageUrl($company->image))
@endif
@section('og:url', route('companies.show', $company->slug))

@section('title', $company->name . ' - ' . config('app.name'))

@section('content')
<div class="container">
  <div class="row mt-5">
    <div class="col-lg-3">
      <div class="company-logo" title="{{ $company->name }}" style="background-image: url({{ getImageUrl($company->logo) }})"></div>
    </div>
    <div class="col-lg-9">
      <div class="headline">
        <h1>{{ $company->name }}</h1>
        @if(isset($company->country))
          <a href="{{ route('countries.show', ['id' => $company->country->id, 'name' => __('countries.'.$company->country->name)]) }}">
            <img class="align-bottom country-flag" src="{{ asset('assets/countries') . '/' . strtolower($company->country->code) . '.svg' }}" alt="{{ $company->country->name }}" title="{{ $company->country->name }}">
          </a>
        @endif
      </div>

      {{-- like/dislike --}}
      @include('like.create', ['model' => $company, 'type' => 'company'])
    </div>
  </div>

  <div class="row mt-1">
    <div class="col-12">
      <nav>
        <div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
          <a class="nav-item nav-link active" id="nav-films-tab" data-toggle="tab" href="#" data-target="#nav-films" role="tab" aria-controls="nav-films" aria-selected="true">
            Films produits <span>({{ $company->films->count() }})</span>
          </a>
        </div>
      </nav>
      <div class="tab-content" id="nav-tab-content">
        <div class="tab-pane fade show active" id="nav-films" role="tabpanel" aria-labelledby="nav-films-tab">
          <div class="row justify-content-center mt-1">
            @foreach($company->films as $film)
              <div class="card d-flex align-items-center border-0 col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 my-1 text-center">
                <div class="row">
                  <div class="col-12">
                    <div id="{{ $film->slug }}" class="film-poster"
                         title="{{ $film->title }} @if(isset($film->release_year)) ({{ $film->release_year }})@endif"
                         style="background-image: url({{ getImageUrl($film->poster_thumbnail) }})"></div>
                  </div>
                  <div class="col-12">
                    <a class="movie-title" href="{{ route('films.show', $film->slug) }}">{{ $film->title }}</a>
                    <span> - {{ $film->release_year }}</span>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
