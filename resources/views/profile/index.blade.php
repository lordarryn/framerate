@extends('layouts.app')

@section('title')Profil - {{ config('app.name') }}@endsection

@section('header')
  <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection

@section('content')
<div class="container">
  <div class="row mt-3">
    <div class="col-md-9">
      <div class="card">
        <div class="card-body">
          <div class="tab-content">
            {{-- profile tab --}}
            <div class="tab-pane fade show {{Session::has('alert-notifs') || Session::has('alert-account') ? '' : 'active'}}" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              <h3>Modifier le profil</h3>

              @if(Session::has('alert-info'))
                <div class="alert alert-info m-1" role="alert">
                  {{ Session::get('alert-info') }}
                </div>
              @endif

              <form method="POST" action="{{ route('profile.update') }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="profile_image" name="profile_image">
                    <label class="custom-file-label" for="profile_image">Avatar</label>
                  </div>

                  <div id="preview" class="avatar"></div>

                  @error('profile_image')
                    <div class="alert alert-danger m-1" role="alert">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="form-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="background" name="background">
                    <label class="custom-file-label" for="background">Image de fond</label>
                  </div>

                  @error('background')
                    <div class="alert alert-danger m-1" role="alert">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="form-group">
                  <label for="birthday">Date de naissance</label>
                  <input type="date" class="form-control" id="birthday" name="birthday" value="{{ Auth::user()->birthday }}">

                  @error('birthday')
                    <div class="alert alert-danger mt-1" role="alert">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="show_birthday" name="show_birthday" {{ Auth::user()->show_birthday ? 'checked' : '' }}>
                  <label class="form-check-label" for="show_birthday">Afficher la date de naissance sur le profil</label>

                  @error('show_birthday')
                    <div class="alert alert-danger mt-1" role="alert">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="form-group">
                  <label for="location">Localisation</label>
                  <input type="text" class="form-control" id="location" name="location" value="{{ Auth::user()->location }}" placeholder="France">

                  @error('location')
                    <div class="alert alert-danger mt-1" role="alert">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="form-group">
                  <label for="website">Site web</label>
                  <input type="text" class="form-control" id="website" name="website" value="{{ Auth::user()->website }}" placeholder="https://google.com">

                  @error('website')
                    <div class="alert alert-danger mt-1" role="alert">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="form-group">
                  <label>Bio</label>

                  @include('js.editor')

                  @error('content')
                    <div class="alert alert-danger mt-1" role="alert">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="form-group">
                  <button id="update-profile" type="submit" class="btn btn-pink shadow-none">Modifier</button>
                </div>
              </form>
            </div>

            {{-- notifs tab --}}
            <div class="tab-pane fade show {{Session::has('alert-notifs') ? 'active' : ''}}" id="notifs" role="tabpanel" aria-labelledby="notifs-tab">
              <h3>Notifications</h3>
              <p>En construction</p>
            </div>

            {{-- account tab --}}
            <div class="tab-pane fade show {{Session::has('alert-account') ? 'active' : ''}}" id="account" role="tabpanel" aria-labelledby="account-tab">
              <h3>Modifier le compte</h3>

              @if(Session::has('alert-account'))
                <div class="alert alert-info m-1" role="alert">
                  {{ Session::get('alert-account') }}
                </div>
              @endif

              <form method="POST" action="{{ route('profile.update.account') }}">
                @csrf
                @method('PUT')
                <div class="form-group">
                  <label for="email">Adresse mail</label>
                  <input type="email" class="form-control" id="email" name="email" value="{{ Auth::user()->email }}">

                  @error('email')
                    <div class="alert alert-danger mt-1" role="alert">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <p>Modifier le mot de passe</p>
                <div class="form-group">
                  <input type="password" class="form-control mb-1" name="password" placeholder="Nouveau mot de passe">
                  <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmer le nouveau mot de passe">

                  @error('password')
                    <div class="alert alert-danger mt-1" role="alert">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-pink shadow-none">Modifier</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-3 order-first order-md-2 mb-3">
      <div class="list-group" id="list-tab" role="tablist">
        <a class="list-group-item list-group-item-action {{Session::has('alert-notifs') || Session::has('alert-account') ? '' : 'active'}}" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="{{Session::has('alert-notifs') || Session::has('alert-account') ? 'false' : 'true'}}"><i class="fas fa-home"></i> Profil</a>
        <a class="list-group-item list-group-item-action {{Session::has('alert-notifs') ? 'active' : ''}}" id="notifs-tab" data-toggle="tab" href="#notifs" role="tab" aria-controls="notifs" aria-selected="{{Session::has('alert-notifs') ? 'true' : 'false'}}"><i class="fas fa-bell"></i> Notifications</a>
        <a class="list-group-item list-group-item-action {{Session::has('alert-account') ? 'active' : ''}}" id="account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="{{Session::has('alert-account') ? 'true' : 'false'}}"><i class="fas fa-cogs"></i> Compte</a>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
@include('js.wysiwyg')

<script>
  quill.clipboard.dangerouslyPasteHTML('{!! Auth::user()->bio !!}');

  $("#update-profile").on("click", function(e) {
    let content = JSON.stringify(quill.getContents());
    $("#content").val(content);
    $(this).submit();
  });

  function readUrl(input) {
    if(input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#preview').css('background-image', 'url(' + e.target.result + ')')
                     .css('display', 'block');
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#profile_image").change(function() {
    readUrl(this);
  });
</script>
@endsection
