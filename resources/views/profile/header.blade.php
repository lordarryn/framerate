<div class="background-user" style="background-image: url({{ getImageUrl($user->background, 'background_placeholder.svg') }})">
  <div class="container">
    <header class="profile-header">
      <div class="row align-items-end">
        {{-- avatar --}}
        <div class="col-lg-2 col-md-3 col-4">
          <div class="avatar border-width" style="background-image: url({{ getImageUrl($user->profile_image, 'avatar.jpg') }})"></div>
        </div>

        {{-- pseudo/website --}}
        <div class="col-md-4 col-4">
          <h1 class="profile-title profile-text">{{ $user->pseudo }}</h1>
          @if($user->website)
            <a href="{{ $user->website }}">{{ $user->website }}</a><br>
          @endif
          @auth
            @if($user->id == Auth::user()->id)
              <a href="{{ route('profile') }}"><i class="fas fa-fw fa-cogs"></i> Paramètres</a>
            @endif
          @endauth
        </div>

        {{-- following/ers --}}
        <div class="col-lg-6 col-4">
          @php($following = $user->following->count())
          @php($followers = $user->followers->count())
          <div class="float-right">
            <span class="d-inline-block">
              @if($following>0)
                <a href="#" data-toggle="modal" data-target="#following-modal">
                  {{ $following }} éclaireur{{ $following > 1 ? 's' : '' }}
                </a>
              @else
                <p class="profile-text">{{ $following }} éclaireur{{ $following > 1 ? 's' : '' }}</p>
              @endif
            </span>
            <span class="d-inline-block">
              @if($followers>0)
                <a href="#" data-toggle="modal" data-target="#followers-modal">
                  {{ $followers }} abonné{{ $followers > 1 ? 's' : '' }}
                </a>
              @else
                <p class="profile-text">{{ $followers }} abonné{{ $followers > 1 ? 's' : '' }}</p>
              @endif
            </span>
            {{-- if logged & visited profile is not the same as the user --}}
            @auth
              @if($user->id != Auth::user()->id)
                @php($follows = Auth::user()->following->contains($user))
                <form class="d-inline-block" action="{{ route('profile.follow', $user->pseudo) }}" method="post">
                  @csrf
                  <input type="hidden" name="follow" value="{{ $follows ? '0' : '1' }}">
                  <button type="submit" class="btn btn-pink shadow-none">{{ $follows ? 'Ne plus suivre' : 'Suivre' }}</button>
                </form>
              @endif
            @endauth
          </div>
        </div>
      </div>
    </header>
  </div>
</div>

@section('script')
<div class="modal fade" id="following-modal" tabindex="-1" role="dialog" aria-labelledby="following-modal-label" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Eclaireurs</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      @foreach($user->following as $following)
        <span class="mx-3"><a href="{{ route('profile.show', $following->pseudo) }}">{{ $following->pseudo }}</a></span>
      @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="followers-modal" tabindex="-1" role="dialog" aria-labelledby="followers-modal-label" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Abonnés</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      @foreach($user->followers as $follower)
        <span class="mx-3"><a href="{{ route('profile.show', $follower->pseudo) }}">{{ $follower->pseudo }}</a></span>
      @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>
@append
