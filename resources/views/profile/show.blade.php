@extends('layouts.app')

@section('description', 'Profil de ' . $user->pseudo . ' - ' . config('app.name'))
@section('og:title', 'Profil de ' . $user->pseudo . ' - ' . config('app.name'))
@section('og:description', 'Profil de ' . $user->pseudo . ' - ' . config('app.name'))
@section('og:url', route('profile.show', $user->pseudo))

@section('title', 'Profil de ' . $user->pseudo . ' - ' . config('app.name'))

@section('content')
@include('profile.header')

<div class="bg-dark">
  <div class="container">
    <nav>
      <div class="nav nav-tabs profile-nav justify-content-center" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-presentation-tab" data-toggle="tab" href="#" data-target="#nav-presentation" role="tab" aria-controls="nav-info" aria-selected="true"><i class="fas fa-home"></i> Présentation</a>
        <a class="nav-item nav-link" id="nav-activity-tab" data-toggle="tab" href="#" data-target="#nav-activity" role="tab" aria-controls="nav-cast" aria-selected="false"><i class="fas fa-clock"></i> Activité</a>
        <a class="nav-item nav-link" id="nav-collection-tab" data-toggle="tab" href="#" data-target="#nav-collection" role="tab" aria-controls="nav-cast" aria-selected="false"><i class="fas fa-table"></i> Collection</a>
        <a class="nav-item nav-link" id="nav-lists-tab" data-toggle="tab" href="#" data-target="#nav-lists" role="tab" aria-controls="nav-cast" aria-selected="false"><i class="fas fa-list"></i> Listes</a>
        <a class="nav-item nav-link" id="nav-images-tab" data-toggle="tab" href="#" data-target="#nav-images" role="tab" aria-controls="nav-cast" aria-selected="false"><i class="fas fa-photo-video"></i> Galerie</a>
      </div>
    </nav>
  </div>
</div>

<div class="container mt-1">
  <div class="tab-content" id="nav-tab-content">
    {{-- profile + top 50 --}}
    <div class="tab-pane fade show active" id="nav-presentation" role="tabpanel" aria-labelledby="nav-presentation-tab">
      @if($user->bio && !empty($user->bio))
        <div class="user-bio"><i class="fas fa-quote-left pr-1"></i>{!! $user->bio !!}<i class="fas fa-quote-right pl-1"></i></div>
      @endif
      <div class="row">
        <div class="col-6">
          <span><i class="fas fa-pen-alt"></i> Inscription : {{ \Carbon\Carbon::parse($user->created_at->timestamp)->translatedFormat('j F Y') }}</span><br>
          @if($user->last_active_at)
            <span><i class="fas fa-sign-in-alt"></i> Dernière connexion : {{ \Carbon\Carbon::parse($user->last_active_at->timestamp)->translatedFormat('j F Y') }}</span><br>
          @endif
          @if($user->show_birthday)
            <span><i class="fas fa-birthday-cake"></i> Né le : {{ \Carbon\Carbon::parse($user->birthday)->translatedFormat('j F Y') }}</span><br>
          @endif
          @if($user->location)
            <span><i class="fas fa-map-marker-alt"></i> {{ $user->location }}</span><br>
          @endif
        </div>
        <div class="col-6">
          <span><i class="fas fa-star"></i> Films notés : {{ $user->ratings->count() }}</span><br>
          <span><i class="fas fa-edit"></i> Renotations : {{ $user->oldRatings->count() }}</span><br>
          <span><i class="fas fa-percent"></i> Moyenne : {{ note($user->ratings->avg('rating')) }}</span><br>
        </div>
      </div>
      <hr>
      @if(!$top)
        <p class="text-center">Cette personne n'a pas encore de top 50.</p>
        @verified
          @if($user->id == Auth::user()->id)
            <p class="text-center"><a href="{{ route('list.top.edit') }}">Créer son top 50</a></p>
          @endif
        @endverified
      @else
        @verified
          @if($user->id == Auth::user()->id)
            <p class="text-center"><a href="{{ route('list.top.edit') }}">Modifier son top 50</a></p>
          @endif
        @endverified
        <div class="row justify-content-center mt-1">
          @foreach($top->films as $film)
            <div class="card d-flex align-items-center border-0 col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 my-1 text-center">
              <div class="row">
                  <div class="col-12">
                    <div id="{{ $film->slug }}" class="film-poster"
                         title="{{ $film->title }} @if(isset($film->release_year)) ({{ $film->release_year }})@endif"
                         style="background-image: url({{ getImageUrl($film->poster_thumbnail) }})"></div>
                  </div>
                  <div class="col-12">
                    <span>#{{ $film->pivot->order }}</span>
                    <a class="movie-title" href="{{ route('films.show', $film->slug) }}">{{ $film->title }}</a>
                    <span> - {{ $film->release_year }}</span>
                  </div>
              </div>
            </div>
          @endforeach
        </div>
      @endif
    </div>

    {{-- activity --}}
    <div class="tab-pane fade show" id="nav-activity" role="tabpanel" aria-labelledby="nav-activity-tab">
      <div class="row mt-1">
        <div class="col-md-12">
          @if($activities->total() > 0)
            <div class="card">
              <div class="card-body">
                <ul id="activity-feed" class="timeline">
                  @include('activity.profile', ['activities' => $activities])
                </ul>
              </div>
            </div>
          @else
            <p class="text-center mt-3">Aucune activité</p>
          @endif
        </div>
      </div>

      <div id="activity-loader" class="page-load-status">
        <div class="loader-ellips infinite-scroll-request">
          <span class="loader-ellips__dot"></span>
          <span class="loader-ellips__dot"></span>
          <span class="loader-ellips__dot"></span>
          <span class="loader-ellips__dot"></span>
        </div>
        <p class="infinite-scroll-last">Fin de la page</p>
        <p class="infinite-scroll-error">Fin de la page</p>
      </div>
    </div>

    {{-- collection --}}
    <div class="tab-pane fade show" id="nav-collection" role="tabpanel" aria-labelledby="nav-collection-tab">
      {{-- sorting row --}}
      <div class="row justify-content-between my-3">
        <div class="col-4">
          {{-- sort by --}}
          <label for="sort-by">Trier par : </label>
          <select name="sort_by" id="sort-by" class="btn shadow-none">
            <option value="note_date">Date de notation</option>
            <option value="release_date">Date de sortie</option>
            <option selected value="user_note">Note de {{ $user->pseudo }}</option>
            <option value="film_avg">Moyenne du film</option>
            <option value="film_pop">Popularité</option>
          </select>
        </div>
        <div class="col-4 text-right">
          {{-- film genre --}}
          <select name="genre" id="sort-genre" class="btn shadow-none">
            <option selected value="all">Genre (tout)</option>
            @foreach($genres as $genre)
              <option value="{{ $genre->id }}">{{ __('genres.'.$genre->name) }}</option>
            @endforeach
          </select>
          {{-- film decade --}}
          <select name="decade" id="sort-decade" class="btn shadow-none">
            <option selected value="all">Décennie (tout)</option>
            @for($i=2020;$i>=1870;$i-=10)
              <option value="{{ $i }}">Années {{ $i }}</option>
            @endfor
          </select>
        </div>
      </div>
      {{-- </sorting row> --}}
      <hr>
      <div id="film-collection" class="row justify-content-center mt-1">
        <div class="spinner-border" role="status">
          <span class="sr-only">Loading...</span>
        </div>
      </div>
    </div>

    {{-- lists --}}
    <div class="tab-pane fade show" id="nav-lists" role="tabpanel" aria-labelledby="nav-lists-tab">
      <hr>
      @if($user->lists->count() == 0)
        <p class="text-center">Aucune liste</p>
      @else
        <p class="text-center">{{ $user->lists->count() }} liste{{ $user->lists->count() > 1 ? 's' : '' }}</p>
      @endif

      <div class="row justify-content-center mt-1">
        @foreach($user->lists as $list)
          <a href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id]) }}">
            <div class="card d-flex align-items-center border-0 col-md-6 my-1 text-center">
              <img class="card-img-top" height="200" src="{{ isset($list->background) ? getImageUrl('thumbnail'.$list->background) : getImageUrl(null, 'background_placeholder.svg') }}" alt="List background">
              <div class="card-body">
                <h4 class="card-subtitle"><a href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id]) }}">{{ $list->title }}</a></h4>
              </div>
            </div>
          </a>
        @endforeach
      </div>
    </div>

    {{-- images/videos --}}
    @php($galeryTotal = $images->total())
    <div class="tab-pane fade show" id="nav-images" role="tabpanel" aria-labelledby="nav-images-tab">
      <h2 class="my-3 d-inline">Galerie</h2>
      <p class="mt-1">{{ $galeryTotal }} média{{ $galeryTotal > 1 ? 's' : '' }} posté{{ $galeryTotal > 1 ? 's' : '' }}</p>
      <hr>
      @include('image.index', ['images' => $images, 'caption' => 'profile'])

      <div id="gallery-loader" class="page-load-status">
        <div class="loader-ellips infinite-scroll-request">
          <span class="loader-ellips__dot"></span>
          <span class="loader-ellips__dot"></span>
          <span class="loader-ellips__dot"></span>
          <span class="loader-ellips__dot"></span>
        </div>
        <p class="infinite-scroll-last">Fin de la page</p>
        <p class="infinite-scroll-error">Fin de la page</p>
      </div>
    </div>
    <hr>
  </div>
</div>
@endsection

@section('script')
@include('image.js')
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
<script>
  $(function() {
    /**
     * Collection tab
     */
    function getCollection() {
      $("#film-collection").html(loader);
      $.ajax({
        type: 'GET',
        url: '{{ route('films.profile', $user->pseudo) }}?by='+sortBy+'&genre='+genre+'&decade='+decade+'&page='+page,
        success: function(data) {
          $("#film-collection").html(data);
        },
        error: function(request) {
          $("#film-collection").html('<p class="text-center text-muted col-12">Erreur</p>');
        }
      });
    }

    // load
    var loader =
    '<div class="spinner-border" id="collection-loader" role="status">'+
      '<span class="sr-only">Loading...</span>'+
    '</div>';
    var sortBy = "user_note";
    var genre = "all";
    var decade = "all";
    var page = 1;
    getCollection();

    // page
    $(document).on("click", ".collection-page", function(e) {
      e.preventDefault();
      page = $(this).data("page");
      getCollection();
    });

    // sort types
    $("#sort-by").change(function(e) {
      e.preventDefault();
      sortBy = this.value;
      page = 1;
      getCollection();
    });

    $("#sort-genre").change(function(e) {
      e.preventDefault();
      genre = this.value;
      page = 1;
      getCollection();
    });

    $("#sort-decade").change(function(e) {
      e.preventDefault();
      decade = this.value;
      page = 1;
      getCollection();
    });

    /**
     * Activity tab
     */
    $('#activity-feed').infiniteScroll({
      path: "{{ route('activities.profile', $user->pseudo) }}?page=@{{#}}",
      append: ".event-activity",
      history: false,
      status: "#activity-loader"
    });

    /**
     * Gallery tab
     */
    $("a[data-toggle=tab]").each(function () {
      var $this = $(this);

      $this.on('shown.bs.tab', function () {
        $(".grid").masonry({
          gutter: 10,
          fitWidth: true
        });

        $('.grid').infiniteScroll({
          path: "{{ route('images.profile', $user->pseudo) }}?page=@{{#}}",
          append: ".grid-item",
          outlayer: $(".grid").data("masonry"),
          history: false,
          status: "#gallery-loader"
        });
      });
    });
  });
</script>
@append
