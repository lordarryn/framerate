@extends('layouts.app')

@section('og:title')Vérification - {{ config('app.name') }}@endsection

@section('title')Vérification - {{ config('app.name') }}@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-5">
                <div class="card-header">Vérifier l'adresse e-mail</div>

                <div class="card-body">
                    @if(session('resent'))
                        <div class="alert alert-success" role="alert">Un nouveau lien a été envoyé à votre adresse e-mail.</div>
                    @endif

                    <span>Avant de poursuivre, veuillez vérifier votre e-mail pour cliquer sur le lien de vérification. Si vous n'avez pas reçu l'e-mail, <a id="verification-resend" class="btn btn-link shadow-none p-0 m-0 align-baseline" href="{{ route('verification.resend') }}">cliquez ici pour en demander un autre</a>.</span>

                    <form id="verification-resend-form" action="{{ route('verification.resend') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('#verification-resend').click(function(e) {
        e.preventDefault()
        $('#verification-resend-form').submit()
    })
</script>
@endsection
