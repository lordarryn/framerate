@extends('layouts.app')

@section('og:title')Confirmation mot de passe - {{ config('app.name') }}@endsection

@section('title')Confirmation mot de passe - {{ config('app.name') }}@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-5">
                <div class="card-header">Confirmation du mot de passe</div>

                <div class="card-body">
                    <span>Merci de confirmer le mot de passe avant de continuer.</span>

                    <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Mot de passe</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-pink shadow-none">Confirmer</button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link shadow-none" href="{{ route('password.request') }}">Mot de passe oublié</a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
