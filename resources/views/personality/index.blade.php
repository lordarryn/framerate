@extends('layouts.app')

@section('og:title', 'Top personnalités - ' . config('app.name'))

@section('title', 'Top personnalités - ' . config('app.name'))

@section('content')
<div class="container">
  <h1>Top personnalités</h1>

  <ul class="list-group list-group-flush">
    @foreach($personalities as $personality)
      <li class="list-group-item text-center">
        <div class="personality-image" title="{{ $personality->name }}" style="background-image: url({{ getImageUrl($personality->image) }})"></div>
        <a href="{{ route('personalities.show', $personality->slug) }}">{{ $personality->name }}</a> : {{ $personality->likes }} fan{{ $personality->likes > 1 ? 's' : '' }}.
      </li>
    @endforeach
  </ul>
</div>
@endsection
