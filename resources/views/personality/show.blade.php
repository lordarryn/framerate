@extends('layouts.app')

@section('description', $personality->bio)
@section('og:title', $personality->name . ' - ' . config('app.name'))
@section('og:description', $personality->bio)
@if($personality->image)
  @section('og:image', getImageUrl($personality->image))
@endif
@section('og:url', route('personalities.show', $personality->slug))

@section('title', $personality->name . ' - ' . config('app.name'))

@section('content')
<div class="container">
  <div class="personality-content row mt-5">
    <div class="col-lg-3">
      <div class="personality-image" title="{{ $personality->name }}" style="background-image: url({{ getImageUrl($personality->image) }})"></div>
    </div>
    <div class="col-lg-9">
      <div class="headline">
        <h1>{{ $personality->name }}</h1>
        @if(!empty($personality->imdb_id))
          <a href="https://www.imdb.com/name/{{ $personality->imdb_id }}/">
            <img class="imdb-logo mx-1" src="{{ asset('assets') . '/imdb.png' }}" title="Lien IMDb" alt="Lien IMDb">
          </a>
        @endif
        @foreach($personality->countries as $country)
          <a href="{{ route('countries.show', ['id' => $country->id, 'name' => __('countries.'.$country->name)]) }}">
            <img class="align-top country-flag" src="{{ asset('assets/countries') . '/' . strtolower($country->code) . '.svg' }}" alt="{{ $country->name }}" title="{{ $country->name }}">
          </a>
        @endforeach
      </div>

      {{-- like/dislike --}}
      @include('like.create', ['model' => $personality, 'type' => 'personality'])

      <div class="headline">
        @if($personality->akas->count() > 0)
          <p id="akas">
          @foreach($personality->akas as $aka)
            <span>{{ $aka->name }}</span>@if(!$loop->last), @endif
            @if($personality->akas->count() > 3 && $loop->iteration == 3)
              <a class="btn btn-light-pink shadow-none" href="#" data-toggle="collapse" data-target="#aka-more" aria-expanded="false" aria-controls="aka-more">Voir plus <i class="fas fa-caret-down"></i></a>
              <p id="aka-more" class="collapse" data-parent="#akas">
            @endif
            @if($personality->akas->count() > 5 && $loop->last)</p>@endif
          @endforeach
          </p>
        @endif
      </div>
      <p>
        <span>{{ $personality->place_of_birth }}</span>
        @if(isset($personality->deathday))
          <span>{{ (new \Carbon\Carbon($personality->birthday))->format('d/m/Y') }}</span>
        @endif
        @if(isset($personality->deathday))
          <span> - {{ (new \Carbon\Carbon($personality->deathday))->format('d/m/Y') }}</span>
        @endif
      </p>
      <p class="overview">{{ $personality->bio }}</p>
    </div>
  </div>

  {{-- lists --}}
  <div class="personality-content row mt-1">
    <div class="col-12">
      <nav>
        <div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
          @php ($panel = false)
          @foreach($personality->getters as $getter)
            @if($personality->{$getter['0']}->count() > 0)
              <a class="nav-item nav-link @if(!$panel)active @endif" id="nav-{{ $getter['0'] }}-tab" data-toggle="tab" href="#" data-target="#nav-{{ $getter['0'] }}" role="tab" aria-controls="nav-{{ $getter['0'] }}" @if(!$panel) aria-selected="true" @else aria-selected="false" @endif>
                @if($personality->sex == 1 && isset($getter['2']))
                  <span>{{ $getter['2'] }}</span>
                @else
                  <span>{{ $getter['1'] }}</span>
                @endif
                <span>({{ $personality->{$getter['0']}->groupBy('film_id')->count() }})</span>
              </a>
              @if(!$panel)
                @php ($panel = true)
              @endif
            @endif
          @endforeach
          <a class="nav-item nav-link" href="#medias"><i class="fas fa-photo-video"></i> Galerie</a>
        </div>
      </nav>
      <div class="tab-content" id="nav-tab-content">
        @php ($panel = false)
        @foreach($personality->getters as $getter)
          @if($personality->{$getter['0']}->count() > 0)
            <div class="tab-pane fade show @if(!$panel)active @endif" id="nav-{{ $getter['0'] }}" role="tabpanel" aria-labelledby="nav-{{ $getter['0'] }}-tab">
               <div class="row justify-content-center mt-1">
                  @foreach($personality->{$getter['0']}->groupBy('film_id') as $film)
                    <div class="card d-flex align-items-center border-0 col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 my-1 text-center">
                      <div class="row">
                        <div class="col-12">
                          <div id="{{ $film->first()->slug }}" class="film-poster"
                               title="{{ $film->first()->title }} @if(isset($film->first()->release_year)) ({{ $film->first()->release_year }})@endif"
                               style="background-image: url({{ getImageUrl($film->first()->poster_thumbnail) }})"></div>
                        </div>
                        <div class="col-12">
                          <a class="movie-title" href="{{ route('films.show', $film->first()->slug) }}">{{ $film->first()->title }}</a>
                          @php ($count = $film->first()->film->ratings->count())
                          <span> - {{ $film->first()->release_year }}</span>
                          @if($count > 0)
                              <span> - {{ note($film->first()->film->getAverage()) }}</span>
                          @endif
                          <br>
                          <span>@foreach($film as $filmGroupedBy){{ $filmGroupedBy->employment->name }}{{ !$loop->last ? ', ' : ''}}@endforeach</span>
                        </div>
                      </div>
                    </div>
                  @endforeach
               </div>
            </div>
            @if(!$panel)
              @php ($panel = true)
            @endif
          @endif
        @endforeach
      </div>
    </div>
  </div>

  {{-- Gallery --}}
  <div class="row">
    <div class="col-12 text-center">
      <h2 class="d-inline-block mb-0" id="medias">Galerie</h2>
      @verified
        <a href="#" class="btn btn-pink shadow-none mb-3" data-toggle="modal" data-target="#gallery-modal">
          Poster un média
        </a>
      @endverified
    </div>
  </div>
  <hr class="mt-n1">

  @if($personality->images->count() == 0)
    <div class="alert alert-primary" role="alert">
      La galerie est vide.
    </div>
  @else
    @include('image.index', ['images' => $personality->images, 'caption' => 'page'])
  @endif
</div>
@endsection

@section('script')
{{-- post to the gallery --}}
@include('image.create', ['model' => 'personality', 'slug' => $personality->slug])
@append
