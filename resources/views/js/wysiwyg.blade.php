<!-- wysiwyg quill -->
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
<script>
  //spoiler tag
  var Inline = Quill.import('blots/inline');

  class SpoilerBlot extends Inline {
    static blotName = "spoiler";
    static className = "spoiler-tag";
    static tagName = "span";

    static formats() {
      return true;
    }
  }

  Quill.register(SpoilerBlot);

  var quill = new Quill("#editor", {
    theme: "snow",
    placeholder: "Contenu",
    modules: {
      toolbar: "#toolbar"
    },
  });

  var spoilerButton = document.querySelector(".ql-spoiler");

  spoilerButton.addEventListener('click', function() {
    var range = quill.getSelection();

    if(range){
      quill.formatText(range, 'spoiler', true);
    }
  });

  $("#wform").on("submit", function(e) {
    let content = JSON.stringify(quill.getContents());
    $("#content").val(content);
    $(this).submit();
  });
</script>
