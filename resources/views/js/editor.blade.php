<!-- toolbar container -->
<div id="toolbar">
  <span class="ql-formats">
    <button class="ql-header" value="1"></button>
    <button class="ql-header" value="2"></button>
  </span>
  <span class="ql-formats">
    <button class="ql-bold"></button>
    <button class="ql-italic"></button>
    <button class="ql-underline"></button>
    <button class="ql-strike"></button>
  </span>
  <span class="ql-formats">
    <button class="ql-list" value="ordered"></button>
    <button class="ql-list" value="bullet"></button>
  </span>
  <span class="ql-formats">
    <button class="ql-blockquote"></button>
    <button class="ql-spoiler"><i class="fas fa-eye-slash"></i></button>
  </span>
  <span class="ql-formats">
    <button class="ql-clean"></button>
  </span>
</div>
<!-- editor -->
<div id="editor"></div>

<textarea name="content" id="content" class="d-none"></textarea>
