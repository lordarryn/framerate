<!-- app -->
<script>
@auth
  var guest = false;
  var notificationsRoute = "{{ route('notifications.notified') }}";
@endauth
@guest
  var guest = true;
@endguest
</script>

<script src="{{ asset('js/app.js') }}"></script>

{{-- crsf laravel token --}}
<script>
  $(function() {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  });
</script>
