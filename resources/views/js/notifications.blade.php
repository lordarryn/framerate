<style>
    .notification-row {
        cursor: pointer;
    }
    .notification-unread {
        border-left: 3px solid #dc3545;
        font-weight: bold;
    }
    .notification-read {
        border-left: 3px solid #6c757d;
    }
</style>
<script>
    // Fetch notifications
    function getNotifications(page) {
        $.ajax({
            url: "{{ route('notifications') }}/?page=" + page,
            type: "GET",
            success: function(html) {
                $("#notifications-pagination").empty();
                $("#notifications-table").empty();
                $("#notifications-pagination").append($(html).filter("#pagination"));
                $("#notifications-table").append($(html).filter("#table"));
            }
        });
    }
    getNotifications(1);

    // Pagination
    $(document).on("click", "#notifications-pagination .page-item", function(e) {
        e.preventDefault();
        page = $(this).data("value");
        getNotifications(page);
    });

    // Read a specific notification
    $(document).on("click", ".notification-row", function() {
        id = $(this).data("id");

        // Show the modal with overlay
        $("#notification-modal").modal('show');

        $(".modal-body").empty();

        // Get notification data
        $.ajax({
            url: "{{ route('notifications.show', '') }}/" + id,
            type: "GET",
            success: function(html) {
                // Remove the overlay
                $("#notification-modal .overlay").removeClass("d-flex").addClass("d-none");

                $(".modal-body").append($(html).filter(".modal-body").html());
                $("#notification-delete").data("value", id);

                // Mark as read
                $.ajax({
                    url: "{{ route('notifications.read', '') }}/" + id,
                    type: "PUT"
                });
                $(".notification-row[data-id=\"" + id + "\"]").removeClass("notification-unread").addClass("notification-read");
            }
        });
    });

    // When closing modal add overlay
    $(document).on("hidden.bs.modal", "#notification-modal", function() {
        $("#notification-modal .overlay").removeClass("d-none").addClass("d-flex");
    });

    // Mark all as read
    $(document).on("click", "#notifications-read-all", function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{ route('notifications.read', '*') }}",
            type: "PUT"
        });

        $("#notifications-table tbody").children("tr").each(function() {
            $(this).removeClass("notification-unread").addClass("notification-read");
        });
    });

    // Delete the notification
    $(document).on("click", "#notification-delete", function(e) {
        e.preventDefault();
        $.ajax({
            url: "{{ route('notifications.destroy', '') }}/" + $("#notification-delete").data("value"),
            type: "DELETE",
            success: function() {
                $(".notification-row[data-id=\"" + id + "\"]").remove();
            }
        });
    });
</script>

<div class="modal fade" id="notification-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="overlay d-flex justify-content-center align-items-center">
                <i class="fas fa-2x fa-sync fa-spin"></i>
            </div>
            <div class="modal-header">
                <h5 class="modal-title">Notification</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="notification-delete" data-value="">Supprimer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>
