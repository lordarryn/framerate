@extends('layouts.app')

@section('title'){{ $list->title }} - {{ config('app.name') }}@endsection
@section('og:title'){{ $list->title }} - {{ config('app.name') }}@endsection
@section('description'){{ 'Rejoignez ' . config('app.name') . ' pour pouvoir commencer à créer des listes !' }}@endsection
@section('og:description'){{ 'Rejoignez ' . config('app.name') . ' pour pouvoir commencer à créer des listes !' }}@endsection
@section('og:url', route('list.film.show', ['slug' => $list->slug, 'id' => $list->id]))

@if($list->background)
  @section('og:image', getImageUrl($list->background))
@endif

@php($totalFilm = $films->total())

@section('content')
<div class="background-list" style="{{ !isset($list->background) ? 'background-size:cover;' : '' }}background-image: url({{ getImageUrl($list->background, 'background_placeholder.svg') }})">
</div>

<div class="container">
  <header class="list-header">
    <h1>{{ $list->title }}</h1>
    <div class="list-subheader">
      <div style="background-image: url({{ getImageUrl($list->user->profile_image, 'avatar.jpg') }})" class="rounded-circle profile-avatar"></div>
      Liste de {{ $totalFilm }} film{{ $totalFilm > 1 ? 's' : ''}}, par <a href="{{ route('profile.show', $list->user->pseudo) }}">{{ $list->user->pseudo }}</a>
    </div>

    @verified
      @if(Auth::user()->id == $list->user->id)
        <p><a href="{{ route('list.film.edit', $list->id) }}"><i class="fas fa-edit"></i> Modifier la liste</a></p>
      @endif
    @endverified
  </header>

  @if($list->text && !empty($list->text))
    <div class="list-description annotation-div">{!! $list->text !!}</div>
  @endif

  <hr>

  <div class="row">
    <div class="col-md-9">
      {{-- empty list --}}
      @if($totalFilm == 0)
        <p class="empty-list">La liste est vide</p>
      @endif
      @foreach($films as $film)
        <div class="row mb-3">
          <div class="col-sm-5">
            <div id="{{ $film->slug }}" class="film-poster"
                 title="{{ $film->title }} @if(isset($film->release_year)) ({{ $film->release_year }})@endif"
                 style="background-image: url({{ getImageUrl($film->poster_thumbnail) }})"></div>
          </div>
          <div class="col-sm-7">
            <p>#{{ $film->pivot->order }} <a class="movie-title" href="{{ route('films.show', $film->slug) }}">{{ $film->title }} ({{ $film->release_year }})</a></p>
            @if($film->pivot->text)
              <div class="annotation-div">
                {!! nl2br(e($film->pivot->text)) !!}
              </div>
            @endif
            {{-- film avg --}}
            @if($film->ratings->count() > 0)
              <p class="mt-4">Moyenne du film : {{ $film->getAverage() }}</p>
            @endif

            {{-- note of the user from the list --}}
            @if((null !== Auth::user() && Auth::user()->id != $list->user->id) || null === Auth::user())
              @if($film->userRating($list->user))
                <p>Note de {{ $list->user->pseudo }} : {{ $film->userRating($list->user) }}</p>
              @endif
            @endif
            {{-- note of the user from the list if same as auth user --}}
            @if(null !== Auth::user() && Auth::user()->id == $list->user->id)
              @if($film->authRating())
                <p>Ma note : {{ $film->authRating() }}</p>
              @endif
            @endif

            {{-- auth user note if diff from user list --}}
            @if(null !== Auth::user() && Auth::user()->id != $list->user->id)
              @if($film->authRating())
                <p>Ma note : {{ $film->authRating() }}</p>
              @endif
            @endif
          </div>
        </div>
        @if(!$loop->last)
          <hr>
        @endif
      @endforeach
    </div>
    <div class="col-md-3">
      {{-- like/dislike --}}
      @include('like.create', ['model' => $list, 'type' => 'list'])

      {{-- info creation --}}
      <div class="annotation-div infos-list mb-5">
        Créée le {{ \Carbon\Carbon::parse($list->created_at)->translatedFormat('j F Y') }}<br>
        Modifiée le {{ \Carbon\Carbon::parse($list->updated_at)->translatedFormat('j F Y') }}<br>
        {{ $totalFilm }} film{{ $totalFilm > 1 ? 's' : ''}}
      </div>

      {{-- other lists --}}
      @if($list->user->lists->count() > 1)
        @verified
          @if(Auth::user()->id == $list->user->id)
            <h4>Mes autres listes</h4>
          @else
            <h4>Ses autres listes</h4>
          @endif
        @else
          <h4>Ses autres listes</h4>
        @endverified
        <div class="annotation-div infos-list">
          @foreach($list->user->lists as $otherList)
            @if($otherList->id != $list->id)
              <a href="{{ route('list.film.show', ['slug' => $otherList->slug, 'id' => $otherList->id]) }}">{{ $otherList->title }}</a>
            @endif
          @endforeach
        </div>
      @endif
    </div>
  </div>

  {{-- pagination --}}
  @if($films->count() != 0 && $films->lastPage() > 1)
    <hr>

    <div class="row">
      <div class="col-md-2 py-1 text-center">
        @if($films->currentPage() != 1)
          <a class="btn btn-pink shadow-none" href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id, 'page' => $films->currentPage()-1]) }}">Précédent</a>
        @endif
      </div>

      <div class="col-md-8 py-1 text-center d-flex">
        {{-- pagination main --}}
        <ul class="pagination mx-auto">
          {{-- always display first page --}}
          <li class="page-item{{ ($films->currentPage() == 1) ? ' active' : '' }}">
            <a class="page-link" href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id]) }}">1</a>
          </li>

          @php($prev = false)
          @php($next = false)
          @for($i = 1; $i <= $films->lastPage(); $i++)
            {{-- if iteration isn't first or last page --}}
            @if($i != 1 && $i != $films->lastPage())
              {{-- if iteration is current page --}}
              @if($i == $films->currentPage())
                <li class="page-item active">
                  <a class="page-link" href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id, 'page' => $i]) }}">{{ $i }}</a>
                </li>
              {{-- display on each side --}}
              @elseif(in_array($i, [$films->currentPage()-2, $films->currentPage()-1, $films->currentPage()+1, $films->currentPage()+2]))
                <li class="page-item">
                  <a class="page-link" href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id, 'page' => $i]) }}">{{ $i }}</a>
                </li>
              {{-- or display "..." --}}
              @else
                @if($i < $films->currentPage() && !$prev)
                  <li class="page-item disabled">
                    <a class="page-link" href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id, 'page' => $i]) }}">...</a>
                  </li>
                  @php($prev = true)
                @elseif($i > $films->currentPage() && !$next)
                  <li class="page-item disabled">
                    <a class="page-link" href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id, 'page' => $i]) }}">...</a>
                  </li>
                  @php($next = true)
                @endif
              @endif
            @endif
          @endfor

          {{-- always display last page --}}
          <li class="page-item{{ ($films->currentPage() == $films->lastPage()) ? ' active' : '' }}">
            <a class="page-link" href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id, 'page' => $films->lastPage()]) }}">{{ $films->lastPage() }}</a>
          </li>
        </ul>
      </div>

      <div class="col-md-2 py-1 text-center">
        @if($films->currentPage() != $films->lastPage())
          <a class="btn btn-pink shadow-none" href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id, 'page' => $films->currentPage()+1]) }}">Suivant</a>
        @endif
      </div>
    </div>
  @endif
</div>
@endsection
