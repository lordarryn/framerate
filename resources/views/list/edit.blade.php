@extends('layouts.app')

@section('title')Modifier la liste {{ $list->title }} - {{ config('app.name') }}@endsection

@section('og:title')Modifier la liste {{ $list->title }} - {{ config('app.name') }}@endsection

@section('header')
  <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection

@section('content')
<div class="container">
  <header class="list-header">
    <h1>Modifier la liste</h1>
    <p><a href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id]) }}"><i class="fas fa-eye"></i> Accéder à la liste</a></p>
  </header>

  @if(Session::has('info'))
    <div class="alert alert-info my-1" role="alert">
      {{ Session::get('info') }}
    </div>
  @endif

  @if(Session::has('alert'))
    <div class="alert alert-danger my-1" role="alert">
      {{ Session::get('alert') }}
    </div>
  @endif

  <form action="{{ route('list.film.update', $list->id) }}" method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    @error('title')
      <div class="alert alert-danger my-1" role="alert">
        {{ $message }}
      </div>
    @enderror

    <div class="form-group">
      <input name="title" class="form-control shadow-none" type="text" value="{{ $list->title }}" placeholder="Titre de la liste">
    </div>

    @error('content')
      <div class="alert alert-danger my-1" role="alert">
        {{ $message }}
      </div>
    @enderror

    @if(Session::has('alert-content'))
      <div class="alert alert-danger my-1" role="alert">
        {{ Session::get('alert-content') }}
      </div>
    @endif

    <div class="form-group">
      @include('js.editor')
    </div>

    @error('backgroundList')
      <div class="alert alert-danger m-1" role="alert">
        {{ $message }}
      </div>
    @enderror

    <div class="custom-file">
      <input type="file" class="custom-file-input" id="backgroundList" name="backgroundList">
      <label class="custom-file-label" for="backgroundList">Choisir un fond...</label>
    </div>

    <input type="submit" id="update-list" class="btn btn-pink shadow-none mt-3" value="Modifier">
  </form>

  <hr>
  <form action="{{ route('list.destroy', $list->id) }}" method="post">
    @csrf
    @method('DELETE')
    <input type="submit" id="delete-list" class="btn btn-pink shadow-none" value="Supprimer la liste">
  </form>
  <hr>

  <div class="row">
    <div class="col-12">
      {{-- empty list --}}
      @if($films->count() == 0)
        <div class="empty-list alert alert-info my-1" role="alert">
          La liste est vide. Vous pouvez rajouter un film sur un top depuis la page du film.
        </div>
      @endif
      @foreach($films as $film)
        <div class="row mb-3">
          <div class="col-4">
            <div id="{{ $film->slug }}" class="film-poster"
                 title="{{ $film->title }} @if(isset($film->release_year)) ({{ $film->release_year }})@endif"
                 style="background-image: url({{ getImageUrl($film->poster_thumbnail) }})"></div>
          </div>
          <div class="col-8">
            <p>#{{ $film->pivot->order }} <a class="movie-title" href="{{ route('films.show', $film->slug) }}">{{ $film->title }} ({{ $film->release_year }})</a></p>

            <div class="annotation-div">
              {{-- order --}}
              @if($films->count() > 1)
                <form action="{{ route('list.film.order', $list->id) }}" method="post">
                  @csrf
                  @method('PUT')
                  <input type="hidden" name="film_id" value="{{ $film->id }}">
                  <select name="order">
                    @php($tmp_title = $film->title)
                    @foreach($films as $tmp_film)
                      @if($tmp_film->title != $tmp_title)
                        <option value="{{ $tmp_film->pivot->order }}">#{{ $tmp_film->pivot->order . ' ' . $tmp_film->title }}</option>
                      @else
                        <option selected disabled value="{{ $tmp_film->pivot->order }}">#{{ $tmp_film->pivot->order . ' ' . $tmp_film->title }}</option>
                      @endif
                    @endforeach
                  </select>
                  <input type="submit" class="btn btn-pink shadow-none" value="Modifier">
                </form>
              @endif
              <hr>
              {{-- text --}}
              <form action="{{ route('list.film.text', $list->id) }}" method="POST">
                @csrf
                @method('PUT')
                <input type="hidden" name="film_id" value="{{ $film->id }}">
                <div class="form-group">
                  <textarea name="text" class="form-control shadow-none" cols="10" rows="5">{{ $film->pivot->text }}</textarea>
                </div>
                <input type="submit" class="btn btn-pink shadow-none" value="Modifier">
              </form>
              <hr>
              {{-- delete --}}
              <form action="{{ route('list.film.destroy', $list->id) }}" method="post">
                @csrf
                @method('DELETE')
                <input type="hidden" name="film_id" value="{{ $film->id }}">
                <input type="submit" class="btn btn-pink shadow-none" value="Supprimer">
              </form>
            </div>
          </div>
        </div>
        @if(!$loop->last)
          <hr>
        @endif
      @endforeach
    </div>
  </div>

  {{-- pagination --}}
  @if($films->count() != 0 && $films->lastPage() > 1)
    <hr>

    <div class="row">
      <div class="col-md-2 py-1 text-center">
        @if($films->currentPage() != 1)
          <a class="btn btn-pink shadow-none" href="{{ route('list.film.edit', ['id' => $list->id, 'page' => $films->currentPage()-1]) }}">Précédent</a>
        @endif
      </div>

      <div class="col-md-8 py-1 text-center d-flex">
        {{-- pagination main --}}
        <ul class="pagination mx-auto">
          {{-- always display first page --}}
          <li class="page-item{{ ($films->currentPage() == 1) ? ' active' : '' }}">
            <a class="page-link" href="{{ route('list.film.edit', ['id' => $list->id]) }}">1</a>
          </li>

          @php($prev = false)
          @php($next = false)
          @for($i = 1; $i <= $films->lastPage(); $i++)
            {{-- if iteration isn't first or last page --}}
            @if($i != 1 && $i != $films->lastPage())
              {{-- if iteration is current page --}}
              @if($i == $films->currentPage())
                <li class="page-item active">
                  <a class="page-link" href="{{ route('list.film.edit', ['id' => $list->id, 'page' => $i]) }}">{{ $i }}</a>
                </li>
              {{-- display on each side --}}
              @elseif(in_array($i, [$films->currentPage()-2, $films->currentPage()-1, $films->currentPage()+1, $films->currentPage()+2]))
                <li class="page-item">
                  <a class="page-link" href="{{ route('list.film.edit', ['id' => $list->id, 'page' => $i]) }}">{{ $i }}</a>
                </li>
              {{-- or display "..." --}}
              @else
                @if($i < $films->currentPage() && !$prev)
                  <li class="page-item disabled">
                    <a class="page-link" href="{{ route('list.film.edit', ['id' => $list->id, 'page' => $i]) }}">...</a>
                  </li>
                  @php($prev = true)
                @elseif($i > $films->currentPage() && !$next)
                  <li class="page-item disabled">
                    <a class="page-link" href="{{ route('list.film.edit', ['id' => $list->id, 'page' => $i]) }}">...</a>
                  </li>
                  @php($next = true)
                @endif
              @endif
            @endif
          @endfor

          {{-- always display last page --}}
          <li class="page-item{{ ($films->currentPage() == $films->lastPage()) ? ' active' : '' }}">
            <a class="page-link" href="{{ route('list.film.edit', ['id' => $list->id, 'page' => $films->lastPage()]) }}">{{ $films->lastPage() }}</a>
          </li>
        </ul>
      </div>

      <div class="col-md-2 py-1 text-center">
        @if($films->currentPage() != $films->lastPage())
          <a class="btn btn-pink shadow-none" href="{{ route('list.film.edit', ['id' => $list->id, 'page' => $films->currentPage()+1]) }}">Suivant</a>
        @endif
      </div>
    </div>
  @endif
</div>
@endsection

@section('script')
@include('js.wysiwyg')

<script>
  quill.root.dataset.placeholder = "Description de la liste";
  quill.clipboard.dangerouslyPasteHTML('{!! $list->text !!}');

  $("#update-list").on("click", function(e) {
    let content = JSON.stringify(quill.getContents());
    $("#content").val(content);
    $(this).submit();
  });

  $("#delete-list").on("click", function(e) {
    if(confirm("Êtes-vous sûr de vouloir supprimer la liste ?")) {
      $(this).submit();
    } else {
      e.preventDefault();
      return;
    }
  });
</script>
@endsection
