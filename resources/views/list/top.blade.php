@extends('layouts.app')

@section('title')Top 50 personnalisé - {{ config('app.name') }}@endsection

@section('og:title')Top 50 personnalisé - {{ config('app.name') }}@endsection

@section('content')
@include('profile.header')

<div class="bg-dark">
  <div class="container">
    <nav>
      <div class="nav nav-tabs justify-content-center">
        <h1 class="my-2 text-light">Modifier son top 50 films</h1>
      </div>
    </nav>
  </div>
</div>

<div class="container">
  <p class="text-center"><a href="{{ route('profile.show', $user->pseudo) }}"><i class="fas fa-eye"></i> Retourner sur le profil</a></p>

  @if(Session::has('alert'))
    <div class="alert alert-danger my-1" role="alert">
      {{ Session::get('alert') }}
    </div>
  @endif

  @if(Session::has('info'))
    <div class="alert alert-info my-1" role="alert">
      {{ Session::get('info') }}
    </div>
  @endif

  <div class="row">
    <div class="col-12">
      {{-- empty list --}}
      @if(!$top)
        <div class="alert alert-info my-1" role="alert">
          <h4>Votre top est vide</h4>
          <p>Vous pouvez rajouter un film sur un top depuis la page du film.</p>
        </div>
      @else
        @foreach($top->films as $film)
          <div class="row mb-3">
            <div class="col-4">
              <div id="{{ $film->slug }}" class="film-poster"
                   title="{{ $film->title }} @if(isset($film->release_year)) ({{ $film->release_year }})@endif"
                   style="background-image: url({{ getImageUrl($film->poster_thumbnail) }})"></div>
            </div>
            <div class="col-8">
              <p>#{{ $film->pivot->order }} <a class="movie-title" href="{{ route('films.show', $film->slug) }}">{{ $film->title }} ({{ $film->release_year }})</a></p>

              <div class="annotation-div">
                {{-- order --}}
                @if($top->films->count() > 1)
                  <form action="{{ route('list.film.order', $top->id) }}" method="post">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="film_id" value="{{ $film->id }}">
                    <select name="order">
                      @php($tmp_title = $film->title)
                      @foreach($top->films as $tmp_film)
                        @if($tmp_film->title != $tmp_title)
                          <option value="{{ $tmp_film->pivot->order }}">#{{ $tmp_film->pivot->order . ' ' . $tmp_film->title }}</option>
                        @else
                          <option selected disabled value="{{ $tmp_film->pivot->order }}">#{{ $tmp_film->pivot->order . ' ' . $tmp_film->title }}</option>
                        @endif
                      @endforeach
                    </select>
                    <input type="submit" class="btn btn-pink shadow-none" value="Modifier">
                  </form>
                @endif
                <hr>
                {{-- delete --}}
                <form action="{{ route('list.film.destroy', $top->id) }}" method="post">
                  @csrf
                  @method('DELETE')
                  <input type="hidden" name="film_id" value="{{ $film->id }}">
                  <input type="submit" class="btn btn-pink shadow-none" value="Supprimer">
                </form>
              </div>
            </div>
          </div>
          @if(!$loop->last)
            <hr>
          @endif
        @endforeach
      @endif
    </div>
  </div>

</div>
@endsection
