@extends('layouts.app')

@section('og:title')Forums - {{ config('app.name') }}@endsection

@section('title')Forums - {{ config('app.name') }}@endsection

@section('content')
<div class="container">
  <nav aria-label="breadcrumb" class="mt-1">
    <ol class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page">Tous les forums</li>
    </ol>
  </nav>

  <div class="row justify-content-center">
    @foreach($forums as $forum)
    <div class="col-md-7 col-11">
      <a href="{{ route('forums.show', $forum->id) }}" class="forums forum-{{ forumStyle($forum->name) }}">{{ $forum->name }}</a>
    </div>
    @endforeach
  </div>
</div>
@endsection
