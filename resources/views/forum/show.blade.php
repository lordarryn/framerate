@extends('layouts.app')

@section('og:title')Forum {{ $forum->name }} - {{ config('app.name') }}@endsection

@section('title')Forum {{ $forum->name }} - {{ config('app.name') }}@endsection

@section('header')
  <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection

@section('content')
<div class="container">
  <nav aria-label="breadcrumb" class="mt-1">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('forums') }}">Tous les forums</a></li>
      <li class="breadcrumb-item active" aria-current="page">{{ $forum->name }}</li>
    </ol>
  </nav>

  {{-- threads --}}
  <div class="row justify-content-center">
    <div class="col-md-8 col-11">
      @if($threads->total() == 0)
        <h3>Aucun sujet</h3>
        <p>Soyez le premier à en publier un !</p>
      @else
        {{-- pagination --}}
        <div class="row mb-1">
          <div class="col-4 text-left">
            @if($threads->currentPage() != 1)
              <a class="btn btn-pink shadow-none" href="{{ route('forums.show', $forum->id) }}"><i class="fas fa-angle-double-left"></i></a>
              <a class="btn btn-pink shadow-none" href="{{ route('forums.show', ['id' => $forum->id, 'page' => $threads->currentPage()-1]) }}"><i class="fas fa-angle-left"></i></a>
            @endif
          </div>
          <div class="col-2 offset-6 text-right">
            @if($threads->currentPage() != $threads->lastPage())
              <a class="btn btn-pink shadow-none" href="{{ route('forums.show', ['id' => $forum->id, 'page' => $threads->currentPage()+1]) }}"><i class="fas fa-angle-right"></i></a>
            @endif
          </div>
        </div>

        {{-- content --}}
        <table class="table table-hover">
          <thead>
            <th scope="col">Sujet</th>
            <th scope="col">Auteur</th>
            <th scope="col">NB</th>
            <th scope="col">Dernier MSG</th>
          </thead>
          <tbody>
            @foreach($threads as $thread)
              @php($timestamp = $thread->replies()->orderBy('created_at', 'desc')->first()->created_at->timestamp)
              <tr>
                <td class="p-2"><a href="{{ route('forums.threads.show', $thread->id) }}">{{ $thread->title }}</a></td>
                <td class="p-2"><a href="{{ route('profile.show', $thread->user->pseudo) }}">{{ $thread->user->pseudo }}</a></td>
                <td class="p-2">{{ $thread->replies->count() - 1 }}</td>
                <td class="p-2">{{ date('d/m/Y', $timestamp) == $date ? date('H:i:s', $timestamp) : date('d/m/Y', $timestamp)}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>

        <hr>

        {{-- pagination --}}
        <div class="row">
          <div class="col-4 text-left">
            @if($threads->currentPage() != 1)
              <a class="btn btn-pink shadow-none" href="{{ route('forums.show', $forum->id) }}"><i class="fas fa-angle-double-left"></i></a>
              <a class="btn btn-pink shadow-none" href="{{ route('forums.show', ['id' => $forum->id, 'page' => $threads->currentPage()-1]) }}"><i class="fas fa-angle-left"></i></a>
            @endif
          </div>
          <div class="col-2 offset-6 text-right">
            @if($threads->currentPage() != $threads->lastPage())
              <a class="btn btn-pink shadow-none" href="{{ route('forums.show', ['id' => $forum->id, 'page' => $threads->currentPage()+1]) }}"><i class="fas fa-angle-right"></i></a>
            @endif
          </div>
        </div>
      @endif
    </div>
  </div>

  {{-- Form --}}
  @auth
    @verified
      <div class="row justify-content-center">
        <div class="col-md-8 col-11">
          <h3>Nouveau sujet</h3>

          <form action="{{ route('forums.store', $forum->id) }}" method="post" id="wform">
            @csrf

            @error('title')
              <div class="alert alert-danger my-1" role="alert">
                {{ $message }}
              </div>
            @enderror

            <div class="form-group">
              <input name="title" class="form-control shadow-none" type="text" placeholder="Titre du sujet">
            </div>

            @error('content')
              <div class="alert alert-danger my-1" role="alert">
                {{ $message }}
              </div>
            @enderror

            @if(Session::has('alert-content'))
              <div class="alert alert-danger my-1" role="alert">
                {{ Session::get('alert-content') }}
              </div>
            @endif

            <div class="form-group">
              @include('js.editor')
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-pink shadow-none">Envoyer</button>
            </div>
          </form>
        </div>
      </div>
    @else
      <p class="alert alert-info">Vérifiez votre compte pour commencer à poster !</p>
    @endverified
  @endauth
</div>
@endsection

@section('script')
@include('js.wysiwyg')
@endsection
