@extends('layouts.app')

@section('og:title'){{ $reply->user->pseudo }} - {{ $reply->thread->title }} - Forum {{ $reply->thread->forum->name }} - {{ config('app.name') }}@endsection

@section('title'){{ $reply->user->pseudo }} - {{ $reply->thread->title }} - Forum {{ $reply->thread->forum->name }} - {{ config('app.name') }}@endsection

@section('content')
<div class="container">
  <nav aria-label="breadcrumb" class="mt-1">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('forums') }}">Tous les forums</a></li>
      <li class="breadcrumb-item"><a href="{{ route('forums.show', $reply->thread->forum->id) }}">{{ $reply->thread->forum->name }}</a></li>
      <li class="breadcrumb-item" aria-current="page"><a href="{{ route('forums.threads.show', $reply->thread->id) }}">Sujet : {{ $reply->thread->title }}</a></li>
      <li class="breadcrumb-item active" aria-current="page">{{ $reply->user->pseudo }} {{ $reply->created_at }}</li>
    </ol>
  </nav>

  <div class="row justify-content-center">
    <div class="col-md-8 col-11">
      <h3>{{ $reply->thread->title }}</h3>

      <div class="card my-3">
        <div class="card-header">
          <a href="{{ route('profile.show', $reply->user->pseudo) }}">{{ $reply->user->pseudo }}</a>,
          {{ date('d/m/Y à H:i:s', $reply->created_at->timestamp) }}
        </div>
        <div class="forum-msg card-body">
          {!! $reply->content !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
