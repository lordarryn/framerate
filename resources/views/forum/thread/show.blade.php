@extends('layouts.app')

@section('og:title'){{ $thread->title }} - Forum {{ $thread->forum->name }} - {{ config('app.name') }}@endsection

@section('title'){{ $thread->title }} - Forum {{ $thread->forum->name }} - {{ config('app.name') }}@endsection

@section('header')
  <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection

@section('content')
<div class="container">
  <nav aria-label="breadcrumb" class="mt-1">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('forums') }}">Tous les forums</a></li>
      <li class="breadcrumb-item"><a href="{{ route('forums.show', $thread->forum->id) }}">{{ $thread->forum->name }}</a></li>
      <li class="breadcrumb-item active" aria-current="page">Sujet : {{ $thread->title }}</li>
    </ol>
  </nav>

  {{-- thread content --}}
  <div class="row justify-content-center">
    <div class="col-md-8 col-11">
      <h3>{{ $thread->title }}</h3>

      {{-- pagination --}}
      @if($replies->count() != 0 && $replies->lastPage() > 1)
        <hr>

        <div class="row">
          <div class="col-md-2 py-1 text-center">
            @if($replies->currentPage() != 1)
              <a class="btn btn-pink shadow-none" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $replies->currentPage()-1]) }}"><i class="fas fa-angle-left"></i></a>
            @endif
          </div>

          <div class="col-md-8 py-1 text-center d-flex">
            {{-- pagination main --}}
            <ul class="pagination mx-auto">
              {{-- always display first page --}}
              <li class="page-item{{ ($replies->currentPage() == 1) ? ' active' : '' }}">
                <a class="page-link" href="{{ route('forums.threads.show', $thread->id) }}">1</a>
              </li>

              @php($prev = false)
              @php($next = false)
              @for($i = 1; $i <= $replies->lastPage(); $i++)
                {{-- if iteration isn't first or last page --}}
                @if($i != 1 && $i != $replies->lastPage())
                  {{-- if iteration is current page --}}
                  @if($i == $replies->currentPage())
                    <li class="page-item active">
                      <a class="page-link" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $i]) }}">{{ $i }}</a>
                    </li>
                  {{-- display on each side --}}
                  @elseif(in_array($i, [$replies->currentPage()-2, $replies->currentPage()-1, $replies->currentPage()+1, $replies->currentPage()+2]))
                    <li class="page-item">
                      <a class="page-link" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $i]) }}">{{ $i }}</a>
                    </li>
                  {{-- or display "..." --}}
                  @else
                    @if($i < $replies->currentPage() && !$prev)
                      <li class="page-item disabled">
                        <a class="page-link" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $i]) }}">...</a>
                      </li>
                      @php($prev = true)
                    @elseif($i > $replies->currentPage() && !$next)
                      <li class="page-item disabled">
                        <a class="page-link" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $i]) }}">...</a>
                      </li>
                      @php($next = true)
                    @endif
                  @endif
                @endif
              @endfor

              {{-- always display last page --}}
              <li class="page-item{{ ($replies->currentPage() == $replies->lastPage()) ? ' active' : '' }}">
                <a class="page-link" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $replies->currentPage()+1]) }}">{{ $replies->lastPage() }}</a>
              </li>
            </ul>
          </div>

          <div class="col-md-2 py-1 text-center">
            @if($replies->currentPage() != $replies->lastPage())
              <a class="btn btn-pink shadow-none" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $replies->currentPage()+1]) }}"><i class="fas fa-angle-right"></i></a>
            @endif
          </div>
        </div>
      @endif

      {{-- content --}}
      @foreach($replies as $reply)
        <div class="card my-3" id="reply_{{ $reply->id }}">
          <div class="card-header">
            <a href="{{ route('profile.show', $reply->user->pseudo) }}">{{ $reply->user->pseudo }}</a>,
            <a href="{{ route('forums.replies.show', $reply->id) }}">{{ date('d/m/Y à H:i:s', $reply->created_at->timestamp) }}</a>
          </div>
          <div class="forum-msg card-body">
            {!! $reply->content !!}
          </div>
        </div>
      @endforeach
      <hr>

      {{-- pagination --}}
      @if($replies->count() != 0 && $replies->lastPage() > 1)
        <hr>

        <div class="row">
          <div class="col-md-2 py-1 text-center">
            @if($replies->currentPage() != 1)
              <a class="btn btn-pink shadow-none" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $replies->currentPage()-1]) }}"><i class="fas fa-angle-left"></i></a>
            @endif
          </div>

          <div class="col-md-8 py-1 text-center d-flex">
            {{-- pagination main --}}
            <ul class="pagination mx-auto">
              {{-- always display first page --}}
              <li class="page-item{{ ($replies->currentPage() == 1) ? ' active' : '' }}">
                <a class="page-link" href="{{ route('forums.threads.show', $thread->id) }}">1</a>
              </li>

              @php($prev = false)
              @php($next = false)
              @for($i = 1; $i <= $replies->lastPage(); $i++)
                {{-- if iteration isn't first or last page --}}
                @if($i != 1 && $i != $replies->lastPage())
                  {{-- if iteration is current page --}}
                  @if($i == $replies->currentPage())
                    <li class="page-item active">
                      <a class="page-link" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $i]) }}">{{ $i }}</a>
                    </li>
                  {{-- display on each side --}}
                  @elseif(in_array($i, [$replies->currentPage()-2, $replies->currentPage()-1, $replies->currentPage()+1, $replies->currentPage()+2]))
                    <li class="page-item">
                      <a class="page-link" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $i]) }}">{{ $i }}</a>
                    </li>
                  {{-- or display "..." --}}
                  @else
                    @if($i < $replies->currentPage() && !$prev)
                      <li class="page-item disabled">
                        <a class="page-link" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $i]) }}">...</a>
                      </li>
                      @php($prev = true)
                    @elseif($i > $replies->currentPage() && !$next)
                      <li class="page-item disabled">
                        <a class="page-link" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $i]) }}">...</a>
                      </li>
                      @php($next = true)
                    @endif
                  @endif
                @endif
              @endfor

              {{-- always display last page --}}
              <li class="page-item{{ ($replies->currentPage() == $replies->lastPage()) ? ' active' : '' }}">
                <a class="page-link" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $replies->currentPage()+1]) }}">{{ $replies->lastPage() }}</a>
              </li>
            </ul>
          </div>

          <div class="col-md-2 py-1 text-center">
            @if($replies->currentPage() != $replies->lastPage())
              <a class="btn btn-pink shadow-none" href="{{ route('forums.threads.show', ['id' => $thread->id, 'page' => $replies->currentPage()+1]) }}"><i class="fas fa-angle-right"></i></a>
            @endif
          </div>
        </div>
      @endif
    </div>
  </div>

  {{-- Form --}}
  @auth
    @verified
      <div class="row justify-content-center">
        <div class="col-md-8 col-11">
          <h3>Répondre</h3>

          <form action="{{ route('forums.threads.store', $thread->id) }}" method="post" id="wform">
            @csrf

            @error('content')
              <div class="alert alert-danger my-1" role="alert">
                {{ $message }}
              </div>
            @enderror

            @if(Session::has('alert-content'))
              <div class="alert alert-danger my-1" role="alert">
                {{ Session::get('alert-content') }}
              </div>
            @endif

            <div class="form-group">
              @include('js.editor')
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-pink shadow-none">Envoyer</button>
            </div>
          </form>
        </div>
      </div>
    @else
      <p class="alert alert-info">Vérifiez votre compte pour commencer à poster !</p>
    @endverified
  @endauth
</div>
@endsection

@section('script')
@include('js.wysiwyg')
@endsection
