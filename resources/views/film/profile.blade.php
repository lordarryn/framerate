<p class="text-center text-muted col-12">{{ $films->total() }} film{{ $films->total() > 1 ? 's' : '' }} affiché{{ $films->total() > 1 ? 's' : '' }}</p>

{{-- film collection --}}
@foreach($films as $film)
  <div class="card d-flex align-items-center border-0 col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 my-1 text-center">
    <div class="row">
      <div class="col-12">
        <div id="{{ $film->film->slug }}" class="film-poster"
             title="{{ $film->film->title }} @if(isset($film->film->release_year)) ({{ $film->film->release_year }})@endif"
             style="background-image: url({{ getImageUrl($film->film->poster_thumbnail) }})"></div>
      </div>
      <div class="col-12">
        <a class="movie-title" href="{{ route('films.show', $film->film->slug) }}">{{ $film->film->title }}</a>
        <span> - {{ $film->film->release_year }} - {{ note($film->rating) }} le {{ date('d/m/Y', $film->created_at->timestamp )}}</span>
      </div>
    </div>
  </div>
@endforeach
{{-- </film collection> --}}

{{-- pagination --}}
<div class="col-md-12 py-1 text-center d-flex">
  @if($films->count() != 0 && $films->lastPage() > 1)
    {{-- pagination main --}}
    <ul class="pagination mx-auto">
      {{-- always display first page --}}
      <li class="page-item{{ ($films->currentPage() == 1) ? ' active' : '' }}">
        <a class="page-link collection-page shadow-none" href="#" data-page="1">1</a>
      </li>

      @php($prev = false)
      @php($next = false)
      @for($i = 1; $i <= $films->lastPage(); $i++)
        {{-- if iteration isn't first or last page --}}
        @if($i != 1 && $i != $films->lastPage())
          {{-- if iteration is current page --}}
          @if($i == $films->currentPage())
            <li class="page-item active">
              <a class="page-link collection-page shadow-none" href="#" data-page="{{ $i }}">{{ $i }}</a>
            </li>
          {{-- display on each side --}}
          @elseif(in_array($i, [$films->currentPage()-2, $films->currentPage()-1, $films->currentPage()+1, $films->currentPage()+2]))
            <li class="page-item">
              <a class="page-link collection-page shadow-none" href="#" data-page="{{ $i }}">{{ $i }}</a>
            </li>
          {{-- or display "..." --}}
          @else
            @if($i < $films->currentPage() && !$prev)
              <li class="page-item disabled">
                <a class="page-link" href="#">...</a>
              </li>
              @php($prev = true)
            @elseif($i > $films->currentPage() && !$next)
              <li class="page-item disabled">
                <a class="page-link" href="#">...</a>
              </li>
              @php($next = true)
            @endif
          @endif
        @endif
      @endfor

      {{-- always display last page --}}
      <li class="page-item{{ ($films->currentPage() == $films->lastPage()) ? ' active' : '' }}">
        <a class="page-link collection-page shadow-none" href="#" data-page="{{ $films->lastPage() }}">{{ $films->lastPage() }}</a>
      </li>
    </ul>
  @endif
</div>
