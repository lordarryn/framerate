@extends('layouts.app')

@section('description', $film->overview)
@section('og:title'){{ $film->title }} {{ isset($film->release_year) ? '('.$film->release_year.')' : '' }} - {{ config('app.name') }}@endsection
@section('og:description', $film->overview)
@if($film->backdrop)
  @section('og:image', getImageUrl($film->backdrop))
@endif
@section('og:url', route('films.show', $film->slug))

@section('title'){{ $film->title }} {{ isset($film->release_year) ? '('.$film->release_year.')' : '' }} - {{ config('app.name') }}@endsection

@section('content')
<div class="container">
  {{-- backdrop --}}
  @if(isset($film->backdrop))
    <header class="backdrop" style="background-image: url({{ getImageUrl($film->backdrop) }})">
      <div class="overlay"></div>
    </header>
  @else
    <header class="no-backdrop"></header>
  @endif
  <div class="film-content row">
    <div class="col-lg-3 text-center">
      {{-- film poster --}}
      <div class="row">
        <div class="col-12">
          <div class="film-poster"
               title="{{ $film->title }} @if(isset($film->release_year)) ({{ $film->release_year }})@endif"
               style="background-image: url({{ getImageUrl($film->poster_thumbnail) }})">
           </div>
        </div>
      </div>
      {{-- film notes --}}
      <div class="col-12 my-3">
        @php ($count = $film->ratings->count())
        @if($count == 0)
          <p id="average">Ø</p>
        @else
          <p id="average">{{ note($film->getAverage()) }}</p>
          <p><a href="#" data-toggle="modal" data-target="#notes-modal">Notes</a></p>
        @endif
        <p>{{ $count == 0 ? 'Ø' : $count }} <i class="fas fa-star"></i></p>
        @auth
          @verified
            @php ($note = $film->authRating())
            @if(isset($note))
              <p>Ma note : {{ $note }}</p>
            @endif
            <a class="btn btn-pink" data-toggle="modal" data-target="#rating-modal">Noter</a>
          @else
            <p class="alert alert-info">Vérifiez votre compte pour commencer à noter !</p>
          @endverified
        @endauth
      </div>
    </div>

    {{-- film content, film infos --}}
    <div class="film-content-col col-lg-9">
      <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
          <a class="nav-item nav-link active" id="nav-info-tab" data-toggle="tab" href="#" data-target="#nav-info" role="tab" aria-controls="nav-info" aria-selected="true"><i class="fas fa-info"></i> Informations</a>
          <a class="nav-item nav-link" id="nav-cast-tab" data-toggle="tab" href="#" data-target="#nav-cast" role="tab" aria-controls="nav-cast" aria-selected="false"><i class="fas fa-users"></i> Distribution</a>
          <a class="nav-item nav-link" id="nav-crew-tab" data-toggle="tab" href="#" data-target="#nav-crew" role="tab" aria-controls="nav-crew" aria-selected="false"><i class="fas fa-video"></i> Equipe</a>
          <a class="nav-item nav-link" id="nav-gestion-tab" data-toggle="tab" href="#" data-target="#nav-gestion" role="tab" aria-controls="nav-gestion" aria-selected="false"><i class="fas fa-cogs"></i> Gestion</a>
        </div>
      </nav>
      <div class="tab-content" id="nav-tab-content">
        {{-- Movie infos --}}
        <div class="tab-pane fade show active" id="nav-info" role="tabpanel" aria-labelledby="nav-info-tab">
          <div class="headline">
            <h1 class="movie-title">{{ $film->title }}</h1>
            @if(isset($film->release_year))
              <p><a href="{{ route('films.year', $film->release_year) }}">{{ $film->release_year }}</a></p>
            @endif
            @if(!empty($film->imdb_id))
              <p>
                <a href="https://www.imdb.com/title/{{ $film->imdb_id }}/">
                  <img class="imdb-logo mx-1" src="{{ asset('assets') . '/imdb.png' }}" title="Lien IMDb" alt="Lien IMDb">
                </a>
              </p>
            @endif
            <p>
            @foreach($film->countries as $country)
              <a href="{{ route('countries.show', ['id' => $country->id, 'name' => __('countries.'.$country->name)]) }}">
                <img class="align-top country-flag" src="{{ asset('assets/countries') . '/' . strtolower($country->code) . '.svg' }}" alt="{{ __('countries.'.$country->name) }}" title="{{ __('countries.'.$country->name) }}">
              </a>
            @endforeach
            </p>
          </div>
          @if($film->original_title != $film->title)
            <div class="headline">
              <p class="original-title">{{ $film->original_title }}</p>
            </div>
          @endif
          <div class="headline">
            <p id="directors">
              Réalisé par
              @foreach($film->directed as $director)
                <a clas="btn btn-link" href="{{ route('personalities.show', $director->personality->slug) }}">{{ $director->personality->name }}</a>@if(!$loop->last), @endif
                @if($film->directed->count() > 5 && $loop->iteration == 5)
                  <a class="btn btn-light-pink shadow-none my-2" href="#" data-toggle="collapse" data-target="#direction-more" aria-expanded="false" aria-controls="direction-more">Voir plus <i class="fas fa-caret-down"></i></a>
                  <div id="direction-more" class="collapse" data-parent="#directors">
                @endif
                @if($film->directed->count() > 5 && $loop->last)</div>@endif
              @endforeach
            </p>
          </div>
          <p class="overview">{{ $film->overview }}</p>
          <p>
            <i class="fas fa-clipboard-list"></i> Genres :
            @foreach($film->genres as $genre)
              <a clas="btn btn-link" href="{{ route('films.genre', ['id' => $genre->id, 'name' =>  __('genres.'.$genre->name)]) }}">{{ __('genres.'.$genre->name) }}</a>@if(!$loop->last), @endif
            @endforeach
          </p>
          <p>
            <i class="fas fa-industry"></i> Production :
            @foreach($film->companies as $company)
              <a clas="btn btn-link" href="{{ route('companies.show', $company->slug) }}">{{ $company->name }}</a>@if(!$loop->last), @endif
            @endforeach
          </p>
          <p><i class="fas fa-clock"></i> Durée : @if(isset($film->duration)){{ $film->duration }} min @endif</p>
          <p>
            <i class="fas fa-language"></i> Langue :
            @foreach($film->languages as $lang)
              {{ __('languages.'.$lang->name) }}@if(!$loop->last), @endif
            @endforeach
          </p>
          <p><i class="fas fa-camera-retro"></i> Aspect : {{ $film->aspect }}</p>
          <hr>
          @verified
            <p><a href="#" data-toggle="modal" data-target="#add-list-modal"><i class="fas fa-bars"></i> Ajouter à une liste</a></p>
          @endverified
          @php ($lists = $film->lists->count())
          @php ($tops = $film->tops->count())
          <p>Le film est présent dans {!! $lists > 0 ? '<a href="#" data-toggle="modal" data-target="#lists-modal">' : '' !!}{{ $lists }} liste{{ $lists > 1 ? 's' : '' }}{!! $lists > 0 ? '</a>' : '' !!} et {{ $tops }} top{{ $tops > 1 ? 's' : '' }} 50.</p>
        </div>

        {{-- Casting --}}
        <div class="tab-pane fade show" id="nav-cast" role="tabpanel" aria-labelledby="nav-cast-tab">
          <div id="casting" class="mt-3">
            @foreach($film->played as $actor)
              <p><a href="{{ route('personalities.show', $actor->personality->slug) }}">{{ $actor->personality->name }}</a> : <em>{{ $actor->role }}</em></p>
              @if($film->played->count() > 10 && $loop->iteration == 10)
                <p><a class="btn btn-light-pink shadow-none my-2" href="#" data-toggle="collapse" data-target="#casting-more" aria-expanded="false" aria-controls="casting-more">Voir plus <i class="fas fa-caret-down"></i></a></p>
                <div id="casting-more" class="collapse" data-parent="#casting">
              @endif
              @if($film->played->count() > 10 && $loop->last)</div>@endif
            @endforeach
          </div>
        </div>

        {{-- Crew tab --}}
        <div class="tab-pane fade show" id="nav-crew" role="tabpanel" aria-labelledby="nav-crew-tab">
          <div id="crew" class="mt-3">
            @foreach($film->crew->groupBy('personality_id') as $crewJobs)
              <p>
                <a href="{{ route('personalities.show', $crewJobs['0']->personality->slug) }}">{{ $crewJobs['0']->personality->name }}</a> :
                @foreach($crewJobs as $crewJob)
                  {{ $crewJob->employment->name }}@if(!$loop->last), @endif
                @endforeach
              </p>
              @if($film->crew->groupBy('personality_id')->count() > 10 && $loop->iteration == 10)
                <p><a class="btn btn-light-pink shadow-none my-2" href="#" data-toggle="collapse" data-target="#crew-more" aria-expanded="false" aria-controls="crew-more">Voir plus <i class="fas fa-caret-down"></i></a></p>
                <div id="crew-more" class="collapse" data-parent="#crew">
              @endif
              @if($film->crew->groupBy('personality_id')->count() > 10 && $loop->last)</div>@endif
            @endforeach
          </div>
        </div>

        {{-- Gestion tab --}}
        <div class="tab-pane fade show" id="nav-gestion" role="tabpanel" aria-labelledby="nav-gestion-tab">
          <div class="alert alert-danger my-1" role="alert">
            En construction
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- Gallery --}}
  <div class="row">
    <div class="col-12 text-center">
      <h2 class="d-inline-block mb-0">Galerie</h2>
      @verified
        <a href="#" class="btn btn-pink shadow-none mb-3" data-toggle="modal" data-target="#gallery-modal">
          Poster un média
        </a>
      @endverified
    </div>
  </div>
  <hr class="mt-n1">

  @if($film->images->count() == 0)
    <div class="alert alert-primary" role="alert">
      La galerie est vide.
    </div>
  @else
    @include('image.index', ['images' => $film->images, 'caption' => 'page'])
  @endif
</div>
@endsection

@section('script')
{{-- display lists --}}
@if($lists > 0)
  <div class="modal fade" id="lists-modal" tabindex="-1" role="dialog" aria-labelledby="lists-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Listes</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @foreach($film->lists as $list)
            <p><a href="{{ route('list.film.show', ['slug' => $list->slug, 'id' => $list->id]) }}" class="btn btn-link shadow-none">
              {{ $list->title }}, {{ $list->user->pseudo }}
            </a></p>
          @endforeach
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        </div>
      </div>
    </div>
  </div>
@endif

{{-- add list --}}
@verified
  <div class="modal fade" id="add-list-modal" tabindex="-1" role="dialog" aria-labelledby="add-list-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Ajouter à une liste</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('list.top.store') }}" method="post">
            @csrf
            <input type="hidden" name="film_id" value="{{ $film->id }}">
            <button type="submit" class="btn btn-link shadow-none"><i class="fas fa-star"></i> Ajouter ce film à son top 50</button>
          </form>
          <hr>

          <form action="{{ route('list.film.store') }}" method="post">
            @csrf
            <input type="hidden" name="film_id" value="{{ $film->id }}">
            <button type="submit" class="btn btn-link shadow-none"><i class="fas fa-plus"></i> Créer une liste et y ajouter ce film</a></button>
          </form>
          <hr>

          <h5>Ajouter ce film à une liste :</h5>
          @if(Auth::user()->lists->count() == 0)
            <p>Vous n'avez encore aucune liste</p>
          @else
            @foreach(Auth::user()->lists as $list)
              <form action="{{ route('list.film.add', $list->id) }}" method="post">
                @csrf
                <input type="hidden" name="film_id" value="{{ $film->id }}">
                <button type="submit" class="btn btn-link shadow-none"><i class="fas fa-plus-square"></i> {{ $list->title }}, créée le {{ \Carbon\Carbon::parse($list->created_at->timestamp)->translatedFormat('j F Y  à H:i:s') }}</button>
              </form>
            @endforeach
          @endif
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        </div>
      </div>
    </div>
  </div>
@endverified

{{-- notes --}}
<div class="modal fade" id="notes-modal" tabindex="-1" role="dialog" aria-labelledby="notes-modal-label" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Notes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      @foreach($film->ratings as $rating)
        <span class="mx-3"><a href="{{ route('profile.show', $rating->user->pseudo) }}">{{ $rating->user->pseudo }}</a> : {{ note($rating->rating) }}</span>
      @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

{{-- rate the film --}}
<div class="modal fade" id="rating-modal" tabindex="-1" role="dialog" aria-labelledby="rating-modal-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Noter {{ $film->title }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="alert"></div>
        <div id="rate" class="m-auto"></div>
        <div id="counter" class="text-center h3 mt-1"></div>
      </div>
      <div class="modal-footer">
        @if(isset($note))
          <button type="button" class="btn btn-pink shadow-none" id="destroy"><i class="fas fa-trash-alt"></i> Effacer mes actions</button>
        @endif
        <button type="button" class="btn btn-pink shadow-none" style="{{ isset($note) ? 'display:none' : '' }}" id="send-note">Noter</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

{{-- post to the gallery --}}
@include('image.create', ['model' => 'film', 'slug' => $film->slug])

<script>
  $(function() {
    @if(isset($film->poster))
      $(".film-poster").click(function() {
        window.open("{{ getImageUrl($film->poster) }}", "_blank")
      })
    @endif

    /**
     * Rating
     */
    var lastRating = {{ isset($note) ? $note : '0' }},
        rated = false

    $('#rating-modal').on('hidden.bs.modal', function() {
      if(rated) {
        location.reload()
      }
    })

    if(lastRating == 0) {
      $('#send-note').css('display', 'none')
    }

    var messages = {
      "1":    "Navet 😡",
      "1.5":  "Décevant 😠",
      "2":    "Mauvais 😧",
      "2.5":  "Mauvais 😧",
      "3":    "Médiocre 😟",
      "3.5":  "Médiocre 😟",
      "4":    "Insuffisant 🙁",
      "4.5":  "Insuffisant 🙁",
      "5":    "Moyen 😕",
      "5.5":  "Moyen 😕",
      "6":    "Correct 🙂",
      "6.5":  "Correct 🙂",
      "7":    "Bon 😊",
      "7.5":  "Bon 😊",
      "8":    "Très bon 😃",
      "8.5":  "Très bon 😃",
      "9":    "Excellent 😄",
      "9.5":  "Exceptionnel 😄",
      "10":   "Chef-d'œuvre 😍",
    }

    $('#rate').rateYo({
      rating: lastRating,
      numStars: 10,
      minValue: 1,
      maxValue: 10,
      halfStar: true,
      starSvg:  '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"/></svg>',
    }).on('rateyo.set', function(e, data) {
      if(data.rating < 1) {
        $(this).rateYo('rating', lastRating);
      } else {
        $('#send-note').css('display', 'inline-block');
        lastRating = data.rating;
      }
    }).on('rateyo.change', function(e, data) {
      if(data.rating >= 1) {
        $('#counter').text(data.rating + " " + messages[data.rating.toString()]);
      } else {
        $(this).rateYo('rating', lastRating);
      }
    })

    /**
     * Send the note
     */
    $('#send-note').on('click', function(e) {
      e.preventDefault();

      $.ajax({
        type: 'POST',
        url: '{{ route('ratings.rate') }}',
        data: {
          film: '{{ $film->slug }}',
          note: lastRating
        },
        success: function(data) {
          rated = true;
          $('#alert').empty().append('<p class="alert alert-info">Vous avez attribué la note de ' + lastRating + ' à {{ $film->title }}.</p>');
          $('#send-note').remove();
        },
        error: function(request) {
          $('#alert').empty().append('<p class="alert alert-danger">Erreur.</p>');
        }
      })
    })

    /**
     * Delete all informations
     */
    $('#destroy').on('click', function(e) {
      e.preventDefault();

      if(confirm("Êtes-vous sûr de vouloir effacer vos actions liées à ce film ?")) {
        $.ajax({
          type: 'DELETE',
          url: '{{ route('ratings.destroy') }}',
          data: {
            film: '{{ $film->slug }}'
          },
          success: function(data) {
            rated = true;
            $('#alert').empty().append('<p class="alert alert-info">Vous avez supprimé vos actions liées à ce film.</p>');
            $('#rate').rateYo('option', 'readOnly', true);
            $('#send-note').remove();
            $('#destroy').remove();
          },
          error: function(request) {
            $('#alert').empty().append('<p class="alert alert-danger">Erreur.</p>');
          }
        });
      }
    });
  });
</script>
@endsection
