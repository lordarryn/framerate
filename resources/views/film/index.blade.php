@extends('layouts.app')

@section('description', 'Top 50 des films tout confondu')
@section('og:title')Top films - {{ config('app.name') }}@endsection
@section('og:description', 'Top 50 des films tout confondu')
@section('og:url', route('films.index'))

@section('title')Top films - {{ config('app.name') }}@endsection

@section('content')
  <div class="container">
    <h2>Top films</h2>

    <select name="genre" id="top-genre" class="btn shadow-none">
      <option selected disabled>Filtrer par genre</option>
      @foreach($genres as $genre)
        <option value="{{ $genre->id }}" data-href="{{ route('films.genre', ['id' => $genre->id, 'name' =>  __('genres.'.$genre->name)]) }}">{{ __('genres.'.$genre->name) }}</option>
      @endforeach
    </select>

    <hr>

    <div class="row justify-content-center mt-1">
      @foreach($films as $film)
        <div class="card d-flex align-items-center border-0 col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 my-1 text-center">
          <div class="row">
            <div class="col-12">
              <div id="{{ $film->slug }}" class="film-poster"
                   title="{{ $film->title }} @if(isset($film->release_year)) ({{ $film->release_year }})@endif"
                   style="background-image: url({{ getImageUrl($film->poster_thumbnail) }})"></div>
            </div>
            <div class="col-12">
              <a class="movie-title" href="{{ route('films.show', $film->slug) }}">{{ $film->title }}</a>
              <span> - {{ $film->release_year }} - {{ note($film->average) }}</span>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection

@section('script')
<script>
$(function() {
  $("#top-genre").change(function(e) {
    e.preventDefault();
    genreHref = $("#top-genre option:selected").data('href');
    window.location.href = genreHref;
  });
});
</script>
@endsection
