{{-- wrapper for profile activity tab --}}
@foreach($activities as $activity)

  @php($isLastDailyActivity = $activity->isLastDailyActivity())
  @if($isLastDailyActivity)
    <li class="event event-activity" data-date="{{ ucfirst(\Carbon\Carbon::parse($activity->created_at->timestamp)->translatedFormat('l j F')) }}">
  @else
    <li class="in-event event-activity">
  @endif

    @include('activity.show', ['activity' => $activity, 'page' => 'profile'])

  </li>
@endforeach
