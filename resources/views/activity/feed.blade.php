{{-- wrapper for feed (index) --}}
@foreach($activities as $activity)
  <div class="activity-row">
    <a href="{{ route('profile.show', $activity->user->pseudo) }}">
      <div style="background-image: url({{ getImageUrl($activity->user->profile_image, 'avatar.jpg') }})" class="d-avatar"></div>
    </a>
    @include('activity.show', ['activity' => $activity, 'page' => 'feed'])
  </div>
@endforeach
