{{-- profile page: display pseudo and activity date as hour minute second --}}
@if($page == 'profile')
  @php($pseudo = $activity->user->pseudo)
  <p class="activity-date text-muted m-0">{{ date('H \h i', $activity->created_at->timestamp) }}</p>
{{-- feed page: display link to pseudo and activity date as difference --}}
@elseif($page == 'feed')
  @php($pseudo = '<a href="'.route('profile.show', $activity->user->pseudo).'">'.$activity->user->pseudo.'</a>')
  <p class="activity-date text-muted m-0">{{ ucfirst(\Carbon\Carbon::parse($activity->created_at->timestamp)->diffForHumans()) }}</p>
@endif

<p class="activity">
{{-- if null then activitiable model deleted --}}
{{-- rating film --}}
@if($activity->activityType->type == 'note')
  @if(isset($activity->activitiable->film))
    {!! $pseudo !!} a attribué la note de {{ $activity->activitiable->rating }} au film <a href="{{ route('films.show', $activity->activitiable->film->slug) }}">{{ $activity->activitiable->film->title }}</a>
  @else
    {!! $pseudo !!} a attribué la note de {{ $activity->activitiable->rating }} à un film
  @endif

{{-- following people --}}
@elseif($activity->activityType->type == 'follow')
  @if(isset($activity->activitiable))
    {!! $pseudo !!} a choisi <a href="{{ route('profile.show', $activity->activitiable->pseudo) }}">{{ $activity->activitiable->pseudo }}</a> comme éclaireur
  @else
    {!! $pseudo !!} a choisi un éclaireur
  @endif

{{-- post image gallery --}}
@elseif($activity->activityType->type == 'post_image')
  @if(isset($activity->activitiable))

    {!! $pseudo !!} a posté une {{ $activity->activitiable->media_type == 'image' ? 'image' : 'vidéo' }} dans la galerie
    @if(class_basename($activity->activitiable->imageable) == "Film")
      du film <a href="{{ route('films.show', $activity->activitiable->imageable->slug) }}">{{ $activity->activitiable->imageable->title }}</a>
    @elseif(class_basename($activity->activitiable->imageable) == "Personality")
      de la personnalité <a href="{{ route('personalities.show', $activity->activitiable->imageable->slug) }}">{{ $activity->activitiable->imageable->name }}</a>
    @endif

  @else
    {!! $pseudo !!} a posté une image dans les galeries
  @endif

{{-- like personality --}}
@elseif($activity->activityType->type == 'like_personality')
  @if(isset($activity->activitiable))
    {!! $pseudo !!} aime <a href="{{ route('personalities.show', $activity->activitiable->slug) }}">{{ $activity->activitiable->name }}</a>
  @else
    {!! $pseudo !!} aime une personnalité
  @endif

{{-- like list --}}
@elseif($activity->activityType->type == 'like_list')
  @if(isset($activity->activitiable))
    {!! $pseudo !!} aime la liste <a href="{{ route('list.film.show', ['slug' => $activity->activitiable->slug, 'id' => $activity->activitiable->id]) }}">{{ $activity->activitiable->title }}</a>
  @else
    {!! $pseudo !!} aime une liste
  @endif

{{-- like company --}}
@elseif($activity->activityType->type == 'like_company')
  @if(isset($activity->activitiable))
    {!! $pseudo !!} aime la société de production <a href="{{ route('companies.show', $activity->activitiable->slug) }}">{{ $activity->activitiable->name }}</a>
  @else
    {!! $pseudo !!} aime une société de production
  @endif

{{-- create list --}}
@elseif($activity->activityType->type == 'create_list')
  @if(isset($activity->activitiable))
    {!! $pseudo !!} a créé la liste <a href="{{ route('list.film.show', ['slug' => $activity->activitiable->slug, 'id' => $activity->activitiable->id]) }}">{{ $activity->activitiable->title }}</a>
  @else
    {!! $pseudo !!} a créé une liste
  @endif

{{-- add film to a list --}}
@elseif($activity->activityType->type == 'add_film_list')
  @if(isset($activity->activitiable))
    {!! $pseudo !!} a ajouté le film <a href="{{ route('films.show', $activity->addedFilm()->slug) }}">{{ $activity->addedFilm()->title }}</a> à la liste <a href="{{ route('list.film.show', ['slug' => $activity->activitiable->slug, 'id' => $activity->activitiable->id]) }}">{{ $activity->activitiable->title }}</a>
  @else
    {!! $pseudo !!} a ajouté un film à une liste
  @endif

{{-- add film to top --}}
@elseif($activity->activityType->type == 'add_film_top')
  @if(isset($activity->activitiable))
    {!! $pseudo !!} a ajouté le film <a href="{{ route('films.show', $activity->addedFilm()->slug) }}">{{ $activity->addedFilm()->title }}</a> dans son top
  @else
    {!! $pseudo !!} a ajouté un film à une liste
  @endif

@endif
</p>
