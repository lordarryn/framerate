@extends('layouts.app')

@section('og:title')Pays - {{ config('app.name') }}@endsection

@section('title')Pays - {{ config('app.name') }}@endsection

@section('content')
<div class="container">
    <h1>Pays</h1>

    <div class="row">
        <div class="col-12">
            <div id="chartdiv" class="countries-map" style="width: 100%; height: 600px"></div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="//cdn.amcharts.com/lib/4/maps.js"></script>
<script src="//cdn.amcharts.com/lib/4/geodata/worldLow.js"></script>

<script>
// Create map instance
var chart = am4core.create("chartdiv", am4maps.MapChart);

// Set map definition
chart.geodata = am4geodata_worldLow;

// Set projection
chart.projection = new am4maps.projections.Miller();

// Zoom control
chart.zoomControl = new am4maps.ZoomControl();

var homeButton = new am4core.Button();
homeButton.events.on("hit", function() {
  chart.goHome();
});

homeButton.icon = new am4core.Sprite();
homeButton.padding(7, 5, 7, 5);
homeButton.width = 30;
homeButton.icon.path = "M16,8 L14,8 L14,16 L10,16 L10,10 L6,10 L6,16 L2,16 L2,8 L0,8 L8,0 L16,8 Z M16,8";
homeButton.marginBottom = 10;
homeButton.parent = chart.zoomControl;
homeButton.insertBefore(chart.zoomControl.plusButton);

// Create map polygon series
var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

// Make map load polygon (like country names) data from GeoJSON
polygonSeries.useGeodata = true;

// Configure series
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.tooltipText = "{name}";
polygonTemplate.fill = am4core.color("#999");

// Create hover state and set alternative fill color
var hs = polygonTemplate.states.create("hover");
hs.properties.fill = am4core.color("#367B25");

// Remove Antarctica
polygonSeries.exclude = ["AQ"];

// Add heat rule
polygonSeries.heatRules.push({
  "property": "fill",
  "target": polygonSeries.mapPolygons.template,
  "min": am4core.color("#DAF7A6"),
  "max": am4core.color("#ff2300")
});

// Add expectancy data
polygonSeries.data = [
@foreach($countries as $country)
  { id: "{{ $country->code }}", title: "{{ __('countries.'.$country->name) }} : {{ $country->total }}", value: {{ $country->total }}, href: "{{ route('countries.show', ['id' => $country->id, 'name' => __('countries.'.$country->name)]) }}" },
@endforeach
];

polygonTemplate.events.on("hit", function(ev) {
  var data = ev.target.dataItem.dataContext;
  if(typeof data.href !== 'undefined') window.open(data.href);
});

polygonTemplate.tooltipText = "{title}";
polygonTemplate.cursorOverStyle = am4core.MouseCursorStyle.pointer;
</script>
@endsection
