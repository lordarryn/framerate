@extends('layouts.app')

@section('og:title')Top {{ __('countries.'.$country->name) }} - {{ config('app.name') }}@endsection

@section('title')Top {{ __('countries.'.$country->name) }} - {{ config('app.name') }}@endsection

@section('content')
<div class="container">
    <h1>{{ __('countries.'.$country->name) }}</h1>

    <div class="row justify-content-center mt-1">
         @foreach($films as $film)
             <div class="card d-flex align-items-center border-0 col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 my-1 text-center">
                 <div class="row">
                     <div class="col-12">
                         <div id="{{ $film->slug }}" class="film-poster"
                              title="{{ $film->title }} @if(isset($film->release_year)) ({{ $film->release_year }})@endif"
                              style="background-image: url({{ getImageUrl($film->poster_thumbnail) }})"></div>
                     </div>
                     <div class="col-12">
                         <a class="movie-title" href="{{ route('films.show', $film->slug) }}">{{ $film->title }}</a>
                         <span> - {{ $film->release_year }} - {{ note($film->average) }}</span>
                     </div>
                 </div>
             </div>
         @endforeach
    </div>
</div>
@endsection

@section('script')
@endsection
