<footer class="mt-5 bg-dark">
  <div class="container py-4">
    <div class="row">
      <div class="col-sm-10">
        <div class="row">
          <div class="col-12">
            <ul class="list-group list-group-horizontal list-unstyled">
              <li>
                <a class="nav-link" href="https://www.patreon.com/frameratefr">Faire un don</a>
              </li>
              <li>
                <a class="nav-link" href="{{ route('about') }}">A propos</a>
              </li>
              <li>
                <a class="nav-link" href="mailto:contact@framerate.fr">Contact</a>
              </li>
            </ul>
          </div>
          <div class="col-12 text-white">
            © 2021 Copyright <a href="{{ route('index') }}">{{ config('app.name') }}</a>.
            Données de <a href="https://www.themoviedb.org/">TMDb</a>.
          </div>
        </div>
      </div>
      <div class="col-sm-2">
        <img src="{{ asset('assets/masque-contour-500px.png') }}" alt="Logo masque" width="50">
      </div>
    </div>
  </div>
</footer>
