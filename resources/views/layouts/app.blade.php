<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta name="description" content="@yield('description')">
  <meta name="og:title" content="@yield('og:title')">
  <meta name="og:description" content="@yield('og:description')">
  <meta name="og:image" content="@yield('og:image')">
  <meta name="og:url" content="@yield('og:url')">
  <meta name="twitter:card" content="summary_large_image">

  <title>@yield('title')</title>

  @yield('header')

  <!-- Fonts -->
  <link rel='dns-prefetch' href='//fonts.googleapis.com' />
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto+Condensed&#038;ver=1.0.0' type='text/css' media='all' />
  <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Roboto+Condensed%3A400%2C700&#038;ver=5.3.3#038;subset=latin' type='text/css' media='all' />
  <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@600&display=swap" rel="stylesheet">

  <!-- JQuery fancybox -->
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css' type='text/css' media='all' />

  <!-- Slick -->
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css" />
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css">

  <!-- RateYo -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

  <!-- app -->
  <link rel='stylesheet' href='{{ asset('css/app.css') }}' type='text/css' media='all' />

  <!-- ga -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-174459838-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-174459838-1');
  </script>

  <!-- cookie consent -->
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
  <script>
    window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "#343a40 ",
          "text": "#ffffff"
        },
        "button": {
          "background": "#fd0e3e",
          "text": "#fff"
        }
      },
      "content": {
        "message": "Ce site internet utilise des cookies pour améliorer l'expérience utilisateur.",
        "dismiss": "Compris",
        "link": "Voir plus",
        "href": "{{ route('about') }}#cookie-policy"
      }
    })});
  </script>
</head>
<body>

<header id="masthead" class="site-header header-default has-search light">
  <div class="container">
    <div class="head-inner">
      <div id="site-logo">
        <a href="/">
          <img src="{{ asset('assets/logo-250px.png') }}" alt="FrameRate" width="150">
        </a>
      </div>

      @include('search.form')

      <div class="head-right">
        <nav class="primary-navigation">
          <div class="menu-mainnav-container">
            <ul id="menu-mainnav" class="nav-menu">
              <li>
                <a class="{{ Route::currentRouteName() == 'images.index' ? 'current-page' : '' }}" href="#">Découvertes</a>
                <ul>
                  <li><a href="{{ route('films.random') }}"><i class="fas fa-fw fa-random"></i> Film au hasard</a></li>
                  <li><a href="{{ route('personalities.random') }}"><i class="fas fa-fw fa-random"></i> Personne au hasard</a></li>
                  <li><a href="{{ route('companies.random') }}"><i class="fas fa-fw fa-random"></i> Société de production au hasard</a></li>
                  <li><a href="{{ route('images.index') }}"><i class="fas fa-fw fa-images"></i> Explorer la galerie</a></li>
                </ul>
              </li>

              <li>
                <a class="{{ in_array(Route::currentRouteName(), ['films.index', 'films.year', 'films.genre', 'personalities.index', 'companies.index', 'countries.index', 'countries.show']) ? 'current-page' : '' }}" href="#">Tops</a>
                <ul>
                  <li><a href="{{ route('films.index') }}"><i class="fas fa-fw fa-film"></i> Films</a></li>
                  <li><a href="{{ route('personalities.index') }}"><i class="fas fa-fw fa-user-friends"></i> Personnalités</a></li>
                  <li><a href="{{ route('countries.index') }}"><i class="fas fa-fw fa-globe-europe"></i> Pays</a></li>
                  <li><a href="{{ route('companies.index') }}"><i class="fas fa-industry"></i> Sociétés de production</a></li>
                </ul>
              </li>

              <li>
                <a href="#">Communauté</a>
                <ul>
                  <li><a href="{{ route('forums') }}"><i class="fas fa-fw fa-users"></i> Forum</a></li>
                </ul>
              </li>

              {{-- Authentication --}}
              @guest
                <li>
                  <div class="mt-2">
                    <a class="btn btn-light-pink shadow-none" href="{{ route('login') }}">Connexion</a>
                  </div>
                </li>
              @else
                <li>
                  <a class="{{ Route::currentRouteName() == 'profile' ? 'current-page' :
                               isset($authVisit) && $authVisit ? 'current-page' : '' }}" href="{{ route('profile.show', Auth::user()->pseudo) }}">{{ Auth::user()->pseudo }}</a>
                  <i class="fas fa-bell notification-bell" id="notification" @unless(Auth::user()->unreadNotifications()->count() > 0) style="display:none" @endunless></i>
                  <a class="p-0" href="{{ route('profile.show', Auth::user()->pseudo) }}">
                    <div style="background-image: url({{ getImageUrl(Auth::user()->profile_image, 'avatar.jpg') }})" class="rounded-circle profile-avatar"></div>
                  </a>
                  <ul>
                    <li><a href="{{ route('profile') }}"><i class="fas fa-fw fa-cogs"></i> Paramètres</a></li>
                    <li><a href="{{ route('admin.notifications') }}" data-id="notifications"><i class="fas fa-fw fa-bell"></i> <p class="d-inline">Notifications</p></a></li>
                    <li>
                      <a class="logout-btn" href="{{ route('logout') }}"><i class="fas fa-fw fa-sign-out-alt"></i> Déconnexion</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                      </form>
                    </li>
                  </ul>
                </li>
              @endguest
            </ul>
            <div id="menu-responsive">
              <i class="fas fa-bars"></i><i class="fas fa-times" style="display: none"></i>
            </div>
          </div>
        </nav>
      </div>
    </div>
  </div>
</header>

<div id="navigation-mobile">
  <ul class="menu">
    <li>
      <a href="#" aria-current="page">Découvertes</a>
      <ul class="sub-menu">
        <li><a href="{{ route('films.random') }}"><i class="fas fa-fw fa-random"></i> Film au hasard</a></li>
        <li><a href="{{ route('personalities.random') }}"><i class="fas fa-fw fa-random"></i> Personne au hasard</a></li>
        <li><a href="{{ route('companies.random') }}"><i class="fas fa-fw fa-random"></i> Société de production au hasard</a></li>
        <li><a href="{{ route('images.index') }}"><i class="fas fa-fw fa-images"></i> Explorer la galerie</a></li>
      </ul>
    </li>
    <li>
      <a href="#">Tops</a>
      <ul class="sub-menu">
        <li><a href="{{ route('films.index') }}"><i class="fas fa-fw fa-film"></i> Films</a></li>
        <li><a href="{{ route('personalities.index') }}"><i class="fas fa-fw fa-user-friends"></i> Personnalités</a></li>
        <li><a href="{{ route('countries.index') }}"><i class="fas fa-fw fa-globe-europe"></i> Pays</a></li>
        <li><a href="{{ route('companies.index') }}"><i class="fas fa-industry"></i> Sociétés de production</a></li>
      </ul>
    </li>
    <li>
      <a href="#">Communauté</a>
      <ul class="sub-menu">
        <li><a href="{{ route('forums') }}"><i class="fas fa-fw fa-users"></i> Forum</a></li>
      </ul>
    </li>
    @guest
      <li>
        <div class="my-1">
          <a class="" href="{{ route('login') }}">Connexion</a>
        </div>
      </li>
    @else
      <li>
        <a href="{{ route('profile.show', Auth::user()->pseudo) }}">
          {{ Auth::user()->pseudo }}
          <i class="fas fa-bell notification-bell" id="notification" @unless(Auth::user()->unreadNotifications()->count() > 0) style="display:none" @endunless></i>
          <img src="{{ getImageUrl(Auth::user()->profile_image, 'avatar.jpg') }}" class="rounded-circle align-middle profile-avatar" alt="Profil" title="Profil">
        </a>
        <ul class="sub-menu">
          <li><a href="{{ route('profile') }}"><i class="fas fa-fw fa-cogs"></i> Paramètres</a></li>
          <li><a href="{{ route('admin.notifications') }}" data-id="notifications"><i class="fas fa-fw fa-bell"></i> <p class="d-inline">Notifications</p></a></li>
          <li><a class="logout-btn" href="{{ route('logout') }}"><i class="fas fa-fw fa-sign-out-alt"></i> Déconnexion</a></li>
        </ul>
      </li>
    @endguest
  </ul>
</div>

  <main id="main">
    @yield('content')
  </main>

  <i id="scroll-main" class="fas fa-arrow-circle-up"></i>

@include('layouts.footer')

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<!-- bootstrap -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<!-- Font Awesome -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/js/all.min.js"></script>
<!-- fancybox -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<!-- masonry -->
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<!-- Slick -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<!-- RateYo -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
<!-- Twemoji -->
<script src="https://twemoji.maxcdn.com/v/latest/twemoji.min.js" crossorigin="anonymous"></script>
@include('js.app')
@yield('script')
</body>
</html>
