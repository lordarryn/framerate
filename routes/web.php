<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Index
Route::get('/', 'IndexController@index')->name('index');
Route::get('/a-propos', 'IndexController@about')->name('about');

Route::get('/profil', 'ProfileController@index')->name('profile');
Route::put('/profil', 'ProfileController@update')->name('profile.update');
Route::put('/profil/compte', 'ProfileController@updateAccount')->name('profile.update.account');

// Login routes
Route::get('/connexion', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/connexion', 'Auth\LoginController@login');
Route::post('/deconnexion', 'Auth\LoginController@logout')->name('logout');

// Register routes
Route::get('/inscription', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/inscription', 'Auth\RegisterController@register');

// Email verification routes
Route::get('/email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('/email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::post('/email/resend', 'Auth\VerificationController@resend')->name('verification.resend');

// Password reseting routes
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// Password confirmation routes
Route::get('/password/confirm', 'Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
Route::post('/password/confirm', 'Auth\ConfirmPasswordController@confirm');

// User profiles
Route::post('/profil/{pseudo}/suivre', 'ProfileController@follow')->name('profile.follow');
Route::get('/profil/{pseudo}', 'ProfileController@show')->name('profile.show');
Route::get('/p/{pseudo}', function($pseudo) {
    return redirect()->route('profile.show', $pseudo);
});

// Ajax notifications
Route::get('/notifications', 'NotificationController@index')->name('notifications');
Route::get('/notifications/notified', 'NotificationController@notified')->name('notifications.notified');
Route::get('/notifications/{id}', 'NotificationController@show')->name('notifications.show');
Route::put('/notifications/{id}', 'NotificationController@read')->name('notifications.read');
Route::delete('/notifications/{id}', 'NotificationController@destroy')->name('notifications.destroy');

// Search
Route::get('/rechercher', 'SearchController@search')->name('search');

// Countries
Route::get('/pays', 'CountryController@index')->name('countries.index');
Route::get('/pays/{id}/{name?}', 'CountryController@show')->name('countries.show');

// Forum
Route::get('/forums', 'ForumController@index')->name('forums');
Route::post('/forum/{id}', 'ForumController@storeThread')->name('forums.store');
Route::get('/forum/{id}', 'ForumController@show')->name('forums.show');
Route::post('/forum/sujet/{id}', 'ForumController@storeReply')->name('forums.threads.store');
Route::get('/forum/sujet/{id}', 'ForumController@showThread')->name('forums.threads.show');
Route::get('/forum/message/{id}', 'ForumController@showReply')->name('forums.replies.show');

// Films
Route::get('/films/top', function() {
    return redirect()->route('films.index');
});
Route::get('/films', 'FilmController@index')->name('films.index');
Route::get('/films/{pseudo}', 'FilmController@profile')->name('films.profile');
Route::get('/films/{id}/{name?}', 'FilmController@genre')->name('films.genre');
Route::get('/annee/{slug}', 'FilmController@year')->name('films.year');
Route::get('/film/aleatoire', 'FilmController@random')->name('films.random');
Route::get('/film/{slug}', 'FilmController@show')->name('films.show');

// Gallery
Route::get('/galerie', 'ImageController@index')->name('images.index');
Route::get('/gallery/{pseudo}', 'ImageController@profile')->name('images.profile');
Route::post('/gallery/{model}/{slug}/{media}', 'ImageController@store')
     ->where(['model' => 'film|personality', 'media' => 'image|video'])
     ->name('images.store');

// Personalities
Route::get('/personnalites', 'PersonalityController@index')->name('personalities.index');
Route::get('/personnalite/aleatoire', 'PersonalityController@random')->name('personalities.random');
Route::get('/personnalite/{slug}', 'PersonalityController@show')->name('personalities.show');

// Companies
Route::get('/productions', 'CompanyController@index')->name('companies.index');
Route::get('/production/aleatoire', 'CompanyController@random')->name('companies.random');
Route::get('/production/{slug}', 'CompanyController@show')->name('companies.show');

// Rating
Route::post('/note', 'RatingController@rate')->name('ratings.rate');
Route::delete('/note', 'RatingController@destroy')->name('ratings.destroy');

// Lists
Route::post('/liste/top/ajouter', 'ListController@storeTop')->name('list.top.store');
Route::post('/liste/ajouter', 'ListController@store')->name('list.film.store');
Route::get('/liste/top', 'ListController@editTop')->name('list.top.edit');
Route::put('/liste/{id}', 'ListController@update')->name('list.film.update');
Route::put('/liste/{id}/text', 'ListController@updateText')->name('list.film.text');
Route::post('/liste/{id}/ajouter', 'ListController@add')->name('list.film.add');
Route::put('/liste/{id}/ordonner', 'ListController@order')->name('list.film.order');
Route::get('/liste/{id}/modifier', 'ListController@edit')->name('list.film.edit');
Route::get('/liste/{slug}/{id}', 'ListController@show')->name('list.film.show');
Route::delete('/liste/{id}/supprimer/film', 'ListController@destroyFilm')->name('list.film.destroy');
Route::delete('/liste/{id}/supprimer', 'ListController@destroy')->name('list.destroy');

// Activity
Route::get('/activity', 'ActivityController@feed')->name('activities.feed');
Route::get('/activity/{pseudo}', 'ActivityController@profile')->name('activities.profile');

// Like/dislike
Route::post('/like/{model}/{id}', 'LikeController@like')
     ->name('like')
     ->where('model', 'image|personality|company|list|activity');
Route::post('/dislike/{model}/{id}', 'LikeController@dislike')
     ->name('dislike')
     ->where('model', 'personality|company|list');
Route::delete('/like/{model}/{id}', 'LikeController@destroy')
     ->name('like.destroy')
     ->where('model', 'image|personality|company|list|activity');

/*
|--------------------------------------------------------------------------
| Administration pages
|--------------------------------------------------------------------------
*/

Route::get('/admin', 'Admin\AdminController@index')->name('admin');
Route::get('/admin/notifications', 'Admin\AdminController@notifications')->name('admin.notifications');

Route::post('/admin/films', 'Admin\FilmController@store')->name('admin.films.store');
Route::get('/admin/films', 'Admin\FilmController@index')->name('admin.films.index');
Route::get('/admin/film/ajouter', 'Admin\FilmController@create')->name('admin.films.create');
Route::put('/admin/film/{id}', 'Admin\FilmController@update')->name('admin.films.update');
Route::delete('/admin/film/{id}', 'Admin\FilmController@destroy')->name('admin.films.destroy');
Route::get('/admin/film/modifier', 'Admin\FilmController@edit')->name('admin.films.edit');

Route::get('/admin/galeries', 'Admin\ImageController@index')->name('admin.images');
