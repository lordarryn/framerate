<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('imdb_id')->nullable();
            $table->string('vf_name');
            $table->string('vo_name')->nullable();
            $table->integer('duration')->nullable();
            $table->date('release')->nullable();
            $table->string('aspect')->nullable();
            $table->double('budget')->nullable();
            $table->string('languages')->nullable();
            $table->longText('overview')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
