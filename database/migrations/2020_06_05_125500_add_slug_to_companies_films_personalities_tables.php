<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugToCompaniesFilmsPersonalitiesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('slug');
        });

        Schema::table('films', function (Blueprint $table) {
            $table->string('slug');
        });

        Schema::table('personalities', function (Blueprint $table) {
            $table->string('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('slug');
        });

        Schema::table('films', function (Blueprint $table) {
            $table->dropColumn('slug');
        });

        Schema::table('personalities', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
