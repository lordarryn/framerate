<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_list', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('film_id');
            $table->foreignId('list_id');
            $table->foreign('film_id')->references('id')->on('films');
            $table->foreign('list_id')->references('id')->on('lists');
            $table->text('text')->nullable();
            $table->integer('order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_list');
    }
}
