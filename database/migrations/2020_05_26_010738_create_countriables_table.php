<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countriable', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('country_id');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->integer('countriable_id');
            $table->string('countriable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countriable');
    }
}
