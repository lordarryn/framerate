<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddViewDateToRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ratings', function (Blueprint $table) {
            $table->date('view_date')->nullable();
        });

        Schema::dropIfExists('viewings');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ratings', function (Blueprint $table) {
            $table->dropColumn('view_date');
        });

        Schema::create('viewings', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->date('viewing');
            $table->foreignId('film_id');
            $table->foreign('film_id')->references('id')->on('films');
            $table->foreignId('user_id');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }
}
