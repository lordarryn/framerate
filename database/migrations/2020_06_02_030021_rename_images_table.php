<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('images', 'galleries');

        Schema::table('galleries', function (Blueprint $table) {
            $table->foreignId('film_id');
            $table->foreign('film_id')->references('id')->on('films');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('galleries', 'images');

        Schema::table('images', function (Blueprint $table) {
            $table->dropForeign('film_id');
            $table->dropColumn('film_id');
        });
    }
}
