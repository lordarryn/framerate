<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('personality_id');
            $table->foreign('personality_id')->references('id')->on('personalities');
            $table->foreignId('film_id');
            $table->foreign('film_id')->references('id')->on('films');
            $table->foreignId('employment_id');
            $table->foreign('employment_id')->references('id')->on('employments');
            $table->string('role')->nullable();
            $table->integer('order')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
