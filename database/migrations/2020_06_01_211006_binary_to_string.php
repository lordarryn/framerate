<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BinaryToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->dropColumn('binary');
            $table->string('path');
        });

        Schema::table('personalities', function (Blueprint $table) {
            $table->string('image')->change();
        });

        Schema::table('films', function (Blueprint $table) {
            $table->string('poster')->change();
            $table->string('backdrop')->change();
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->string('logo')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->dropColumn('path');
            $table->binary('binary')->nullable();
        });

        Schema::table('personalities', function (Blueprint $table) {
            $table->binary('image')->change();
        });

        Schema::table('films', function (Blueprint $table) {
            $table->binary('poster')->change();
            $table->binary('backdrop')->change();
        });

        Schema::table('companies', function (Blueprint $table) {
            $table->binary('logo')->change();
        });
    }
}
