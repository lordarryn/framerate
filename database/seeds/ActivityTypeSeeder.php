<?php

use Illuminate\Database\Seeder;

class ActivityTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('activity_types')->delete();

        $forums = array(
            //array('type' => 'add_film_top'),
            array('type' => 'like_company'),
        );

        DB::table('activity_types')->insert($forums);
    }
}
