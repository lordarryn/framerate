<?php

use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->delete();

        $genres = array(
            array('name' => 'Action'),
            array('name' => 'Adult'),
            array('name' => 'Adventure'),
            array('name' => 'Animation'),
            array('name' => 'Biography'),
            array('name' => 'Comedy'),
            array('name' => 'Crime'),
            array('name' => 'Documentary'),
            array('name' => 'Drama'),
            array('name' => 'Family'),
            array('name' => 'Fantasy'),
            array('name' => 'Film Noir'),
            array('name' => 'History'),
            array('name' => 'Horror'),
            array('name' => 'Musical'),
            array('name' => 'Mystery'),
            array('name' => 'Romance'),
            array('name' => 'Sci-Fi'),
            array('name' => 'Short'),
            array('name' => 'Sport'),
            array('name' => 'TV Movie'),
            array('name' => 'Thriller'),
            array('name' => 'War'),
            array('name' => 'Western')
        );

        DB::table('genres')->insert($genres);
    }
}
