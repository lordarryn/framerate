<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->delete();

        $languages = array(
          0 =>
          array(
            'iso' => 'xx',
            'name' => 'No Language',
          ),
          1 =>
          array(
            'iso' => 'aa',
            'name' => 'Afar',
          ),
          2 =>
          array(
            'iso' => 'af',
            'name' => 'Afrikaans',
          ),
          3 =>
          array(
            'iso' => 'ak',
            'name' => 'Akan',
          ),
          4 =>
          array(
            'iso' => 'an',
            'name' => 'Aragonese',
          ),
          5 =>
          array(
            'iso' => 'as',
            'name' => 'Assamese',
          ),
          6 =>
          array(
            'iso' => 'av',
            'name' => 'Avaric',
          ),
          7 =>
          array(
            'iso' => 'ae',
            'name' => 'Avestan',
          ),
          8 =>
          array(
            'iso' => 'ay',
            'name' => 'Aymara',
          ),
          9 =>
          array(
            'iso' => 'az',
            'name' => 'Azerbaijani',
          ),
          10 =>
          array(
            'iso' => 'ba',
            'name' => 'Bashkir',
          ),
          11 =>
          array(
            'iso' => 'bm',
            'name' => 'Bambara',
          ),
          12 =>
          array(
            'iso' => 'bi',
            'name' => 'Bislama',
          ),
          13 =>
          array(
            'iso' => 'bo',
            'name' => 'Tibetan',
          ),
          14 =>
          array(
            'iso' => 'br',
            'name' => 'Breton',
          ),
          15 =>
          array(
            'iso' => 'ca',
            'name' => 'Catalan',
          ),
          16 =>
          array(
            'iso' => 'cs',
            'name' => 'Czech',
          ),
          17 =>
          array(
            'iso' => 'ce',
            'name' => 'Chechen',
          ),
          18 =>
          array(
            'iso' => 'cu',
            'name' => 'Slavic',
          ),
          19 =>
          array(
            'iso' => 'cv',
            'name' => 'Chuvash',
          ),
          20 =>
          array(
            'iso' => 'kw',
            'name' => 'Cornish',
          ),
          21 =>
          array(
            'iso' => 'co',
            'name' => 'Corsican',
          ),
          22 =>
          array(
            'iso' => 'cr',
            'name' => 'Cree',
          ),
          23 =>
          array(
            'iso' => 'cy',
            'name' => 'Welsh',
          ),
          24 =>
          array(
            'iso' => 'da',
            'name' => 'Danish',
          ),
          25 =>
          array(
            'iso' => 'de',
            'name' => 'German',
          ),
          26 =>
          array(
            'iso' => 'dv',
            'name' => 'Divehi',
          ),
          27 =>
          array(
            'iso' => 'dz',
            'name' => 'Dzongkha',
          ),
          28 =>
          array(
            'iso' => 'eo',
            'name' => 'Esperanto',
          ),
          29 =>
          array(
            'iso' => 'et',
            'name' => 'Estonian',
          ),
          30 =>
          array(
            'iso' => 'eu',
            'name' => 'Basque',
          ),
          31 =>
          array(
            'iso' => 'fo',
            'name' => 'Faroese',
          ),
          32 =>
          array(
            'iso' => 'fj',
            'name' => 'Fijian',
          ),
          33 =>
          array(
            'iso' => 'fi',
            'name' => 'Finnish',
          ),
          34 =>
          array(
            'iso' => 'fr',
            'name' => 'French',
          ),
          35 =>
          array(
            'iso' => 'fy',
            'name' => 'Frisian',
          ),
          36 =>
          array(
            'iso' => 'ff',
            'name' => 'Fulah',
          ),
          37 =>
          array(
            'iso' => 'gd',
            'name' => 'Gaelic',
          ),
          38 =>
          array(
            'iso' => 'ga',
            'name' => 'Irish',
          ),
          39 =>
          array(
            'iso' => 'gl',
            'name' => 'Galician',
          ),
          40 =>
          array(
            'iso' => 'gv',
            'name' => 'Manx',
          ),
          41 =>
          array(
            'iso' => 'gn',
            'name' => 'Guarani',
          ),
          42 =>
          array(
            'iso' => 'gu',
            'name' => 'Gujarati',
          ),
          43 =>
          array(
            'iso' => 'ht',
            'name' => 'Haitian; Haitian Creole',
          ),
          44 =>
          array(
            'iso' => 'ha',
            'name' => 'Hausa',
          ),
          45 =>
          array(
            'iso' => 'sh',
            'name' => 'Serbo-Croatian',
          ),
          46 =>
          array(
            'iso' => 'hz',
            'name' => 'Herero',
          ),
          47 =>
          array(
            'iso' => 'ho',
            'name' => 'Hiri Motu',
          ),
          48 =>
          array(
            'iso' => 'hr',
            'name' => 'Croatian',
          ),
          49 =>
          array(
            'iso' => 'hu',
            'name' => 'Hungarian',
          ),
          50 =>
          array(
            'iso' => 'ig',
            'name' => 'Igbo',
          ),
          51 =>
          array(
            'iso' => 'io',
            'name' => 'Ido',
          ),
          52 =>
          array(
            'iso' => 'ii',
            'name' => 'Yi',
          ),
          53 =>
          array(
            'iso' => 'iu',
            'name' => 'Inuktitut',
          ),
          54 =>
          array(
            'iso' => 'ie',
            'name' => 'Interlingue',
          ),
          55 =>
          array(
            'iso' => 'ia',
            'name' => 'Interlingua',
          ),
          56 =>
          array(
            'iso' => 'id',
            'name' => 'Indonesian',
          ),
          57 =>
          array(
            'iso' => 'ik',
            'name' => 'Inupiaq',
          ),
          58 =>
          array(
            'iso' => 'is',
            'name' => 'Icelandic',
          ),
          59 =>
          array(
            'iso' => 'it',
            'name' => 'Italian',
          ),
          60 =>
          array(
            'iso' => 'jv',
            'name' => 'Javanese',
          ),
          61 =>
          array(
            'iso' => 'ja',
            'name' => 'Japanese',
          ),
          62 =>
          array(
            'iso' => 'kl',
            'name' => 'Kalaallisut',
          ),
          63 =>
          array(
            'iso' => 'kn',
            'name' => 'Kannada',
          ),
          64 =>
          array(
            'iso' => 'ks',
            'name' => 'Kashmiri',
          ),
          65 =>
          array(
            'iso' => 'kr',
            'name' => 'Kanuri',
          ),
          66 =>
          array(
            'iso' => 'kk',
            'name' => 'Kazakh',
          ),
          67 =>
          array(
            'iso' => 'km',
            'name' => 'Khmer',
          ),
          68 =>
          array(
            'iso' => 'ki',
            'name' => 'Kikuyu',
          ),
          69 =>
          array(
            'iso' => 'rw',
            'name' => 'Kinyarwanda',
          ),
          70 =>
          array(
            'iso' => 'ky',
            'name' => 'Kirghiz',
          ),
          71 =>
          array(
            'iso' => 'kv',
            'name' => 'Komi',
          ),
          72 =>
          array(
            'iso' => 'kg',
            'name' => 'Kongo',
          ),
          73 =>
          array(
            'iso' => 'ko',
            'name' => 'Korean',
          ),
          74 =>
          array(
            'iso' => 'kj',
            'name' => 'Kuanyama',
          ),
          75 =>
          array(
            'iso' => 'ku',
            'name' => 'Kurdish',
          ),
          76 =>
          array(
            'iso' => 'lo',
            'name' => 'Lao',
          ),
          77 =>
          array(
            'iso' => 'la',
            'name' => 'Latin',
          ),
          78 =>
          array(
            'iso' => 'lv',
            'name' => 'Latvian',
          ),
          79 =>
          array(
            'iso' => 'li',
            'name' => 'Limburgish',
          ),
          80 =>
          array(
            'iso' => 'ln',
            'name' => 'Lingala',
          ),
          81 =>
          array(
            'iso' => 'lt',
            'name' => 'Lithuanian',
          ),
          82 =>
          array(
            'iso' => 'lb',
            'name' => 'Letzeburgesch',
          ),
          83 =>
          array(
            'iso' => 'lu',
            'name' => 'Luba-Katanga',
          ),
          84 =>
          array(
            'iso' => 'lg',
            'name' => 'Ganda',
          ),
          85 =>
          array(
            'iso' => 'mh',
            'name' => 'Marshall',
          ),
          86 =>
          array(
            'iso' => 'ml',
            'name' => 'Malayalam',
          ),
          87 =>
          array(
            'iso' => 'mr',
            'name' => 'Marathi',
          ),
          88 =>
          array(
            'iso' => 'mg',
            'name' => 'Malagasy',
          ),
          89 =>
          array(
            'iso' => 'mt',
            'name' => 'Maltese',
          ),
          90 =>
          array(
            'iso' => 'mo',
            'name' => 'Moldavian',
          ),
          91 =>
          array(
            'iso' => 'mn',
            'name' => 'Mongolian',
          ),
          92 =>
          array(
            'iso' => 'mi',
            'name' => 'Maori',
          ),
          93 =>
          array(
            'iso' => 'ms',
            'name' => 'Malay',
          ),
          94 =>
          array(
            'iso' => 'my',
            'name' => 'Burmese',
          ),
          95 =>
          array(
            'iso' => 'na',
            'name' => 'Nauru',
          ),
          96 =>
          array(
            'iso' => 'nv',
            'name' => 'Navajo',
          ),
          97 =>
          array(
            'iso' => 'nr',
            'name' => 'Ndebele',
          ),
          98 =>
          array(
            'iso' => 'nd',
            'name' => 'Ndebele',
          ),
          99 =>
          array(
            'iso' => 'ng',
            'name' => 'Ndonga',
          ),
          100 =>
          array(
            'iso' => 'ne',
            'name' => 'Nepali',
          ),
          101 =>
          array(
            'iso' => 'nl',
            'name' => 'Dutch',
          ),
          102 =>
          array(
            'iso' => 'nn',
            'name' => 'Norwegian Nynorsk',
          ),
          103 =>
          array(
            'iso' => 'nb',
            'name' => 'Norwegian Bokmål',
          ),
          104 =>
          array(
            'iso' => 'no',
            'name' => 'Norwegian',
          ),
          105 =>
          array(
            'iso' => 'ny',
            'name' => 'Chichewa; Nyanja',
          ),
          106 =>
          array(
            'iso' => 'oc',
            'name' => 'Occitan',
          ),
          107 =>
          array(
            'iso' => 'oj',
            'name' => 'Ojibwa',
          ),
          108 =>
          array(
            'iso' => 'or',
            'name' => 'Oriya',
          ),
          109 =>
          array(
            'iso' => 'om',
            'name' => 'Oromo',
          ),
          110 =>
          array(
            'iso' => 'os',
            'name' => 'Ossetian; Ossetic',
          ),
          111 =>
          array(
            'iso' => 'pi',
            'name' => 'Pali',
          ),
          112 =>
          array(
            'iso' => 'pl',
            'name' => 'Polish',
          ),
          113 =>
          array(
            'iso' => 'pt',
            'name' => 'Portuguese',
          ),
          114 =>
          array(
            'iso' => 'qu',
            'name' => 'Quechua',
          ),
          115 =>
          array(
            'iso' => 'rm',
            'name' => 'Raeto-Romance',
          ),
          116 =>
          array(
            'iso' => 'ro',
            'name' => 'Romanian',
          ),
          117 =>
          array(
            'iso' => 'rn',
            'name' => 'Rundi',
          ),
          118 =>
          array(
            'iso' => 'ru',
            'name' => 'Russian',
          ),
          119 =>
          array(
            'iso' => 'sg',
            'name' => 'Sango',
          ),
          120 =>
          array(
            'iso' => 'sa',
            'name' => 'Sanskrit',
          ),
          121 =>
          array(
            'iso' => 'si',
            'name' => 'Sinhalese',
          ),
          122 =>
          array(
            'iso' => 'sk',
            'name' => 'Slovak',
          ),
          123 =>
          array(
            'iso' => 'sl',
            'name' => 'Slovenian',
          ),
          124 =>
          array(
            'iso' => 'se',
            'name' => 'Northern Sami',
          ),
          125 =>
          array(
            'iso' => 'sm',
            'name' => 'Samoan',
          ),
          126 =>
          array(
            'iso' => 'sn',
            'name' => 'Shona',
          ),
          127 =>
          array(
            'iso' => 'sd',
            'name' => 'Sindhi',
          ),
          128 =>
          array(
            'iso' => 'so',
            'name' => 'Somali',
          ),
          129 =>
          array(
            'iso' => 'st',
            'name' => 'Sotho',
          ),
          130 =>
          array(
            'iso' => 'es',
            'name' => 'Spanish',
          ),
          131 =>
          array(
            'iso' => 'sq',
            'name' => 'Albanian',
          ),
          132 =>
          array(
            'iso' => 'sc',
            'name' => 'Sardinian',
          ),
          133 =>
          array(
            'iso' => 'sr',
            'name' => 'Serbian',
          ),
          134 =>
          array(
            'iso' => 'ss',
            'name' => 'Swati',
          ),
          135 =>
          array(
            'iso' => 'su',
            'name' => 'Sundanese',
          ),
          136 =>
          array(
            'iso' => 'sw',
            'name' => 'Swahili',
          ),
          137 =>
          array(
            'iso' => 'sv',
            'name' => 'Swedish',
          ),
          138 =>
          array(
            'iso' => 'ty',
            'name' => 'Tahitian',
          ),
          139 =>
          array(
            'iso' => 'ta',
            'name' => 'Tamil',
          ),
          140 =>
          array(
            'iso' => 'tt',
            'name' => 'Tatar',
          ),
          141 =>
          array(
            'iso' => 'te',
            'name' => 'Telugu',
          ),
          142 =>
          array(
            'iso' => 'tg',
            'name' => 'Tajik',
          ),
          143 =>
          array(
            'iso' => 'tl',
            'name' => 'Tagalog',
          ),
          144 =>
          array(
            'iso' => 'th',
            'name' => 'Thai',
          ),
          145 =>
          array(
            'iso' => 'ti',
            'name' => 'Tigrinya',
          ),
          146 =>
          array(
            'iso' => 'to',
            'name' => 'Tonga',
          ),
          147 =>
          array(
            'iso' => 'tn',
            'name' => 'Tswana',
          ),
          148 =>
          array(
            'iso' => 'ts',
            'name' => 'Tsonga',
          ),
          149 =>
          array(
            'iso' => 'tk',
            'name' => 'Turkmen',
          ),
          150 =>
          array(
            'iso' => 'tr',
            'name' => 'Turkish',
          ),
          151 =>
          array(
            'iso' => 'tw',
            'name' => 'Twi',
          ),
          152 =>
          array(
            'iso' => 'ug',
            'name' => 'Uighur',
          ),
          153 =>
          array(
            'iso' => 'uk',
            'name' => 'Ukrainian',
          ),
          154 =>
          array(
            'iso' => 'ur',
            'name' => 'Urdu',
          ),
          155 =>
          array(
            'iso' => 'uz',
            'name' => 'Uzbek',
          ),
          156 =>
          array(
            'iso' => 've',
            'name' => 'Venda',
          ),
          157 =>
          array(
            'iso' => 'vi',
            'name' => 'Vietnamese',
          ),
          158 =>
          array(
            'iso' => 'vo',
            'name' => 'Volapük',
          ),
          159 =>
          array(
            'iso' => 'wa',
            'name' => 'Walloon',
          ),
          160 =>
          array(
            'iso' => 'wo',
            'name' => 'Wolof',
          ),
          161 =>
          array(
            'iso' => 'xh',
            'name' => 'Xhosa',
          ),
          162 =>
          array(
            'iso' => 'yi',
            'name' => 'Yiddish',
          ),
          163 =>
          array(
            'iso' => 'za',
            'name' => 'Zhuang',
          ),
          164 =>
          array(
            'iso' => 'zu',
            'name' => 'Zulu',
          ),
          165 =>
          array(
            'iso' => 'ab',
            'name' => 'Abkhazian',
          ),
          166 =>
          array(
            'iso' => 'zh',
            'name' => 'Mandarin',
          ),
          167 =>
          array(
            'iso' => 'ps',
            'name' => 'Pushto',
          ),
          168 =>
          array(
            'iso' => 'am',
            'name' => 'Amharic',
          ),
          169 =>
          array(
            'iso' => 'ar',
            'name' => 'Arabic',
          ),
          170 =>
          array(
            'iso' => 'bg',
            'name' => 'Bulgarian',
          ),
          171 =>
          array(
            'iso' => 'cn',
            'name' => 'Cantonese',
          ),
          172 =>
          array(
            'iso' => 'mk',
            'name' => 'Macedonian',
          ),
          173 =>
          array(
            'iso' => 'el',
            'name' => 'Greek',
          ),
          174 =>
          array(
            'iso' => 'fa',
            'name' => 'Persian',
          ),
          175 =>
          array(
            'iso' => 'he',
            'name' => 'Hebrew',
          ),
          176 =>
          array(
            'iso' => 'hi',
            'name' => 'Hindi',
          ),
          177 =>
          array(
            'iso' => 'hy',
            'name' => 'Armenian',
          ),
          178 =>
          array(
            'iso' => 'en',
            'name' => 'English',
          ),
          179 =>
          array(
            'iso' => 'ee',
            'name' => 'Ewe',
          ),
          180 =>
          array(
            'iso' => 'ka',
            'name' => 'Georgian',
          ),
          181 =>
          array(
            'iso' => 'pa',
            'name' => 'Punjabi',
          ),
          182 =>
          array(
            'iso' => 'bn',
            'name' => 'Bengali',
          ),
          183 =>
          array(
            'iso' => 'bs',
            'name' => 'Bosnian',
          ),
          184 =>
          array(
            'iso' => 'ch',
            'name' => 'Chamorro',
          ),
          185 =>
          array(
            'iso' => 'be',
            'name' => 'Belarusian',
          ),
          186 =>
          array(
            'iso' => 'yo',
            'name' => 'Yoruba',
          ),
        );

        DB::table('languages')->insert($languages);
    }
}
