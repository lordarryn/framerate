<?php

use Illuminate\Database\Seeder;

class ForumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forums')->delete();

        $forums = array(
            array('name' => 'Cinéma'),
            array('name' => 'Séries TV'),
            array('name' => 'Littérature'),
            array('name' => 'Jeux vidéo'),
            array('name' => 'Musique'),
            array('name' => 'Bla-bla'),
        );

        DB::table('forums')->insert($forums);
    }
}
